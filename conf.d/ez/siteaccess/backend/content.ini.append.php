<?php /* #?ini charset="utf-8"?


[ClassAttributeSettings]
CategoryList[content]=Contenuti principali
CategoryList[meta]=Meta
CategoryList[hidden]=Nascosto
CategoryList[details]=Contenuti secondari
CategoryList[taxonomy]=Categorizzazione
CategoryList[attribuzione]=Attribuzione
CategoryList[taxonomy]=Categoria
CategoryList[geo]=Dove
CategoryList[time]=Quando

[embed]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
AvailableViewModes[]=banner
AvailableClasses[]
AvailableClasses[]=primary
AvailableClasses[]=secondary
AvailableClasses[]=success
AvailableClasses[]=info
AvailableClasses[]=warning
AvailableClasses[]=danger
AvailableClasses[]=dark
AvailableClasses[]=black

[embed-inline]
AvailableViewModes[]
AvailableViewModes[]=embed-inline

*/
