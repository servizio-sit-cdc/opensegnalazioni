FROM registry.gitlab.com/opencontent/ezpublish/php:1.2.3-v2

# Install and configure new relic agent
#ENV PHP_AGENT_URL=https://download.newrelic.com/php_agent/release/newrelic-php5-10.0.0.312-linux.tar.gz
#RUN \
#  curl -L $PHP_AGENT_URL | tar -C /tmp -zx && \
#  export NR_INSTALL_USE_CP_NOT_LN=1 && \
#  export NR_INSTALL_SILENT=1 && \
#  /tmp/newrelic-php5-*/newrelic-install install && \
#  rm -rf /tmp/newrelic-php5-* /tmp/nrinstall* && \
#  sed -i \
#      -e 's/newrelic.appname = "PHP Application"/newrelic.appname = "Opensegnalazioni"/' \
#      -e 's/;newrelic.daemon.app_connect_timeout =.*/newrelic.daemon.app_connect_timeout=15s/' \
#      -e 's/;newrelic.daemon.start_timeout =.*/newrelic.daemon.start_timeout=5s/' \
#      /usr/local/etc/php/conf.d/newrelic.ini

WORKDIR /var/www

COPY composer.json composer.lock /var/www/

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN echo "Running composer"  \
	&& composer install --prefer-dist --no-scripts --no-dev \
	&& rm -rf /root/.composer \
	&& ls -l html/vendor/bin


# Add some custom config... only if strictly needed for a single instance
# otherwise change it in the base-image
# COPY conf.d/php.ini ${PHP_INI_DIR}/conf.d/php.ini

# Add default settings
COPY conf.d/ez/override /var/www/html/settings/override
COPY conf.d/ez/siteaccess /var/www/html/settings/siteaccess
COPY conf.d/ez/config.cluster.php /var/www/html/config.cluster.php

# Add installer
COPY conf.d/installer /var/www/installer

COPY security.txt /var/www/html/.well-known/security.txt

# Copy install script to default entrypoint script dir to extend base entrypoint without modifying it
# @see https://github.com/OpencontentCoop/docker-ezpublish/blob/master/scripts/docker-entrypoint.sh#L84
COPY scripts/install.sh /docker-entrypoint-initdb.d/

WORKDIR /var/www/html

RUN php bin/php/ezpgenerateautoloads.php -e

ARG CI_COMMIT_REF_NAME=no-branch 
ARG CI_COMMIT_SHORT_SHA=1234567
ARG CI_COMMIT_TAG

COPY scripts/get-version.sh /bin/
RUN /bin/get-version.sh > /var/www/html/VERSION

WORKDIR /var/www/html

# The following directives are already present in the base-image
# don't change them here unless for debugging or improvement of the
# base-image itself.
#ENTRYPOINT ["/scripts/docker-entrypoint.sh"]
#CMD php-fpm
#EXPOSE 9000
