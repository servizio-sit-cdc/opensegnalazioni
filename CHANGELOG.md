# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [3.0.46](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.45...3.0.46) - 2023-05-10


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-crt-php           | v1.0.4   | v1.2.1   | [...](https://github.com/awslabs/aws-crt-php/compare/v1.0.4...v1.2.1)                           |
| aws/aws-sdk-php           | 3.261.15 | 3.269.9  | [...](https://github.com/aws/aws-sdk-php/compare/3.261.15...3.269.9)                            |
| google/apiclient          | 5ed4edc  | 53c3168  | [...](https://github.com/googleapis/google-api-php-client/compare/5ed4edc...53c3168)            |
| google/apiclient-services | v0.291.0 | v0.299.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.291.0...v0.299.0) |
| league/oauth2-client      | 2.6.1    | 2.7.0    | [...](https://github.com/thephpleague/oauth2-client/compare/2.6.1...2.7.0)                      |
| opencontent/ocinstaller   | 4c2986e  | d2b7487  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/4c2986e...d2b7487)                 |
| opencontent/ocopendata-ls | 2.28.3   | 2.28.6   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.28.3...2.28.6)                    |
| opencontent/ocsensor-ls   | 7.4.0    | 7.4.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.4.0...7.4.1)                        |
| opencontent/ocsensorapi   | 7.2.5    | 7.2.6    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.5...7.2.6)                     |
| php-http/client-common    | 2.6.0    | 2.x-dev  | [...](https://github.com/php-http/client-common/compare/2.6.0...2.x-dev)                        |
| php-http/message          | 1.13.0   | 1.x-dev  | [...](https://github.com/php-http/message/compare/1.13.0...1.x-dev)                             |
| php-http/message-factory  | 597f30e  | 4d8778e  | [...](https://github.com/php-http/message-factory/compare/597f30e...4d8778e)                    |
| psr/http-client           | 22b2ef5  | 0955afe  | [...](https://github.com/php-fig/http-client/compare/22b2ef5...0955afe)                         |
| psr/http-factory          | 5a4f141  | e616d01  | [...](https://github.com/php-fig/http-factory/compare/5a4f141...e616d01)                        |
| psr/http-message          | efd67d1  | 1.1      | [...](https://github.com/php-fig/http-message/compare/efd67d1...1.1)                            |
| zetacomponents/mvc-tools  | 1.2.1    | 1.2.2    | [...](https://github.com/zetacomponents/MvcTools/compare/1.2.1...1.2.2)                         |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 4c2986e and d2b7487](https://github.com/OpencontentCoop/ocinstaller/compare/4c2986e...d2b7487)**
* Add lock status feature
* Remove tag only from custom script
* Remove verbose debug log
* Fix merge imports
* Store data dit foreach version
* Bugfix in TagTreeCsv installer

**[opencontent/ocopendata-ls changes between 2.28.3 and 2.28.6](https://github.com/OpencontentCoop/ocopendata/compare/2.28.3...2.28.6)**
* Add cache-control response filter (disabled by default)
* Fix file name enconding in api file url Bugfix in PAge connector
* Encode filename in file.url payload if needed

**[opencontent/ocsensor-ls changes between 7.4.0 and 7.4.1](https://github.com/OpencontentCoop/ocsensor/compare/7.4.0...7.4.1)**
* Introduce un flag env SENSOR_READ_ONLY per mettere il sito in modalità sola lettura

**[opencontent/ocsensorapi changes between 7.2.5 and 7.2.6](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.5...7.2.6)**
* Use SENSOR_READ_ONLY env to disable all permission grants


## [3.0.45](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.44...3.0.45) - 2023-03-20


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.261.0  | 3.261.15 | [...](https://github.com/aws/aws-sdk-php/compare/3.261.0...3.261.15)                            |
| google/apiclient          | cabac12  | 5ed4edc  | [...](https://github.com/googleapis/google-api-php-client/compare/cabac12...5ed4edc)            |
| google/apiclient-services | v0.289.0 | v0.291.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.289.0...v0.291.0) |
| opencontent/ocinstaller   | 8bdf978  | 4c2986e  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/8bdf978...4c2986e)                 |
| opencontent/ocopendata-ls | 2.28.2   | 2.28.3   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.28.2...2.28.3)                    |
| opencontent/ocsensor-ls   | 7.3.2    | 7.4.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.3.2...7.4.0)                        |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 8bdf978 and 4c2986e](https://github.com/OpencontentCoop/ocinstaller/compare/8bdf978...4c2986e)**
* Avoid missing node error Force regenerate class cache
* Add reset content fields
* Avoid published version duplication
* Allow override installer var invoking script

**[opencontent/ocopendata-ls changes between 2.28.2 and 2.28.3](https://github.com/OpencontentCoop/ocopendata/compare/2.28.2...2.28.3)**
* Fix empty field validation

**[opencontent/ocsensor-ls changes between 7.3.2 and 7.4.0](https://github.com/OpencontentCoop/ocsensor/compare/7.3.2...7.4.0)**
* Introduce la possibilità di embeddare il widget satisfy senza usare tagmanager


## [3.0.44](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.43...3.0.44) - 2023-02-28


#### Code dependencies
| Changes                          | From     | To       | Compare                                                                                         |
|----------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-crt-php                  | v1.0.2   | v1.0.4   | [...](https://github.com/awslabs/aws-crt-php/compare/v1.0.2...v1.0.4)                           |
| aws/aws-sdk-php                  | 3.258.2  | 3.261.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.258.2...3.261.0)                             |
| google/apiclient                 | f2791c2  | cabac12  | [...](https://github.com/googleapis/google-api-php-client/compare/f2791c2...cabac12)            |
| google/apiclient-services        | v0.285.0 | v0.289.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.285.0...v0.289.0) |
| http-interop/http-factory-guzzle |          | 1.1.1    |                                                                                                 |
| opencontent/ocinstaller          | 8d1682a  | 8bdf978  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/8d1682a...8bdf978)                 |
| opencontent/ocsensor-ls          | 7.3.1    | 7.3.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.3.1...7.3.2)                        |
| opencontent/ocsiracauth-ls       | 1.6.2    | 1.6.3    | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.6.2...1.6.3)                     |
| php-http/discovery               | 1.14.3   | 1.x-dev  | [...](https://github.com/php-http/discovery/compare/1.14.3...1.x-dev)                           |
| predis/predis                    | v2.1.1   | v2.x-dev | [...](https://github.com/predis/predis/compare/v2.1.1...v2.x-dev)                               |
| zetacomponents/archive           | 1.5      | 1.5.1    | [...](https://github.com/zetacomponents/Archive/compare/1.5...1.5.1)                            |



## [2.6.12](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.11...2.6.12) - 2023-02-21


#### Code dependencies
| Changes                    | From  | To    | Compare                                                                     |
|----------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocsiracauth-ls | 1.2.0 | 1.6.3 | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.2.0...1.6.3) |



## [3.0.43](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.42...3.0.43) - 2023-02-03


#### Code dependencies
| Changes                          | From       | To       | Compare                                                                                         |
|----------------------------------|------------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.257.6    | 3.258.2  | [...](https://github.com/aws/aws-sdk-php/compare/3.257.6...3.258.2)                             |
| google/apiclient-services        | v0.284.0   | v0.285.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.284.0...v0.285.0) |
| opencontent/occaslogin-ls        | 1.2.0      | 1.2.1    | [...](https://github.com/OpencontentCoop/occaslogin/compare/1.2.0...1.2.1)                      |
| opencontent/ocopendata_forms-ls  | 1.6.11     | 1.6.12   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.11...1.6.12)              |
| predis/predis                    | v2.2.x-dev | v2.1.1   | [...](https://github.com/predis/predis/compare/v2.2.x-dev...v2.1.1)                             |
| symfony/polyfill-ctype           | 5bbc823    | ea208ce  | [...](https://github.com/symfony/polyfill-ctype/compare/5bbc823...ea208ce)                      |
| symfony/polyfill-intl-idn        | 639084e    | ecaafce  | [...](https://github.com/symfony/polyfill-intl-idn/compare/639084e...ecaafce)                   |
| symfony/polyfill-intl-normalizer | 19bd1e4    | 8c4ad05  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/19bd1e4...8c4ad05)            |
| symfony/polyfill-mbstring        | 8ad114f    | f9c7aff  | [...](https://github.com/symfony/polyfill-mbstring/compare/8ad114f...f9c7aff)                   |
| symfony/polyfill-php72           | 869329b    | 70f4aeb  | [...](https://github.com/symfony/polyfill-php72/compare/869329b...70f4aeb)                      |
| symfony/polyfill-php73           | 9e8ecb5    | fe2f306  | [...](https://github.com/symfony/polyfill-php73/compare/9e8ecb5...fe2f306)                      |
| symfony/polyfill-php80           | 7a6ff3f    | 6caa573  | [...](https://github.com/symfony/polyfill-php80/compare/7a6ff3f...6caa573)                      |


Relevant changes by repository:

**[opencontent/occaslogin-ls changes between 1.2.0 and 1.2.1](https://github.com/OpencontentCoop/occaslogin/compare/1.2.0...1.2.1)**
* Find existing user by mail optionally

**[opencontent/ocopendata_forms-ls changes between 1.6.11 and 1.6.12](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.11...1.6.12)**
* Corregge un problema di compatibilità di bootstrap nella navigazione a tab dei form


## [3.0.42](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.41...3.0.42) - 2023-01-24


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.257.4  | 3.257.6  | [...](https://github.com/aws/aws-sdk-php/compare/3.257.4...3.257.6)                             |
| google/apiclient-services | v0.283.0 | v0.284.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.283.0...v0.284.0) |
| opencontent/ocsensor-ls   | 7.3.0    | 7.3.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.3.0...7.3.1)                        |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.3.0 and 7.3.1](https://github.com/OpencontentCoop/ocsensor/compare/7.3.0...7.3.1)**
* Corregge la funzione di duplicazione che non copiava correttamente le immagini e i file


## [3.0.41](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.40...3.0.41) - 2023-01-20


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                  |
|-------------------------|---------|---------|--------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.257.1 | 3.257.4 | [...](https://github.com/aws/aws-sdk-php/compare/3.257.1...3.257.4)      |
| opencontent/ocsensor-ls | 7.2.4   | 7.3.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.2.4...7.3.0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.2.4 and 7.3.0](https://github.com/OpencontentCoop/ocsensor/compare/7.2.4...7.3.0)**
* Introduce un login handler per inibire il login integrato agli utenti reporter


## [3.0.40](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.39...3.0.40) - 2023-01-17


#### Code dependencies
| Changes                     | From     | To         | Compare                                                                                         |
|-----------------------------|----------|------------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.253.0  | 3.257.1    | [...](https://github.com/aws/aws-sdk-php/compare/3.253.0...3.257.1)                             |
| google/apiclient            | b2624d2  | f2791c2    | [...](https://github.com/googleapis/google-api-php-client/compare/b2624d2...f2791c2)            |
| google/apiclient-services   | v0.278.0 | v0.283.0   | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.278.0...v0.283.0) |
| opencontent/occustomfind-ls | 2.4.4    | 2.4.6      | [...](https://github.com/OpencontentCoop/occustomfind/compare/2.4.4...2.4.6)                    |
| opencontent/ocinstaller     | 5fd5bdd  | 8d1682a    | [...](https://github.com/OpencontentCoop/ocinstaller/compare/5fd5bdd...8d1682a)                 |
| opencontent/ocsiracauth-ls  | 1.6.1    | 1.6.2      | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.6.1...1.6.2)                     |
| opencontent/openpa-ls       | 3.16.2   | 3.18.1     | [...](https://github.com/OpencontentCoop/openpa/compare/3.16.2...3.18.1)                        |
| predis/predis               | bc5892e  | v2.2.x-dev | [...](https://github.com/predis/predis/compare/bc5892e...v2.2.x-dev)                            |


Relevant changes by repository:

**[opencontent/occustomfind-ls changes between 2.4.4 and 2.4.6](https://github.com/OpencontentCoop/occustomfind/compare/2.4.4...2.4.6)**
* Add data-bs-toggle to tabs nav
* Add data-bs-toggle to tabs nav

**[opencontent/ocinstaller changes between 5fd5bdd and 8d1682a](https://github.com/OpencontentCoop/ocinstaller/compare/5fd5bdd...8d1682a)**
* Minor bug fixes
* Add missing tag description setter in tag from csv
* Add sync with local feature

**[opencontent/ocsiracauth-ls changes between 1.6.1 and 1.6.2](https://github.com/OpencontentCoop/ocsiracauth/compare/1.6.1...1.6.2)**
* Bugfix

**[opencontent/openpa-ls changes between 3.16.2 and 3.18.1](https://github.com/OpencontentCoop/openpa/compare/3.16.2...3.18.1)**
* Aggiunge nuove voci nella matrice dei contatti
* Permette di configurare un endpoint custom per servire le immagini da AWS S3
* Permette di configurare un firewall per l'invio mail basato su bounce collector
* Introduce un cron dedicato all'aggiornamento delle liste di newsletter in base al bounce collector se configurato


## [3.0.39](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.38...3.0.39) - 2022-12-13


#### Code dependencies
| Changes                    | From     | To       | Compare                                                                                         |
|----------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.252.3  | 3.253.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.252.3...3.253.0)                             |
| google/apiclient-services  | v0.277.0 | v0.278.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.277.0...v0.278.0) |
| opencontent/ocsiracauth-ls | 1.6.0    | 1.6.1    | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.6.0...1.6.1)                     |
| predis/predis              | d2b05ff  | bc5892e  | [...](https://github.com/predis/predis/compare/d2b05ff...bc5892e)                               |


Relevant changes by repository:

**[opencontent/ocsiracauth-ls changes between 1.6.0 and 1.6.1](https://github.com/OpencontentCoop/ocsiracauth/compare/1.6.0...1.6.1)**
* Corregge un errore nel redirect del modulo slo


## [3.0.38](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.37...3.0.38) - 2022-12-07


#### Code dependencies
| Changes                      | From    | To      | Compare                                                                       |
|------------------------------|---------|---------|-------------------------------------------------------------------------------|
| opencontent/ocsensor-ls      | 7.2.3   | 7.2.4   | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.2.3...7.2.4)      |
| opencontent/openpa_sensor-ls | 7.1.2   | 7.1.3   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/7.1.2...7.1.3) |
| predis/predis                | e24ecbb | d2b05ff | [...](https://github.com/predis/predis/compare/e24ecbb...d2b05ff)             |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.2.3 and 7.2.4](https://github.com/OpencontentCoop/ocsensor/compare/7.2.3...7.2.4)**
* Introduce uno script di utilità per bloccare i commenti degli operatori

**[opencontent/openpa_sensor-ls changes between 7.1.2 and 7.1.3](https://github.com/OpencontentCoop/openpa_sensor/compare/7.1.2...7.1.3)**
* Corregge un errore sulla definizione del site url in caso di ambiente multilingua


## [3.0.37](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.36...3.0.37) - 2022-12-07


#### Code dependencies
| Changes                      | From     | To       | Compare                                                                                         |
|------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.247.1  | 3.252.3  | [...](https://github.com/aws/aws-sdk-php/compare/3.247.1...3.252.3)                             |
| google/apiclient-services    | v0.275.0 | v0.277.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.275.0...v0.277.0) |
| opencontent/oci18n           | c67c37e  | e34b8d7  | [...](https://github.com/OpencontentCoop/oci18n/compare/c67c37e...e34b8d7)                      |
| opencontent/ocsensor-ls      | 7.2.1    | 7.2.3    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.2.1...7.2.3)                        |
| opencontent/ocsensorapi      | 7.2.3    | 7.2.5    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.3...7.2.5)                     |
| opencontent/openpa_sensor-ls | 7.1.1    | 7.1.2    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/7.1.1...7.1.2)                   |
| predis/predis                | 169604b  | e24ecbb  | [...](https://github.com/predis/predis/compare/169604b...e24ecbb)                               |
| zetacomponents/base          | 1.9.3    | 1.9.4    | [...](https://github.com/zetacomponents/Base/compare/1.9.3...1.9.4)                             |
| zetacomponents/database      | 1.5.2    | 1.5.3    | [...](https://github.com/zetacomponents/Database/compare/1.5.2...1.5.3)                         |


Relevant changes by repository:

**[opencontent/oci18n changes between c67c37e and e34b8d7](https://github.com/OpencontentCoop/oci18n/compare/c67c37e...e34b8d7)**
* Add csv->class util script

**[opencontent/ocsensor-ls changes between 7.2.1 and 7.2.3](https://github.com/OpencontentCoop/ocsensor/compare/7.2.1...7.2.3)**
* Permette di configurare una black list di indirizzi email per bloccare l'esecuzione di webhook
* Evita un errore che sei verificava nella pagina statistica in fase di prima installazione
* Semplifica le visualizzazioni di modifica profilo
* Introduce alcuni script di utilità
* Corregge un problema nella generazione del message id

**[opencontent/ocsensorapi changes between 7.2.3 and 7.2.5](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.3...7.2.5)**
* Add check permission api get sensor/posts/{id}
* Remove message id from mail notifications

**[opencontent/openpa_sensor-ls changes between 7.1.1 and 7.1.2](https://github.com/OpencontentCoop/openpa_sensor/compare/7.1.1...7.1.2)**
* Corregge un errore sulla definizione del site url in caso di ambiente multilingua


## [3.0.36](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.35...3.0.36) - 2022-11-22


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                  |
|-------------------------|---------|---------|--------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.247.0 | 3.247.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.247.0...3.247.1)      |
| opencontent/ocsensor-ls | 7.2.0   | 7.2.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.2.0...7.2.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.2.0 and 7.2.1](https://github.com/OpencontentCoop/ocsensor/compare/7.2.0...7.2.1)**
* Rimuove le chiamate a fonts.googleapis.com


## [3.0.35](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.34...3.0.35) - 2022-11-22


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.245.1  | 3.247.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.245.1...3.247.0)                             |
| google/apiclient-services | v0.274.0 | v0.275.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.274.0...v0.275.0) |
| opencontent/occaslogin-ls | 1.1.4    | 1.2.0    | [...](https://github.com/OpencontentCoop/occaslogin/compare/1.1.4...1.2.0)                      |
| predis/predis             | 94bd54b  | 169604b  | [...](https://github.com/predis/predis/compare/94bd54b...169604b)                               |


Relevant changes by repository:

**[opencontent/occaslogin-ls changes between 1.1.4 and 1.2.0](https://github.com/OpencontentCoop/occaslogin/compare/1.1.4...1.2.0)**
* Add siag handler
* Bugfix in Siag validation request


## [3.0.34](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.33...3.0.34) - 2022-11-18


#### Code dependencies
| Changes                      | From    | To      | Compare                                                                       |
|------------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.242.1 | 3.245.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.242.1...3.245.1)           |
| opencontent/openpa_sensor-ls | 7.1.0   | 7.1.1   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/7.1.0...7.1.1) |
| predis/predis                | bdebad7 | 94bd54b | [...](https://github.com/predis/predis/compare/bdebad7...94bd54b)             |


Relevant changes by repository:

**[opencontent/openpa_sensor-ls changes between 7.1.0 and 7.1.1](https://github.com/OpencontentCoop/openpa_sensor/compare/7.1.0...7.1.1)**
* Esegue il workflow di post pubblicazione della segnalazione con un utente con privilegi di superuser


## [3.0.33](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.32...3.0.33) - 2022-11-14


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.242.0  | 3.242.1  | [...](https://github.com/aws/aws-sdk-php/compare/3.242.0...3.242.1)                             |
| google/apiclient-services | v0.273.0 | v0.274.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.273.0...v0.274.0) |
| opencontent/ocsensorapi   | 7.2.2    | 7.2.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.2...7.2.3)                     |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 7.2.2 and 7.2.3](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.2...7.2.3)**
* Fix loadPostByUUID in api controller


## [3.0.32](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.31...3.0.32) - 2022-11-10



#### Installer
- Add homepage block module

#### Code dependencies
| Changes                          | From     | To       | Compare                                                                                         |
|----------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.239.1  | 3.242.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.239.1...3.242.0)                             |
| google/apiclient                 | 88cc63c  | b2624d2  | [...](https://github.com/googleapis/google-api-php-client/compare/88cc63c...b2624d2)            |
| google/apiclient-services        | v0.271.0 | v0.273.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.271.0...v0.273.0) |
| opencontent/ocsensor-ls          | 7.1.1    | 7.2.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.1.1...7.2.0)                        |
| opencontent/ocsensorapi          | 7.2.1    | 7.2.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.1...7.2.2)                     |
| opencontent/openpa-ls            | 3.16.1   | 3.16.2   | [...](https://github.com/OpencontentCoop/openpa/compare/3.16.1...3.16.2)                        |
| opencontent/openpa_sensor-ls     | 7.0.0    | 7.1.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/7.0.0...7.1.0)                   |
| predis/predis                    | 1a411d2  | bdebad7  | [...](https://github.com/predis/predis/compare/1a411d2...bdebad7)                               |
| symfony/polyfill-ctype           | 6fd1b9a  | 5bbc823  | [...](https://github.com/symfony/polyfill-ctype/compare/6fd1b9a...5bbc823)                      |
| symfony/polyfill-intl-idn        | 59a8d27  | 639084e  | [...](https://github.com/symfony/polyfill-intl-idn/compare/59a8d27...639084e)                   |
| symfony/polyfill-intl-normalizer | 219aa36  | 19bd1e4  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/219aa36...19bd1e4)            |
| symfony/polyfill-mbstring        | 9344f9c  | 8ad114f  | [...](https://github.com/symfony/polyfill-mbstring/compare/9344f9c...8ad114f)                   |
| symfony/polyfill-php72           | bf44a9f  | 869329b  | [...](https://github.com/symfony/polyfill-php72/compare/bf44a9f...869329b)                      |
| symfony/polyfill-php73           | e440d35  | 9e8ecb5  | [...](https://github.com/symfony/polyfill-php73/compare/e440d35...9e8ecb5)                      |
| symfony/polyfill-php80           | cfa0ae9  | 7a6ff3f  | [...](https://github.com/symfony/polyfill-php80/compare/cfa0ae9...7a6ff3f)                      |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.1.1 and 7.2.0](https://github.com/OpencontentCoop/ocsensor/compare/7.1.1...7.2.0)**
* Introduce un sistema per customizzare la homepage degli utenti
* Utilizza il sistema per customizzazione della homepage per generare il menu Segnala
* Ignora il campo obbligatorio di inserimento categoria se la segnalazione viene inserita per conto di un altro utente
* Corregge un errore nella registrazione di evento 'read' in timeline

**[opencontent/ocsensorapi changes between 7.2.1 and 7.2.2](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.1...7.2.2)**
* Remove reporter as super user in SuperUserPostFixListener

**[opencontent/openpa-ls changes between 3.16.1 and 3.16.2](https://github.com/OpencontentCoop/openpa/compare/3.16.1...3.16.2)**
* Include il remote id nella generazione dei tree menu

**[opencontent/openpa_sensor-ls changes between 7.0.0 and 7.1.0](https://github.com/OpencontentCoop/openpa_sensor/compare/7.0.0...7.1.0)**
* Refactor del codice che produce il menu principale


## [3.0.31](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.30...3.0.31) - 2022-10-19


#### Code dependencies
| Changes                        | From     | To       | Compare                                                                                         |
|--------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.237.2  | 3.239.1  | [...](https://github.com/aws/aws-sdk-php/compare/3.237.2...3.239.1)                             |
| composer/installers            | c29dc4b  | 2a91702  | [...](https://github.com/composer/installers/compare/c29dc4b...2a91702)                         |
| google/apiclient-services      | v0.269.0 | v0.271.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.269.0...v0.271.0) |
| opencontent/occodicefiscale-ls | ac8f352  | 280d3c1  | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/ac8f352...280d3c1)             |
| opencontent/ocsensor-ls        | 7.1.0    | 7.1.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.1.0...7.1.1)                        |
| opencontent/ocsensorapi        | 7.2.0    | 7.2.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.0...7.2.1)                     |
| opencontent/ocsiracauth-ls     | 1.5.0    | 1.6.0    | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.5.0...1.6.0)                     |
| predis/predis                  | bb6c1a7  | 1a411d2  | [...](https://github.com/predis/predis/compare/bb6c1a7...1a411d2)                               |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between ac8f352 and 280d3c1](https://github.com/OpencontentCoop/occodicefiscale/compare/ac8f352...280d3c1)**
* Esegue il trim del valore da inserire in db

**[opencontent/ocsensor-ls changes between 7.1.0 and 7.1.1](https://github.com/OpencontentCoop/ocsensor/compare/7.1.0...7.1.1)**
* Customizza i template per la verifica della mail con accesso ocsirac

**[opencontent/ocsensorapi changes between 7.2.0 and 7.2.1](https://github.com/OpencontentCoop/ocsensorapi/compare/7.2.0...7.2.1)**
* Add auto response in SuperUserPostFixListener
* Bug fix in SuperUserPostFixListener

**[opencontent/ocsiracauth-ls changes between 1.5.0 and 1.6.0](https://github.com/OpencontentCoop/ocsiracauth/compare/1.5.0...1.6.0)**
* Permette la concatenazione di variabili nella mappatura degli attributi
* Utilizza una mail di verifica per disambiguare account utente


## [3.0.30](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.29...3.0.30) - 2022-10-04


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.236.1  | 3.237.2  | [...](https://github.com/aws/aws-sdk-php/compare/3.236.1...3.237.2)                             |
| google/apiclient-services | v0.268.0 | v0.269.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.268.0...v0.269.0) |
| opencontent/ocsensor-ls   | 7.0.5    | 7.1.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.0.5...7.1.0)                        |
| opencontent/ocsensorapi   | 7.1.0    | 7.2.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.1.0...7.2.0)                     |
| php-http/client-common    | 2.5.0    | 2.6.0    | [...](https://github.com/php-http/client-common/compare/2.5.0...2.6.0)                          |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.0.5 and 7.1.0](https://github.com/OpencontentCoop/ocsensor/compare/7.0.5...7.1.0)**
* Introduce il trigger on_add_attachment
* Permette di nascondere il nome degli utenti
* Evidenzia le segnalazioni interne anche nelle liste di segnalazioni

**[opencontent/ocsensorapi changes between 7.1.0 and 7.2.0](https://github.com/OpencontentCoop/ocsensorapi/compare/7.1.0...7.2.0)**
* Use setting HideUserNames in post user aware serializer
* Add date post filters Accept url as attachment file payload
* Add a post audit if the user is authenticated with an external system


## [3.0.29](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.28...3.0.29) - 2022-09-28


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                     |
|-------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocsensorapi | 7.0.4 | 7.1.0 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.4...7.1.0) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 7.0.4 and 7.1.0](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.4...7.1.0)**
* Fix post api query filter and add more filters


## [3.0.28](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.27...3.0.28) - 2022-09-28


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.235.14 | 3.236.1  | [...](https://github.com/aws/aws-sdk-php/compare/3.235.14...3.236.1)                            |
| google/apiclient-services | v0.267.0 | v0.268.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.267.0...v0.268.0) |
| opencontent/ocsensor-ls   | 7.0.4    | 7.0.5    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.0.4...7.0.5)                        |
| opencontent/ocsensorapi   | 7.0.3    | 7.0.4    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.3...7.0.4)                     |
| predis/predis             | 42c33a4  | bb6c1a7  | [...](https://github.com/predis/predis/compare/42c33a4...bb6c1a7)                               |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.0.4 and 7.0.5](https://github.com/OpencontentCoop/ocsensor/compare/7.0.4...7.0.5)**
* Corregge un bug nell'indicizzazione delle timeline
* Riduce l'uso del motore di ricerca nelle richieste api di interfaccia

**[opencontent/ocsensorapi changes between 7.0.3 and 7.0.4](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.3...7.0.4)**
* Fix binary api url encoding
* Ignore empty file or image from mail parser payload in creation api


## [3.0.27](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.26...3.0.27) - 2022-09-23
- Remove new relic agent install from Dockerfile


#### Installer
- Fix default logo and banner image paths

#### Code dependencies
| Changes                          | From     | To       | Compare                                                                                         |
|----------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.232.3  | 3.235.14 | [...](https://github.com/aws/aws-sdk-php/compare/3.232.3...3.235.14)                            |
| composer/installers              | af93ba6  | c29dc4b  | [...](https://github.com/composer/installers/compare/af93ba6...c29dc4b)                         |
| friendsofsymfony/http-cache      | 2.14.1   | 2.x-dev  | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/2.14.1...2.x-dev)                |
| google/apiclient                 | 654c0e2  | 88cc63c  | [...](https://github.com/googleapis/google-api-php-client/compare/654c0e2...88cc63c)            |
| google/apiclient-services        | v0.261.0 | v0.267.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.261.0...v0.267.0) |
| guzzlehttp/promises              | a872440  | b94b280  | [...](https://github.com/guzzle/promises/compare/a872440...b94b280)                             |
| opencontent/ocbootstrap-ls       | 1.10.13  | 1.10.14  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.13...1.10.14)                 |
| opencontent/ocrecaptcha-ls       | 1.5      | 1.5.1    | [...](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.5...1.5.1)                       |
| opencontent/ocsensor-ls          | 7.0.2    | 7.0.4    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.0.2...7.0.4)                        |
| opencontent/ocsensorapi          | 7.0.1    | 7.0.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.1...7.0.3)                     |
| opencontent/ocsocialuser-ls      | 1.8.1    | 1.8.2    | [...](https://github.com/OpencontentCoop/ocsocialuser/compare/1.8.1...1.8.2)                    |
| opencontent/openpa_theme_2014-ls | 2.16.0   | 2.16.3   | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.16.0...2.16.3)             |
| predis/predis                    | dd4ddf6  | 42c33a4  | [...](https://github.com/predis/predis/compare/dd4ddf6...42c33a4)                               |
| zetacomponents/mail              | 1.9.2    | 1.9.4    | [...](https://github.com/zetacomponents/Mail/compare/1.9.2...1.9.4)                             |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.13 and 1.10.14](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.13...1.10.14)**
* Avoid google recaptcha marketing cookies

**[opencontent/ocrecaptcha-ls changes between 1.5 and 1.5.1](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.5...1.5.1)**
* Avoid google recaptcha marketing cookies

**[opencontent/ocsensor-ls changes between 7.0.2 and 7.0.4](https://github.com/OpencontentCoop/ocsensor/compare/7.0.2...7.0.4)**
* Permette di configurare i gruppi di notifica da file di configurazione
* Imposta una policy ad hoc per l'accesso alle segnalazioni critiche
* Corregge un problema di navigazione in sensor/posts
* Corregge alcune traduzioni mancanti

**[opencontent/ocsensorapi changes between 7.0.1 and 7.0.3](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.1...7.0.3)**
* Add super user field in api user serialization
* Fix encoding file api url

**[opencontent/ocsocialuser-ls changes between 1.8.1 and 1.8.2](https://github.com/OpencontentCoop/ocsocialuser/compare/1.8.1...1.8.2)**
* Corregge alcune stringhe di traduzione in greco
* Avoid google recaptcha marketing cookies

**[opencontent/openpa_theme_2014-ls changes between 2.16.0 and 2.16.3](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.16.0...2.16.3)**
* Avoid google recaptcha marketing cookies
* Remove fonts.googleapis.com  and fonts.gstatic.com external calls
* Add font awesome fonts


## [3.0.26](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.25...3.0.26) - 2022-08-09


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.231.18 | 3.232.3  | [...](https://github.com/aws/aws-sdk-php/compare/3.231.18...3.232.3)                            |
| google/apiclient-services | v0.260.0 | v0.261.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.260.0...v0.261.0) |
| opencontent/ocsensor-ls   | 7.0.1    | 7.0.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.0.1...7.0.2)                        |
| opencontent/openpa-ls     | 3.15.0   | 3.16.1   | [...](https://github.com/OpencontentCoop/openpa/compare/3.15.0...3.16.1)                        |
| predis/predis             | a602fc7  | dd4ddf6  | [...](https://github.com/predis/predis/compare/a602fc7...dd4ddf6)                               |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.0.1 and 7.0.2](https://github.com/OpencontentCoop/ocsensor/compare/7.0.1...7.0.2)**
* Corregge un bug sul selettore degli operatori

**[opencontent/openpa-ls changes between 3.15.0 and 3.16.1](https://github.com/OpencontentCoop/openpa/compare/3.15.0...3.16.1)**
* Permette di configurare lo storage del cluster della cache su nfs e della var/storage in S3-like
* Corregge un problema di lettura delle configurazioni nel clustering in nfs


## [3.0.25](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.25-rc1-nr...3.0.25) - 2022-08-02
- Update publiccode.yml
- aggiunte feature PNRR
- Upload New File

#### Code dependencies
| Changes                                | From     | To       | Compare                                                                                         |
|----------------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                        | 3.231.5  | 3.231.18 | [...](https://github.com/aws/aws-sdk-php/compare/3.231.5...3.231.18)                            |
| google/apiclient                       | a69131b  | 654c0e2  | [...](https://github.com/googleapis/google-api-php-client/compare/a69131b...654c0e2)            |
| google/apiclient-services              | v0.257.0 | v0.260.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.257.0...v0.260.0) |
| guzzlehttp/promises                    | 43453bf  | a872440  | [...](https://github.com/guzzle/promises/compare/43453bf...a872440)                             |
| intervention/image                     |          | 2.7.2    |                                                                                                 |
| lasserafn/php-initial-avatar-generator |          | 4.2.1    |                                                                                                 |
| lasserafn/php-initials                 |          | 3.1      |                                                                                                 |
| lasserafn/php-string-script-language   |          | 0.3      |                                                                                                 |
| meyfa/php-svg                          |          | v0.9.1   |                                                                                                 |
| opencontent/ocsensor-ls                | 7.0.0    | 7.0.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/7.0.0...7.0.1)                        |
| opencontent/ocsensorapi                | 7.0.0    | 7.0.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.0...7.0.1)                     |
| overtrue/pinyin                        |          | 4.x-dev  |                                                                                                 |
| zetacomponents/cache                   | 1.6.1    | 1.6.2    | [...](https://github.com/zetacomponents/Cache/compare/1.6.1...1.6.2)                            |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7.0.0 and 7.0.1](https://github.com/OpencontentCoop/ocsensor/compare/7.0.0...7.0.1)**
* Rimuove dalla selezione i gruppi disabilitati
* Incorpora il motore di rendering degli avatar
* Aggiunge alcune info alla pagina dedicata all'utente

**[opencontent/ocsensorapi changes between 7.0.0 and 7.0.1](https://github.com/OpencontentCoop/ocsensorapi/compare/7.0.0...7.0.1)**
* Reduce payload in search posts
* Fix user status stat


## [3.0.25-rc1-nr](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.24-nr...3.0.25-rc1-nr) - 2022-07-14


#### Code dependencies
| Changes                      | From     | To       | Compare                                                                                         |
|------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.229.2  | 3.231.5  | [...](https://github.com/aws/aws-sdk-php/compare/3.229.2...3.231.5)                             |
| friendsofsymfony/http-cache  | 2.13.0   | 2.14.1   | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/2.13.0...2.14.1)                 |
| google/apiclient-services    | v0.256.0 | v0.257.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.256.0...v0.257.0) |
| opencontent/ocopendata-ls    | 2.28.1   | 2.28.2   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.28.1...2.28.2)                    |
| opencontent/ocsensor-ls      | 6.7.4    | 7.0.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.7.4...7.0.0)                        |
| opencontent/ocsensorapi      | 6.7.1    | 7.0.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.7.1...7.0.0)                     |
| opencontent/openpa_sensor-ls | 6.1.0    | 7.0.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/6.1.0...7.0.0)                   |
| php-http/discovery           | 1.14.2   | 1.14.3   | [...](https://github.com/php-http/discovery/compare/1.14.2...1.14.3)                            |
| predis/predis                | 4ec74cc  | a602fc7  | [...](https://github.com/predis/predis/compare/4ec74cc...a602fc7)                               |
| psr/http-factory             | 36fa03d  | 5a4f141  | [...](https://github.com/php-fig/http-factory/compare/36fa03d...5a4f141)                        |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.28.1 and 2.28.2](https://github.com/OpencontentCoop/ocopendata/compare/2.28.1...2.28.2)**
* Fix block items priority in set Page api

**[opencontent/ocsensor-ls changes between 6.7.4 and 7.0.0](https://github.com/OpencontentCoop/ocsensor/compare/6.7.4...7.0.0)**
* Migliora l’utilizzo delle cache

**[opencontent/ocsensorapi changes between 6.7.1 and 7.0.0](https://github.com/OpencontentCoop/ocsensorapi/compare/6.7.1...7.0.0)**
* no message
* no message
* no message
* Merge branch 'master' into refactor-refresh  Conflicts: src/Opencontent/Sensor/Legacy/MessageService.php src/Opencontent/Sensor/Legacy/OperatorService.php
* Refactorize post refresh strategy

**[opencontent/openpa_sensor-ls changes between 6.1.0 and 7.0.0](https://github.com/OpencontentCoop/openpa_sensor/compare/6.1.0...7.0.0)**
* Svuota le cache dei gruppi di utenti all'aggiornamento/inserimento


## [3.0.24-nr](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.24...3.0.24-nr) - 2022-07-11
- Add new relic daemon



## [3.0.24](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.23...3.0.24) - 2022-07-06
- Update changelog and publiccode

#### Code dependencies
| Changes                        | From     | To       | Compare                                                                                         |
|--------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.225.3  | 3.229.2  | [...](https://github.com/aws/aws-sdk-php/compare/3.225.3...3.229.2)                             |
| google/apiclient               | 5738766  | a69131b  | [...](https://github.com/googleapis/google-api-php-client/compare/5738766...a69131b)            |
| google/apiclient-services      | v0.253.0 | v0.256.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.253.0...v0.256.0) |
| guzzlehttp/promises            | fe752ae  | 43453bf  | [...](https://github.com/guzzle/promises/compare/fe752ae...43453bf)                             |
| opencontent/ocsensor-ls        | 6.7.0    | 6.7.4    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.7.0...6.7.4)                        |
| opencontent/ocsensorapi        | 6.7.0    | 6.7.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.7.0...6.7.1)                     |
| opencontent/ocwebhookserver-ls | 1.1.5    | 1.2.4    | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.5...1.2.4)                 |
| predis/predis                  | 7d7b341  | 4ec74cc  | [...](https://github.com/predis/predis/compare/7d7b341...4ec74cc)                               |
| psr/event-dispatcher           | aa4f89e  | e275e2d  | [...](https://github.com/php-fig/event-dispatcher/compare/aa4f89e...e275e2d)                    |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.7.0 and 6.7.4](https://github.com/OpencontentCoop/ocsensor/compare/6.7.0...6.7.4)**
* Migliora la visualizzazione dei filtri nelle segnalazioni critiche
* Aggiunge il campo indirizzo nell'export in csv
* Migliora la visualizzazione dei suggerimenti faq
* Corregge le stringhe di traduzione degli errori della gui
* Introduce alcuni filtri nella versione semplificata della dashboard
* Aggiunge il campo is_internal nella tabella di appoggio timeline
* Introduce il modulo pubblico sensor/metrics
* Evita di riassegnare la segnalazione a un riferimento per il cittadino
* Impedisce l'assegnazione a un gruppo non abilitato
* Evidenzia le segnalazioni interne in Inbox (se abilitato)

**[opencontent/ocsensorapi changes between 6.7.0 and 6.7.1](https://github.com/OpencontentCoop/ocsensorapi/compare/6.7.0...6.7.1)**
* Add sensor_group/avoid_assignment field

**[opencontent/ocwebhookserver-ls changes between 1.1.5 and 1.2.4](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.5...1.2.4)**
* Add delete workflow and trigger
* Check if trigger class exists before register
* Add ezpevent notify on push success and fail
* Add has_webhooks util script
* Add execution metrics


## [3.0.23](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.22...3.0.23) - 2022-06-14


#### Code dependencies
| Changes                          | From     | To       | Compare                                                                                         |
|----------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.222.8  | 3.225.3  | [...](https://github.com/aws/aws-sdk-php/compare/3.222.8...3.225.3)                             |
| clue/stream-filter               | v1.6.0   | 1.x-dev  | [...](https://github.com/clue/stream-filter/compare/v1.6.0...1.x-dev)                           |
| google/apiclient                 | a9a27c2  | 5738766  | [...](https://github.com/googleapis/google-api-php-client/compare/a9a27c2...5738766)            |
| google/apiclient-services        | v0.248.0 | v0.253.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.248.0...v0.253.0) |
| opencontent/occustomfind-ls      | 2.4.3    | 2.4.4    | [...](https://github.com/OpencontentCoop/occustomfind/compare/2.4.3...2.4.4)                    |
| opencontent/ocsensor-ls          | 6.6.6    | 6.7.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.6.6...6.7.0)                        |
| opencontent/ocsensorapi          | 6.6.2    | 6.7.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.6.2...6.7.0)                     |
| opencontent/ocsocialdesign-ls    | 1.6.3    | 1.6.5    | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.3...1.6.5)                  |
| opencontent/openpa-ls            | 3.13.7   | 3.15.0   | [...](https://github.com/OpencontentCoop/openpa/compare/3.13.7...3.15.0)                        |
| opencontent/openpa_theme_2014-ls | 2.15.4   | 2.16.0   | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.4...2.16.0)             |
| paragonie/constant_time_encoding | v2.5.0   | v2.6.3   | [...](https://github.com/paragonie/constant_time_encoding/compare/v2.5.0...v2.6.3)              |
| php-http/discovery               | 1.14.1   | 1.14.2   | [...](https://github.com/php-http/discovery/compare/1.14.1...1.14.2)                            |
| predis/predis                    | a2449da  | 7d7b341  | [...](https://github.com/predis/predis/compare/a2449da...7d7b341)                               |
| symfony/polyfill-ctype           | v1.25.0  | 6fd1b9a  | [...](https://github.com/symfony/polyfill-ctype/compare/v1.25.0...6fd1b9a)                      |
| symfony/polyfill-intl-idn        | v1.25.0  | 59a8d27  | [...](https://github.com/symfony/polyfill-intl-idn/compare/v1.25.0...59a8d27)                   |
| symfony/polyfill-intl-normalizer | v1.25.0  | 219aa36  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/v1.25.0...219aa36)            |
| symfony/polyfill-mbstring        | v1.25.0  | 9344f9c  | [...](https://github.com/symfony/polyfill-mbstring/compare/v1.25.0...9344f9c)                   |
| symfony/polyfill-php72           | v1.25.0  | bf44a9f  | [...](https://github.com/symfony/polyfill-php72/compare/v1.25.0...bf44a9f)                      |
| symfony/polyfill-php73           | v1.25.0  | e440d35  | [...](https://github.com/symfony/polyfill-php73/compare/v1.25.0...e440d35)                      |
| symfony/polyfill-php80           | v1.25.0  | cfa0ae9  | [...](https://github.com/symfony/polyfill-php80/compare/v1.25.0...cfa0ae9)                      |


Relevant changes by repository:

**[opencontent/occustomfind-ls changes between 2.4.3 and 2.4.4](https://github.com/OpencontentCoop/occustomfind/compare/2.4.3...2.4.4)**
* Increase dataset api search limit to 400

**[opencontent/ocsensor-ls changes between 6.6.6 and 6.7.0](https://github.com/OpencontentCoop/ocsensor/compare/6.6.6...6.7.0)**
* Introduce un’interfaccia di visualizzazione delle segnalazioni critiche
* Merge branch 'master' into criticals
* Merge branch 'master' into criticals
* Add filters and presets
* Introduce un meccanismo automatico di predizione delle faq in fase di creazione della segnalazione
* Merge branch 'faq-predictor' into criticals
* bugfix
* Merge branch 'faq-predictor' into criticals
* Consente di importare utenti nei gruppi di utenti tramite file csv
* Corregge un problema di timezone nell'indicizzazione della timeline
* Permette di evidenziare le segnalazioni aperte da un super-utente Consente al super-utente di accedere alle segnalazioni private dei super-gruppi a cui appartiene
* Corregge un problema per cui si generava un errore incollando un testo sul form di inserimento note o commenti
* Permette di modificare l'ordinamento delle segnalazioni nella dashboard avanzata
* Isola la compilazione dei placeholder per poter riutilizzare la funzionalità anche in contesti diversi dai webhook (ad esempio le notifiche)
* Corregge alcuni bug minori Impone come disabilitato per default la collezione dei timeline items Corregge la vista ocsensor_user Permette l'abilitazione del predictor delle faq

**[opencontent/ocsensorapi changes between 6.6.2 and 6.7.0](https://github.com/OpencontentCoop/ocsensorapi/compare/6.6.2...6.7.0)**
* Use UserCanAccessUserGroupPosts setting in limitations search and canRead policy
* Add notification texts placeholders compiling
* Trig add_comment event on upload file or image
* Cast value in user group permissions
* Recognize auto_assign api action matching with current user id

**[opencontent/ocsocialdesign-ls changes between 1.6.3 and 1.6.5](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.3...1.6.5)**
* Rimuove il cookie alert dal design di default
* Installa il GA script cookieless di default

**[opencontent/openpa-ls changes between 3.13.7 and 3.15.0](https://github.com/OpencontentCoop/openpa/compare/3.13.7...3.15.0)**
* Permette la configurazione cookieless degli analytcs
* Espone un sitemap.xml essenziale

**[opencontent/openpa_theme_2014-ls changes between 2.15.4 and 2.16.0](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.4...2.16.0)**
* Permette la configurazione cookieless degli analytcs Aggiunge lo snippet webanalytics.italia se configurato


## [3.0.22](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.21...3.0.22) - 2022-05-10


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.221.0  | 3.222.8  | [...](https://github.com/aws/aws-sdk-php/compare/3.221.0...3.222.8)                             |
| google/apiclient-services | v0.246.0 | v0.248.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.246.0...v0.248.0) |
| opencontent/ocsensor-ls   | 6.6.4    | 6.6.6    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.6.4...6.6.6)                        |
| opencontent/ocsensorapi   | 6.6.0    | 6.6.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.6.0...6.6.2)                     |
| predis/predis             | b3a5bdf  | a2449da  | [...](https://github.com/predis/predis/compare/b3a5bdf...a2449da)                               |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.6.4 and 6.6.6](https://github.com/OpencontentCoop/ocsensor/compare/6.6.4...6.6.6)**
* Aggiunge i tag riferimento nel filtro per gruppo di incaricati nelle statistiche abilitate
* Evita di loggare le chiamate fallite alle api dei gruppi di utenti
* Permette di inserire automaticamente un operatore superutente come osservatore nelle segnalazioni che crea Corregge la chiusura automatica nel caso l'autore superutente sia un operatore Corregge le capacità dell'utente in caso sia autore e osservatore
* Corregge un errore nell'indicizzazione dei gruppi di utenti
* Corregge un bug nell'interfaccia di modifica della visibilità

**[opencontent/ocsensorapi changes between 6.6.0 and 6.6.2](https://github.com/OpencontentCoop/ocsensorapi/compare/6.6.0...6.6.2)**
* Add reference tag in group filter if enabled
* Add User::isSuperUser flag Use AddBehalfOfUserAsObserver and AddOperatorSuperUserAsObserver settings in ScenarioService Merge author and reporter permissions Fix group filters in UserService
* Fix PostService::setUserPostAware bug
* Fix userGroups set and reindex


## [3.0.21](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.20...3.0.21) - 2022-04-27
- Update changelog and publiccode
- Update publiccode.yml

#### Code dependencies
| Changes                    | From     | To       | Compare                                                                                         |
|----------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.220.1  | 3.221.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.220.1...3.221.0)                             |
| google/apiclient           | 51c6179  | a9a27c2  | [...](https://github.com/googleapis/google-api-php-client/compare/51c6179...a9a27c2)            |
| google/apiclient-services  | v0.244.0 | v0.246.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.244.0...v0.246.0) |
| opencontent/ocsensor-ls    | 6.6.2    | 6.6.4    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.6.2...6.6.4)                        |
| opencontent/ocsiracauth-ls | 1.4.0    | 1.5.0    | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.4.0...1.5.0)                     |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.6.2 and 6.6.4](https://github.com/OpencontentCoop/ocsensor/compare/6.6.2...6.6.4)**
* Corregge un bug nel popolamento delle tabelle sensor di appoggio
* Non prepopola la categoria nel form di inserimento della segnalazione

**[opencontent/ocsiracauth-ls changes between 1.4.0 and 1.5.0](https://github.com/OpencontentCoop/ocsiracauth/compare/1.4.0...1.5.0)**
* Esplicita gli errori di autenticazione Permette di autenticarsi con utenti di classi diverse da user
* Aggiunge OCSiracAuthUserTools::getUserByFiscalCodeAndEmail tra gli ExistingUserHandlers


## [3.0.20](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.19...3.0.20) - 2022-04-21


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                              |
|-------------------------|---------|---------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.219.5 | 3.220.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.219.5...3.220.1)                  |
| google/apiclient        | 506c488 | 51c6179 | [...](https://github.com/googleapis/google-api-php-client/compare/506c488...51c6179) |
| opencontent/ocsensor-ls | 6.6.1   | 6.6.2   | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.6.1...6.6.2)             |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.6.1 and 6.6.2](https://github.com/OpencontentCoop/ocsensor/compare/6.6.1...6.6.2)**
* Visualizza tutte le informazioni della segnalazione nella versione in pdf
* Corregge un bug per cui non era possibile modificare il punto sulla mappa nel modulo di modifica segnalazione
* Consente di controllare l'obbligatorietà del campo categoria per i gruppi di utenti


## [3.0.19](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.18...3.0.19) - 2022-04-19


#### Code dependencies
| Changes                           | From     | To       | Compare                                                                                         |
|-----------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                   | 3.219.3  | 3.219.5  | [...](https://github.com/aws/aws-sdk-php/compare/3.219.3...3.219.5)                             |
| google/apiclient                  | cb5250c  | 506c488  | [...](https://github.com/googleapis/google-api-php-client/compare/cb5250c...506c488)            |
| google/apiclient-services         | v0.243.0 | v0.244.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.243.0...v0.244.0) |
| opencontent/occaslogin-ls         | 1.1.3    | 1.1.4    | [...](https://github.com/OpencontentCoop/occaslogin/compare/1.1.3...1.1.4)                      |
| opencontent/ocsensor-ls           | 6.6.0    | 6.6.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.6.0...6.6.1)                        |
| opencontent/openpa_userprofile-ls | e06f87d  | 689bb53  | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/e06f87d...689bb53)          |


Relevant changes by repository:

**[opencontent/occaslogin-ls changes between 1.1.3 and 1.1.4](https://github.com/OpencontentCoop/occaslogin/compare/1.1.3...1.1.4)**
* Add fake email address if missing in user data

**[opencontent/ocsensor-ls changes between 6.6.0 and 6.6.1](https://github.com/OpencontentCoop/ocsensor/compare/6.6.0...6.6.1)**
* Corregge un bug nell'indicizzazione delle timeline

**[opencontent/openpa_userprofile-ls changes between e06f87d and 689bb53](https://github.com/OpencontentCoop/openpa_userprofile/compare/e06f87d...689bb53)**
* Invalida un profilo con indirizzo email non valido


## [3.0.18](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.17...3.0.18) - 2022-04-14


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocinstaller | 4309627 | 5fd5bdd | [...](https://github.com/OpencontentCoop/ocinstaller/compare/4309627...5fd5bdd) |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 4309627 and 5fd5bdd](https://github.com/OpencontentCoop/ocinstaller/compare/4309627...5fd5bdd)**
* Add only-schema option


## [3.0.17](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.16...3.0.17) - 2022-04-14


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                     |
|-------------------------|---------|---------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.219.2 | 3.219.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.219.2...3.219.3)         |
| opencontent/ocsensor-ls | 6.5.0   | 6.6.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.5.0...6.6.0)    |
| opencontent/ocsensorapi | 6.5.0   | 6.6.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.5.0...6.6.0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.5.0 and 6.6.0](https://github.com/OpencontentCoop/ocsensor/compare/6.5.0...6.6.0)**
* Corregge un bug nell'indicizzazione delle timeline
* Permette di inserire un operatore nei gruppi di supe rutenti Corregge un problema di permessi per gli utenti reporter

**[opencontent/ocsensorapi changes between 6.5.0 and 6.6.0](https://github.com/OpencontentCoop/ocsensorapi/compare/6.5.0...6.6.0)**
* Add custom policies solr filter including behalf feature


## [3.0.16](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.15...3.0.16) - 2022-04-13
- Add update only-schema if RUN_INSTALLER is disabled

#### Code dependencies
| Changes                      | From    | To      | Compare                                                                                |
|------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| composer/installers          | 75e5ef0 | af93ba6 | [...](https://github.com/composer/installers/compare/75e5ef0...af93ba6)                |
| google/auth                  | v1.20.0 | v1.20.1 | [...](https://github.com/googleapis/google-auth-library-php/compare/v1.20.0...v1.20.1) |
| opencontent/ocinstaller      | 828b25d | 4309627 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/828b25d...4309627)        |
| opencontent/ocsensor-ls      | 6.4.11  | 6.5.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.11...6.5.0)              |
| opencontent/ocsensorapi      | 6.4.9   | 6.5.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.9...6.5.0)            |
| opencontent/ocsiracauth-ls   | 1.3.0   | 1.4.0   | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.3.0...1.4.0)            |
| opencontent/openpa_sensor-ls | 6.0.1   | 6.1.0   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/6.0.1...6.1.0)          |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 828b25d and 4309627](https://github.com/OpencontentCoop/ocinstaller/compare/828b25d...4309627)**
* Add timestamp type in postgres dba

**[opencontent/ocsensor-ls changes between 6.4.11 and 6.5.0](https://github.com/OpencontentCoop/ocsensor/compare/6.4.11...6.5.0)**
* Introduce una tabella dedicata all''analisi delle timeline
* Introduce alcune tabelle di appoggio per il calcolo delle statistiche

**[opencontent/ocsensorapi changes between 6.4.9 and 6.5.0](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.9...6.5.0)**
* Add create message events
* Force language in treenode call

**[opencontent/ocsiracauth-ls changes between 1.3.0 and 1.4.0](https://github.com/OpencontentCoop/ocsiracauth/compare/1.3.0...1.4.0)**
* Add single logout module

**[opencontent/openpa_sensor-ls changes between 6.0.1 and 6.1.0](https://github.com/OpencontentCoop/openpa_sensor/compare/6.0.1...6.1.0)**
* Aggiorna le tabelle di appoggio alla creazione o aggiornamento dei relativi contenuti


## [3.0.15](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.14...3.0.15) - 2022-04-12


#### Code dependencies
| Changes                   | From    | To      | Compare                                                                    |
|---------------------------|---------|---------|----------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.219.1 | 3.219.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.219.1...3.219.2)        |
| opencontent/occaslogin-ls | 1.1.2   | 1.1.3   | [...](https://github.com/OpencontentCoop/occaslogin/compare/1.1.2...1.1.3) |


Relevant changes by repository:

**[opencontent/occaslogin-ls changes between 1.1.2 and 1.1.3](https://github.com/OpencontentCoop/occaslogin/compare/1.1.2...1.1.3)**
* No attributes workaround


## [3.0.14](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.13...3.0.14) - 2022-04-12


#### Code dependencies
| Changes                   | From  | To    | Compare                                                                    |
|---------------------------|-------|-------|----------------------------------------------------------------------------|
| opencontent/occaslogin-ls | 1.1.0 | 1.1.2 | [...](https://github.com/OpencontentCoop/occaslogin/compare/1.1.0...1.1.2) |


Relevant changes by repository:

**[opencontent/occaslogin-ls changes between 1.1.0 and 1.1.2](https://github.com/OpencontentCoop/occaslogin/compare/1.1.0...1.1.2)**
* Validate login attribute
* Validate login attribute


## [3.0.13](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.12...3.0.13) - 2022-04-12


#### Code dependencies
| Changes                           | From    | To      | Compare                                                                                |
|-----------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| opencontent/occaslogin-ls         | 1.0.0   | 1.1.0   | [...](https://github.com/OpencontentCoop/occaslogin/compare/1.0.0...1.1.0)             |
| opencontent/ocsensor-ls           | 6.4.10  | 6.4.11  | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.10...6.4.11)             |
| opencontent/openpa_userprofile-ls | 3998da2 | e06f87d | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/3998da2...e06f87d) |


Relevant changes by repository:

**[opencontent/occaslogin-ls changes between 1.0.0 and 1.1.0](https://github.com/OpencontentCoop/occaslogin/compare/1.0.0...1.1.0)**
* Add request/input listener on root

**[opencontent/ocsensor-ls changes between 6.4.10 and 6.4.11](https://github.com/OpencontentCoop/ocsensor/compare/6.4.10...6.4.11)**
* Corregge un errore di generazione delle chiavi di cache nel pagelayout

**[opencontent/openpa_userprofile-ls changes between 3998da2 and e06f87d](https://github.com/OpencontentCoop/openpa_userprofile/compare/3998da2...e06f87d)**
* Invalida un profilo con indirizzo email vuoto


## [3.0.12](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.11...3.0.12) - 2022-04-12


#### Code dependencies
| Changes                    | From    | To      | Compare                                                                                |
|----------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.219.0 | 3.219.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.219.0...3.219.1)                    |
| google/auth                | v1.19.0 | v1.20.0 | [...](https://github.com/googleapis/google-auth-library-php/compare/v1.19.0...v1.20.0) |
| opencontent/ocsiracauth-ls | 1.2.1   | 1.3.0   | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.2.1...1.3.0)            |


Relevant changes by repository:

**[opencontent/ocsiracauth-ls changes between 1.2.1 and 1.3.0](https://github.com/OpencontentCoop/ocsiracauth/compare/1.2.1...1.3.0)**
* Permette di configurare il mapper con variabili server di fallback


## [3.0.11](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.10...3.0.11) - 2022-04-11
- Add occaslogin extension

#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.218.5  | 3.219.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.218.5...3.219.0)                             |
| google/apiclient          | e96471b  | cb5250c  | [...](https://github.com/googleapis/google-api-php-client/compare/e96471b...cb5250c)            |
| google/apiclient-services | v0.242.0 | v0.243.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.242.0...v0.243.0) |
| opencontent/occaslogin-ls |          | 1.0.0    |                                                                                                 |



## [3.0.10](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.9...3.0.10) - 2022-04-08
- Merge commit '92ac9273e819cba789d2eab45cb00b14aab9c8bb'
- Fix installer

#### Code dependencies
| Changes                      | From  | To    | Compare                                                                       |
|------------------------------|-------|-------|-------------------------------------------------------------------------------|
| opencontent/openpa_sensor-ls | 6.0.0 | 6.0.1 | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/6.0.0...6.0.1) |


Relevant changes by repository:

**[opencontent/openpa_sensor-ls changes between 6.0.0 and 6.0.1](https://github.com/OpencontentCoop/openpa_sensor/compare/6.0.0...6.0.1)**
* Instanzia il repository in modalità lazy


## [3.0.9](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.8...3.0.9) - 2022-04-08


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                     |
|-------------------------|---------|---------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.218.4 | 3.218.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.218.4...3.218.5)         |
| opencontent/ocsensorapi | 6.4.8   | 6.4.9   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.8...6.4.9) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 6.4.8 and 6.4.9](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.8...6.4.9)**
* Hide operator name in post responses


## [3.0.8](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.7...3.0.8) - 2022-04-07


#### Code dependencies
| Changes                        | From    | To      | Compare                                                                             |
|--------------------------------|---------|---------|-------------------------------------------------------------------------------------|
| opencontent/occodicefiscale-ls | de372f8 | ac8f352 | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/de372f8...ac8f352) |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between de372f8 and ac8f352](https://github.com/OpencontentCoop/occodicefiscale/compare/de372f8...ac8f352)**
* Typo fix


## [3.0.7](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.6...3.0.7) - 2022-04-07


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                              |
|-------------------------|---------|---------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.218.3 | 3.218.4 | [...](https://github.com/aws/aws-sdk-php/compare/3.218.3...3.218.4)                  |
| google/apiclient        | a18b0e1 | e96471b | [...](https://github.com/googleapis/google-api-php-client/compare/a18b0e1...e96471b) |
| opencontent/ocsensor-ls | 6.4.9   | 6.4.10  | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.9...6.4.10)            |
| opencontent/ocsensorapi | 6.4.7   | 6.4.8   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.7...6.4.8)          |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.4.9 and 6.4.10](https://github.com/OpencontentCoop/ocsensor/compare/6.4.9...6.4.10)**
* Corregge alcuni errori di traduzione

**[opencontent/ocsensorapi changes between 6.4.7 and 6.4.8](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.7...6.4.8)**
* Fix range in Trend stat


## [3.0.6](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.5...3.0.6) - 2022-04-06


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 6.4.8 | 6.4.9 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.8...6.4.9) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.4.8 and 6.4.9](https://github.com/OpencontentCoop/ocsensor/compare/6.4.8...6.4.9)**
* Corregge un problema nella generazione delle chiavi univoche di cache per i superutenti


## [3.0.5](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.4...3.0.5) - 2022-04-05
- Update changelog and publiccode

#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 6.4.7 | 6.4.8 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.7...6.4.8) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.4.7 and 6.4.8](https://github.com/OpencontentCoop/ocsensor/compare/6.4.7...6.4.8)**
* Corregge un errore sulla visualizzazione del gruppo di autori


## [3.0.4](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.3...3.0.4) - 2022-04-05


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.218.0  | 3.218.3  | [...](https://github.com/aws/aws-sdk-php/compare/3.218.0...3.218.3)                             |
| google/apiclient          | c0ae314  | a18b0e1  | [...](https://github.com/googleapis/google-api-php-client/compare/c0ae314...a18b0e1)            |
| google/apiclient-services | v0.241.0 | v0.242.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.241.0...v0.242.0) |
| monolog/monolog           | f2f66cd  | 2.x-dev  | [...](https://github.com/Seldaek/monolog/compare/f2f66cd...2.x-dev)                             |
| opencontent/ocsensor-ls   | 6.4.6    | 6.4.7    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.6...6.4.7)                        |
| opencontent/ocsensorapi   | 6.4.6    | 6.4.7    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.6...6.4.7)                     |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.4.6 and 6.4.7](https://github.com/OpencontentCoop/ocsensor/compare/6.4.6...6.4.7)**
* Corregge le traduzioni in tedesco

**[opencontent/ocsensorapi changes between 6.4.6 and 6.4.7](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.6...6.4.7)**
* Bugfix in tree node item


## [3.0.3](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.2...3.0.3) - 2022-04-01


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 6.4.4 | 6.4.6 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.4...6.4.6) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.4.4 and 6.4.6](https://github.com/OpencontentCoop/ocsensor/compare/6.4.4...6.4.6)**
* Aggiunge alcune traduzioni mancanti
* Corregge una deprecazione di chrome dovuta a chiamate ajax sincrone


## [3.0.2](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.1...3.0.2) - 2022-04-01


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 6.4.3 | 6.4.4 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.3...6.4.4) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.4.3 and 6.4.4](https://github.com/OpencontentCoop/ocsensor/compare/6.4.3...6.4.4)**
* Corregge un errore nelle configurazioni di default


## [3.0.1](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0...3.0.1) - 2022-04-01


#### Code dependencies
| Changes                        | From     | To       | Compare                                                                                         |
|--------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.216.1  | 3.218.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.216.1...3.218.0)                             |
| google/apiclient-services      | v0.240.0 | v0.241.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.240.0...v0.241.0) |
| google/auth                    | v1.18.0  | v1.19.0  | [...](https://github.com/googleapis/google-auth-library-php/compare/v1.18.0...v1.19.0)          |
| opencontent/occodicefiscale-ls | 24253b0  | de372f8  | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/24253b0...de372f8)             |
| opencontent/ocsensor-ls        | 6.4.1    | 6.4.3    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.1...6.4.3)                        |
| opencontent/ocsensorapi        | 6.4.4    | 6.4.6    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.4...6.4.6)                     |
| opencontent/ocsiracauth-ls     | 1.2.0    | 1.2.1    | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.2.0...1.2.1)                     |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between 24253b0 and de372f8](https://github.com/OpencontentCoop/occodicefiscale/compare/24253b0...de372f8)**
* Aggiunge le traduzioni delle stringhe

**[opencontent/ocsensor-ls changes between 6.4.1 and 6.4.3](https://github.com/OpencontentCoop/ocsensor/compare/6.4.1...6.4.3)**
* Introduce la nota privata obbligatoria in fase di riassegnazione
* Abilita la chiusura automatica delle segnalazioni aperte da gruppi di operatori (in base a configurazione)
* Aggiunge la possibilità di modificare l'oggetto della segnalazione nell'interfaccia di duplicazione smart
* Introduce un'etichetta riferimento per i gruppi
* Migliora la generazione della cache del layout Introduce la navigazione ajax in alcuni moduli
* Corregge alcuni bug di traduzione
* Corregge un bug di visualizzazione dei filtri
* Migliora la leggibilità della statistica Aperte nel tempo rimuovendo i valori vuoti

**[opencontent/ocsensorapi changes between 6.4.4 and 6.4.6](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.4...6.4.6)**
* Hotfix in scenario remote id generator
* Add UserGroupPostFixListener
* Add group reference property
* Cache tree node in memory

**[opencontent/ocsiracauth-ls changes between 1.2.0 and 1.2.1](https://github.com/OpencontentCoop/ocsiracauth/compare/1.2.0...1.2.1)**
* Corregge un bug sui file delle traduzioni


## [3.0.0](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc16...3.0.0) - 2022-03-24
- Update changelog and publiccode

#### Code dependencies
| Changes         | From    | To      | Compare                                                             |
|-----------------|---------|---------|---------------------------------------------------------------------|
| aws/aws-sdk-php | 3.215.2 | 3.216.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.215.2...3.216.1) |

#### [Code dependencies from version 2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.9...3.0.0)
| Changes                          | From    | To        | Compare                                                                       |
|----------------------------------|---------|-----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.212.7 | 3.216.1   | [...](https://github.com/aws/aws-sdk-php/compare/3.212.7...3.216.1)           |
| composer/installers              | b8e490f | 75e5ef0   | [...](https://github.com/composer/installers/compare/b8e490f...75e5ef0)       |
| opencontent/ocsensor-ls          | 5.19.2  | 6.4.1     | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.19.2...6.4.1)     |
| opencontent/ocsensorapi          | 5.14.5  | 6.4.4     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.5...6.4.4)  |
| opencontent/openpa_sensor-ls     | 4.6.0   | 6.0.0     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.6.0...6.0.0) |
| google/apiclient                 | NEW     | c0ae314   |                                                                               |
| google/apiclient-services        | NEW     | v0.240.0  |                                                                               |
| google/auth                      | NEW     | v1.18.0   |                                                                               |
| monolog/monolog                  | NEW     | f2f66cd   |                                                                               |
| opencontent/googlesheet          | NEW     | 7d19be0   |                                                                               |
| opencontent/oci18n               | NEW     | c67c37e   |                                                                               |
| paragonie/constant_time_encoding | NEW     | v2.5.0    |                                                                               |
| phpseclib/phpseclib              | NEW     | 3.0.x-dev |                                                                               |
| psr/cache                        | NEW     | 1.0.1     |                                                                               |

Relevant changes by repository from version 2:

**opencontent/ocsensor-ls changes between 5.19.2 and 6.4.1**
* Introduce la versione internazionalizzabile
* Introduce la possibilità per il cittadino di allegare file
* Corregge un problema di visualizzazione dei layers della mappa
* Aggiunge una cache ai setting del repository Evita di caricare il repository in sensor gui controller se non necessario Permette di tradurre le stringhe di login custom Permette di disabilitare la cache degli scenari
* Corregge un problema sulla definizione degli scenari
* Introduce la possibilità per gli utenti del gruppo riferimento per il cittadino primario di taggare post come speciali
* Permette di abilitare un operatore a prendere in carico le segnalazioni in cui è coinvolto come osservatore
* Corregge un problema nell'interfaccia di inserimento dell'autore della segnalazione per cui scompariva la selezione del canale
* Permette di embeddare nei report dati remoti
* Parametrizza la definizione dei tipi di notifica
* Introduce la voce di menu Importanti in Inbox
* Permette la selezione di più eventi nella definizione di un automatismo
* Permette la modifica massiva dell'evento degli scenari Riorganizza alcune operazioni batch
* Permette di esportare le segnalazioni in csv dalla pagina sensor/posts
* Corregge un problema di cache dopo la modifica massiva degli scenari
* Introduce il filtro per gruppo di utente nelle statistiche
* Uniforma lo stato di lettura per il gruppo approvatore di default nell'interfaccia inbox
* Evidenzia il gruppo di super utenti nella visualizzazione della segnalazione Corregge alcuni errori di traduzione
* Visualizza le segnalazioni critiche in Inbox/In arrivo
* Corregge un errore sui fogli di stile nei filtri delle statistiche
* Introduce l'interfaccia rapida di duplicazione segnalazione
* Corregge le traduzioni del modulo di cambio password

**opencontent/ocsensorapi changes between 5.14.5 and 6.4.4**
* Add i18n feature
* Handle post/files field
* Use postgres to load single entity in searchOne trait method Add scenario cache Check first scenario approvers consistency Add assign_fix_time in SolrMapper using closing_time if missing fix
* Fix scenario cache load permissions
* Bugfix in scenario service
* wip
* Add post/tags field
* Add isSuperObserver user flag
* Bugfix in scenario service
* Refactor notification types
* Hotfix Scenario unserializer
* Add search in multi target scenarios
* Add missing stat bucket
* Add user group filter in stat
* Collect first approver read status
* Small bugfixes Add usergroup read api
* Fix index sensor_author_group_list_lk
* Bugfix in create operator api
* Fix user group access api
* Fix user group index
* Refactor OpenHistoryPerOwnerGroup stat

**opencontent/openpa_sensor-ls changes between 4.6.0 and 6.0.0**
* Adatta il codice alla versione internazionalizzabile


## [3.0.0-rc16](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc15...3.0.0-rc16) - 2022-03-23


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                     |
|-------------------------|---------|---------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.215.1 | 3.215.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.215.1...3.215.2)         |
| opencontent/ocsensor-ls | 6.4.0   | 6.4.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.4.0...6.4.1)    |
| opencontent/ocsensorapi | 6.4.3   | 6.4.4   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.3...6.4.4) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.4.0 and 6.4.1](https://github.com/OpencontentCoop/ocsensor/compare/6.4.0...6.4.1)**
* Corregge le traduzioni del modulo di cambio password

**[opencontent/ocsensorapi changes between 6.4.3 and 6.4.4](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.3...6.4.4)**
* Fix user group index
* Refactor OpenHistoryPerOwnerGroup stat


## [3.0.0-rc15](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc14...3.0.0-rc15) - 2022-03-22


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                     |
|-------------------------|---------|---------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.215.0 | 3.215.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.215.0...3.215.1)         |
| opencontent/ocsensor-ls | 6.3.2   | 6.4.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.3.2...6.4.0)    |
| opencontent/ocsensorapi | 6.4.2   | 6.4.3   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.2...6.4.3) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.3.2 and 6.4.0](https://github.com/OpencontentCoop/ocsensor/compare/6.3.2...6.4.0)**
* Visualizza le segnalazioni critiche in Inbox/In arrivo
* Corregge un errore sui fogli di stile nei filtri delle statistiche
* Introduce l'interfaccia rapida di duplicazione segnalazione

**[opencontent/ocsensorapi changes between 6.4.2 and 6.4.3](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.2...6.4.3)**
* Fix index sensor_author_group_list_lk
* Bugfix in create operator api
* Fix user group access api


## [3.0.0-rc14](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc13...3.0.0-rc14) - 2022-03-21


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.212.7  | 3.215.0  | [...](https://github.com/aws/aws-sdk-php/compare/3.212.7...3.215.0)                             |
| composer/installers       | b8e490f  | 75e5ef0  | [...](https://github.com/composer/installers/compare/b8e490f...75e5ef0)                         |
| google/apiclient-services | v0.239.0 | v0.240.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.239.0...v0.240.0) |
| monolog/monolog           | cb3675e  | f2f66cd  | [...](https://github.com/Seldaek/monolog/compare/cb3675e...f2f66cd)                             |
| opencontent/ocsensor-ls   | 6.3.1    | 6.3.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.3.1...6.3.2)                        |
| opencontent/ocsensorapi   | 6.4.1    | 6.4.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.1...6.4.2)                     |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.3.1 and 6.3.2](https://github.com/OpencontentCoop/ocsensor/compare/6.3.1...6.3.2)**
* Evidenzia il gruppo di super utenti nella visualizzazione della segnalazione Corregge alcuni errori di traduzione

**[opencontent/ocsensorapi changes between 6.4.1 and 6.4.2](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.1...6.4.2)**
* Small bugfixes Add usergroup read api


## [2.6.11](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.10...2.6.11) - 2022-03-14
- Update changelog and publiccode

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                       |
|-------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.212.5 | 3.212.7 | [...](https://github.com/aws/aws-sdk-php/compare/3.212.5...3.212.7)           |
| opencontent/ocsensorapi | 5.14.4  | 5.14.5  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.4...5.14.5) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.14.4 and 5.14.5](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.4...5.14.5)**
* Add missing stat bucket



## [3.0.0-rc13](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc12...3.0.0-rc13) - 2022-03-14


#### Code dependencies
| Changes                    | From     | To       | Compare                                                                                         |
|----------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.212.4  | 3.212.7  | [...](https://github.com/aws/aws-sdk-php/compare/3.212.4...3.212.7)                             |
| google/apiclient-services  | v0.238.1 | v0.239.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.238.1...v0.239.0) |
| monolog/monolog            | baf2d9d  | cb3675e  | [...](https://github.com/Seldaek/monolog/compare/baf2d9d...cb3675e)                             |
| opencontent/ocdedalogin-ls | 1.0.2    | 1.0.4    | [...](https://github.com/OpencontentCoop/ocdedalogin/compare/1.0.2...1.0.4)                     |
| opencontent/ocsensor-ls    | 6.3.0    | 6.3.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.3.0...6.3.1)                        |
| opencontent/ocsensorapi    | 6.4.0    | 6.4.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.0...6.4.1)                     |


Relevant changes by repository:

**[opencontent/ocdedalogin-ls changes between 1.0.2 and 1.0.4](https://github.com/OpencontentCoop/ocdedalogin/compare/1.0.2...1.0.4)**
* Fix missing bearer in checkAssertion Add default attribute mapper
* Bug fix duplicate encoding

**[opencontent/ocsensor-ls changes between 6.3.0 and 6.3.1](https://github.com/OpencontentCoop/ocsensor/compare/6.3.0...6.3.1)**
* Uniforma lo stato di lettura per il gruppo approvatore di default nell'interfaccia inbox

**[opencontent/ocsensorapi changes between 6.4.0 and 6.4.1](https://github.com/OpencontentCoop/ocsensorapi/compare/6.4.0...6.4.1)**
* Add missing stat bucket
*   Merge commit 'version-5 into i18n
* Collect first approver read status


## [2.6.10](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.9...2.6.10) - 2022-03-10
- Update changelog and publiccode

#### Code dependencies
| Changes                    | From  | To    | Compare                                                                     |
|----------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocdedalogin-ls | 1.0.3 | 1.0.4 | [...](https://github.com/OpencontentCoop/ocdedalogin/compare/1.0.3...1.0.4) |


Relevant changes by repository:

**[opencontent/ocdedalogin-ls changes between 1.0.3 and 1.0.4](https://github.com/OpencontentCoop/ocdedalogin/compare/1.0.3...1.0.4)**
* Bug fix duplicate encoding


## [2.6.9](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.8...2.6.9) - 2022-03-10
- Update changelog and publiccode

#### Code dependencies
| Changes                    | From    | To      | Compare                                                                     |
|----------------------------|---------|---------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.212.1 | 3.212.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.212.1...3.212.5)         |
| opencontent/ocdedalogin-ls | 1.0.2   | 1.0.3   | [...](https://github.com/OpencontentCoop/ocdedalogin/compare/1.0.2...1.0.3) |
| zetacomponents/feed        | 1.4.1   | 1.4.2   | [...](https://github.com/zetacomponents/Feed/compare/1.4.1...1.4.2)         |


Relevant changes by repository:

**[opencontent/ocdedalogin-ls changes between 1.0.2 and 1.0.3](https://github.com/OpencontentCoop/ocdedalogin/compare/1.0.2...1.0.3)**
* Fix missing bearer in checkAssertion Add default attribute mapper


## [3.0.0-rc12](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc11...3.0.0-rc12) - 2022-03-09
- Update changelog and publiccode

#### Code dependencies
| Changes                          | From     | To       | Compare                                                                                         |
|----------------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.210.0  | 3.212.4  | [...](https://github.com/aws/aws-sdk-php/compare/3.210.0...3.212.4)                             |
| google/apiclient-services        | v0.237.0 | v0.238.1 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.237.0...v0.238.1) |
| league/oauth2-client             |          | 2.6.1    |                                                                                                 |
| monolog/monolog                  | fb2c324  | baf2d9d  | [...](https://github.com/Seldaek/monolog/compare/fb2c324...baf2d9d)                             |
| opencontent/ocembed-ls           | 1.5.2    | 1.5.3    | [...](https://github.com/OpencontentCoop/ocembed/compare/1.5.2...1.5.3)                         |
| opencontent/ocsearchtools-ls     | 1.11.1   | 1.11.2   | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.1...1.11.2)                 |
| opencontent/ocsensor-ls          | 6.2.3    | 6.3.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.2.3...6.3.0)                        |
| opencontent/ocsensorapi          | 6.3.2    | 6.4.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.3.2...6.4.0)                     |
| opencontent/ocsiracauth-ls       | 1.1.1    | 1.2.0    | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.1.1...1.2.0)                     |
| symfony/polyfill-ctype           | v1.24.0  | v1.25.0  | [...](https://github.com/symfony/polyfill-ctype/compare/v1.24.0...v1.25.0)                      |
| symfony/polyfill-intl-idn        | v1.24.0  | v1.25.0  | [...](https://github.com/symfony/polyfill-intl-idn/compare/v1.24.0...v1.25.0)                   |
| symfony/polyfill-intl-normalizer | v1.24.0  | v1.25.0  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/v1.24.0...v1.25.0)            |
| symfony/polyfill-mbstring        | v1.24.0  | v1.25.0  | [...](https://github.com/symfony/polyfill-mbstring/compare/v1.24.0...v1.25.0)                   |
| symfony/polyfill-php72           | v1.24.0  | v1.25.0  | [...](https://github.com/symfony/polyfill-php72/compare/v1.24.0...v1.25.0)                      |
| symfony/polyfill-php73           | v1.24.0  | v1.25.0  | [...](https://github.com/symfony/polyfill-php73/compare/v1.24.0...v1.25.0)                      |
| symfony/polyfill-php80           | v1.24.0  | v1.25.0  | [...](https://github.com/symfony/polyfill-php80/compare/v1.24.0...v1.25.0)                      |
| zetacomponents/feed              | 1.4.1    | 1.4.2    | [...](https://github.com/zetacomponents/Feed/compare/1.4.1...1.4.2)                             |


Relevant changes by repository:

**[opencontent/ocembed-ls changes between 1.5.2 and 1.5.3](https://github.com/OpencontentCoop/ocembed/compare/1.5.2...1.5.3)**
* php 5.6 bc

**[opencontent/ocsearchtools-ls changes between 1.11.1 and 1.11.2](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.1...1.11.2)**
* Avoid php warning

**[opencontent/ocsensor-ls changes between 6.2.3 and 6.3.0](https://github.com/OpencontentCoop/ocsensor/compare/6.2.3...6.3.0)**
* Permette la selezione di più eventi nella definizione di un automatismo
* Permette la modifica massiva dell'evento degli scenari Riorganizza alcune operazioni batch
* Permette di esportare le segnalazioni in csv dalla pagina sensor/posts
* Corregge un problema di cache dopo la modifica massiva degli scenari
* Introduce il filtro per gruppo di utente nelle statistiche

**[opencontent/ocsensorapi changes between 6.3.2 and 6.4.0](https://github.com/OpencontentCoop/ocsensorapi/compare/6.3.2...6.4.0)**
* Add search in multi target scenarios
* Add missing stat bucket
* Add user group filter in stat

**[opencontent/ocsiracauth-ls changes between 1.1.1 and 1.2.0](https://github.com/OpencontentCoop/ocsiracauth/compare/1.1.1...1.2.0)**
* Introduce un client oauth embed


## [3.0.0-rc11](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.8...3.0.0-rc11) - 2022-03-09
- Update changelog and publiccode


#### Installer
- Fix sensor_area class

#### Code dependencies
| Changes                          | From    | To        | Compare                                                                       |
|----------------------------------|---------|-----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.212.1 | 3.212.4   | [...](https://github.com/aws/aws-sdk-php/compare/3.212.1...3.212.4)           |
| google/apiclient                 |         | c0ae314   |                                                                               |
| google/apiclient-services        |         | v0.238.1  |                                                                               |
| google/auth                      |         | v1.18.0   |                                                                               |
| monolog/monolog                  |         | baf2d9d   |                                                                               |
| opencontent/googlesheet          |         | 7d19be0   |                                                                               |
| opencontent/oci18n               |         | c67c37e   |                                                                               |
| opencontent/ocsensor-ls          | 5.19.2  | 6.3.0     | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.19.2...6.3.0)     |
| opencontent/ocsensorapi          | 5.14.4  | 6.4.0     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.4...6.4.0)  |
| opencontent/openpa_sensor-ls     | 4.6.0   | 6.0.0     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.6.0...6.0.0) |
| paragonie/constant_time_encoding |         | v2.5.0    |                                                                               |
| phpseclib/phpseclib              |         | 3.0.x-dev |                                                                               |
| psr/cache                        |         | 1.0.1     |                                                                               |
| zetacomponents/feed              | 1.4.1   | 1.4.2     | [...](https://github.com/zetacomponents/Feed/compare/1.4.1...1.4.2)           |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.19.2 and 6.3.0](https://github.com/OpencontentCoop/ocsensor/compare/5.19.2...6.3.0)**
* Introduce la versione internazionalizzabile
* Introduce la possibilità per il cittadino di allegare file
* Corregge un problema di visualizzazione dei layers della mappa
* Aggiunge una cache ai setting del repository Evita di caricare il repository in sensor gui controller se non necessario Permette di tradurre le stringhe di login custom Permette di disabilitare la cache degli scenari
* Corregge un problema sulla definizione degli scenari
* wip
* Merge branch 'version-5' into i18n
* Merge branch 'i18n' into wip-feb-22
* Introduce la possibilità per gli utenti del gruppo riferimento per il cittadino primario di taggare post come speciali
* Permette di abilitare un operatore a prendere in carico le segnalazioni in cui è coinvolto come osservatore
* Corregge un problema nell'interfaccia di inserimento dell'autore della segnalazione per cui scompariva la selezione del canale
* Permette di embeddare nei report dati remoti
* Parametrizza la definizione dei tipi di notifica
* Introduce la voce di menu Importanti in Inbox
* Permette la selezione di più eventi nella definizione di un automatismo
* Permette la modifica massiva dell'evento degli scenari Riorganizza alcune operazioni batch
* Permette di esportare le segnalazioni in csv dalla pagina sensor/posts
* Corregge un problema di cache dopo la modifica massiva degli scenari
* Introduce il filtro per gruppo di utente nelle statistiche

**[opencontent/ocsensorapi changes between 5.14.4 and 6.4.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.4...6.4.0)**
* Add i18n feature
* Handle post/files field
* Use postgres to load single entity in searchOne trait method Add scenario cache Check first scenario approvers consistency Add assign_fix_time in SolrMapper using closing_time if missing fix
* Fix scenario cache load permissions
* Bugfix in scenario service
* wip
* Merge branch 'version-5' into i18n
* Merge branch 'i18n' into wip-feb-22
* Add post/tags field
* Add isSuperObserver user flag
* Bugfix in scenario service
* Refactor notification types
* Hotfix Scenario unserializer
* Add search in multi target scenarios
* Add missing stat bucket
* Add user group filter in stat

**[opencontent/openpa_sensor-ls changes between 4.6.0 and 6.0.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.6.0...6.0.0)**
* Adatta il codice alla versione internazionalizzabile


## [2.6.8](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.7...2.6.8) - 2022-03-04


#### Code dependencies
| Changes                          | From     | To        | Compare                                                                              |
|----------------------------------|----------|-----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.209.25 | 3.212.1   | [...](https://github.com/aws/aws-sdk-php/compare/3.209.25...3.212.1)                 |
| clue/stream-filter               | v1.5.0   | v1.6.0    | [...](https://github.com/clue/stream-filter/compare/v1.5.0...v1.6.0)                 |
| league/oauth2-client             |          | 2.6.1     |                                                                                      |
| opencontent/ocembed-ls           | 1.5.2    | 1.5.3     | [...](https://github.com/OpencontentCoop/ocembed/compare/1.5.2...1.5.3)              |
| opencontent/ocsearchtools-ls     | 1.11.1   | 1.11.2    | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.1...1.11.2)      |
| opencontent/ocsiracauth-ls       | 1.1.1    | 1.2.0     | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.1.1...1.2.0)          |
| opencontent/ocsupport-ls         | 84a0708  | 848dad7   | [...](https://github.com/OpencontentCoop/ocsupport/compare/84a0708...848dad7)        |
| paragonie/random_compat          |          | v9.99.100 |                                                                                      |
| php-http/httplug                 | fc6a139  | f640739   | [...](https://github.com/php-http/httplug/compare/fc6a139...f640739)                 |
| symfony/polyfill-ctype           | v1.24.0  | v1.25.0   | [...](https://github.com/symfony/polyfill-ctype/compare/v1.24.0...v1.25.0)           |
| symfony/polyfill-intl-idn        | v1.24.0  | v1.25.0   | [...](https://github.com/symfony/polyfill-intl-idn/compare/v1.24.0...v1.25.0)        |
| symfony/polyfill-intl-normalizer | v1.24.0  | v1.25.0   | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/v1.24.0...v1.25.0) |
| symfony/polyfill-mbstring        | v1.24.0  | v1.25.0   | [...](https://github.com/symfony/polyfill-mbstring/compare/v1.24.0...v1.25.0)        |
| symfony/polyfill-php72           | v1.24.0  | v1.25.0   | [...](https://github.com/symfony/polyfill-php72/compare/v1.24.0...v1.25.0)           |
| symfony/polyfill-php73           | v1.24.0  | v1.25.0   | [...](https://github.com/symfony/polyfill-php73/compare/v1.24.0...v1.25.0)           |
| symfony/polyfill-php80           | v1.24.0  | v1.25.0   | [...](https://github.com/symfony/polyfill-php80/compare/v1.24.0...v1.25.0)           |


Relevant changes by repository:

**[opencontent/ocembed-ls changes between 1.5.2 and 1.5.3](https://github.com/OpencontentCoop/ocembed/compare/1.5.2...1.5.3)**
* php 5.6 bc

**[opencontent/ocsearchtools-ls changes between 1.11.1 and 1.11.2](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.1...1.11.2)**
* Avoid php warning

**[opencontent/ocsiracauth-ls changes between 1.1.1 and 1.2.0](https://github.com/OpencontentCoop/ocsiracauth/compare/1.1.1...1.2.0)**
* Introduce un client oauth embed

**[opencontent/ocsupport-ls changes between 84a0708 and 848dad7](https://github.com/OpencontentCoop/ocsupport/compare/84a0708...848dad7)**
* Add reindex script




## [3.0.0-rc10](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc9...3.0.0-rc10) - 2022-02-22
- Update changelog and publiccode

#### Code dependencies
| Changes                 | From  | To    | Compare                                                                     |
|-------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocsensorapi | 6.3.1 | 6.3.2 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.3.1...6.3.2) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 6.3.1 and 6.3.2](https://github.com/OpencontentCoop/ocsensorapi/compare/6.3.1...6.3.2)**
* Hotfix Scenario unserializer


## [3.0.0-rc9](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc8...3.0.0-rc9) - 2022-02-22


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                                         |
|---------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.209.27 | 3.209.28 | [...](https://github.com/aws/aws-sdk-php/compare/3.209.27...3.209.28)                           |
| clue/stream-filter        | v1.5.0   | v1.6.0   | [...](https://github.com/clue/stream-filter/compare/v1.5.0...v1.6.0)                            |
| google/apiclient-services | v0.235.0 | v0.236.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.235.0...v0.236.0) |
| opencontent/ocsensor-ls   | 6.2.1    | 6.2.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.2.1...6.2.2)                        |
| opencontent/ocsensorapi   | 6.3.0    | 6.3.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.3.0...6.3.1)                     |
| php-http/httplug          | fc6a139  | f640739  | [...](https://github.com/php-http/httplug/compare/fc6a139...f640739)                            |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.2.1 and 6.2.2](https://github.com/OpencontentCoop/ocsensor/compare/6.2.1...6.2.2)**
* Parametrizza la definizione dei tipi di notifica

**[opencontent/ocsensorapi changes between 6.3.0 and 6.3.1](https://github.com/OpencontentCoop/ocsensorapi/compare/6.3.0...6.3.1)**
* Bugfix in scenario service
* Refactor notification types


## [3.0.0-rc8](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc7...3.0.0-rc8) - 2022-02-20


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 6.2.0 | 6.2.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.2.0...6.2.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.2.0 and 6.2.1](https://github.com/OpencontentCoop/ocsensor/compare/6.2.0...6.2.1)**
* Permette di embeddare nei report dati remoti


## [3.0.0-rc7](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.7...3.0.0-rc7) - 2022-02-20
- Update changelog and publiccode


#### Installer
- Fix sensor_area class

#### Code dependencies
| Changes                          | From     | To        | Compare                                                                       |
|----------------------------------|----------|-----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.209.25 | 3.209.27  | [...](https://github.com/aws/aws-sdk-php/compare/3.209.25...3.209.27)         |
| google/apiclient                 |          | c0ae314   |                                                                               |
| google/apiclient-services        |          | v0.235.0  |                                                                               |
| google/auth                      |          | v1.18.0   |                                                                               |
| monolog/monolog                  |          | fb2c324   |                                                                               |
| opencontent/googlesheet          |          | 7d19be0   |                                                                               |
| opencontent/oci18n               |          | c67c37e   |                                                                               |
| opencontent/ocsensor-ls          | 5.19.2   | 6.2.0     | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.19.2...6.2.0)     |
| opencontent/ocsensorapi          | 5.14.4   | 6.3.0     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.4...6.3.0)  |
| opencontent/openpa_sensor-ls     | 4.6.0    | 6.0.0     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.6.0...6.0.0) |
| paragonie/constant_time_encoding |          | v2.5.0    |                                                                               |
| paragonie/random_compat          |          | v9.99.100 |                                                                               |
| phpseclib/phpseclib              |          | 3.0.x-dev |                                                                               |
| psr/cache                        |          | 1.0.1     |                                                                               |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.19.2 and 6.2.0](https://github.com/OpencontentCoop/ocsensor/compare/5.19.2...6.2.0)**
* Introduce la versione internazionalizzabile
* Introduce la possibilità per il cittadino di allegare file
* Corregge un problema di visualizzazione dei layers della mappa
* Aggiunge una cache ai setting del repository Evita di caricare il repository in sensor gui controller se non necessario Permette di tradurre le stringhe di login custom Permette di disabilitare la cache degli scenari
* Corregge un problema sulla definizione degli scenari
* wip
* Merge branch 'version-5' into i18n
* Merge branch 'i18n' into wip-feb-22
* Introduce la possibilità per gli utenti del gruppo riferimento per il cittadino primario di taggare post come speciali
* Permette di abilitare un operatore a prendere in carico le segnalazioni in cui è coinvolto come osservatore
* Corregge un problema nell'interfaccia di inserimento dell'autore della segnalazione per cui scompariva la selezione del canale

**[opencontent/ocsensorapi changes between 5.14.4 and 6.3.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.4...6.3.0)**
* Add i18n feature
* Handle post/files field
* Use postgres to load single entity in searchOne trait method Add scenario cache Check first scenario approvers consistency Add assign_fix_time in SolrMapper using closing_time if missing fix
* Fix scenario cache load permissions
* Bugfix in scenario service
* wip
* Merge branch 'version-5' into i18n
* Merge branch 'i18n' into wip-feb-22
* Add post/tags field
* Add isSuperObserver user flag

**[opencontent/openpa_sensor-ls changes between 4.6.0 and 6.0.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.6.0...6.0.0)**
* Adatta il codice alla versione internazionalizzabile


## [2.6.7](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc6...2.6.7) - 2022-02-17


#### Code dependencies
| Changes                          | From      | To     | Compare                                                                       |
|----------------------------------|-----------|--------|-------------------------------------------------------------------------------|
| google/apiclient                 | c0ae314   |        |                                                                               |
| google/apiclient-services        | v0.235.0  |        |                                                                               |
| google/auth                      | v1.18.0   |        |                                                                               |
| monolog/monolog                  | fb2c324   |        |                                                                               |
| opencontent/googlesheet          | 7d19be0   |        |                                                                               |
| opencontent/oci18n               | c67c37e   |        |                                                                               |
| opencontent/ocsensor-ls          | 6.1.3     | 5.19.2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.1.3...5.19.2)     |
| opencontent/ocsensorapi          | 6.2.2     | 5.14.4 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.2.2...5.14.4)  |
| opencontent/openpa_sensor-ls     | 6.0.0     | 4.6.0  | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/6.0.0...4.6.0) |
| paragonie/constant_time_encoding | v2.5.0    |        |                                                                               |
| paragonie/random_compat          | v9.99.100 |        |                                                                               |
| phpseclib/phpseclib              | 3.0.x-dev |        |                                                                               |
| psr/cache                        | 1.0.1     |        |                                                                               |

**[opencontent/ocsensor-ls changes between 6.1.3 and 5.19.2](https://github.com/OpencontentCoop/ocsensor/compare/6.1.3...5.19.2)**
* Corregge un problema nell'editor dei settings dell'utente

**[opencontent/ocsensorapi changes between 6.2.2 and 5.14.4](https://github.com/OpencontentCoop/ocsensorapi/compare/6.2.2...5.14.4)**
* Hotfix user settings


## [3.0.0-rc6](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc5...3.0.0-rc6) - 2022-02-17


#### Code dependencies
| Changes                  | From     | To       | Compare                                                                       |
|--------------------------|----------|----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php          | 3.209.24 | 3.209.25 | [...](https://github.com/aws/aws-sdk-php/compare/3.209.24...3.209.25)         |
| composer/installers      | e6facf8  | b8e490f  | [...](https://github.com/composer/installers/compare/e6facf8...b8e490f)       |
| opencontent/ocsensor-ls  | 6.1.2    | 6.1.3    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.1.2...6.1.3)      |
| opencontent/ocsensorapi  | 6.2.1    | 6.2.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.2.1...6.2.2)   |
| opencontent/ocsupport-ls | c4e37a9  | 84a0708  | [...](https://github.com/OpencontentCoop/ocsupport/compare/c4e37a9...84a0708) |
| php-http/httplug         | 191a0a1  | fc6a139  | [...](https://github.com/php-http/httplug/compare/191a0a1...fc6a139)          |

Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.1.2 and 6.1.3](https://github.com/OpencontentCoop/ocsensor/compare/6.1.2...6.1.3)**
* Corregge un problema sulla definizione degli scenari

**[opencontent/ocsensorapi changes between 6.2.1 and 6.2.2](https://github.com/OpencontentCoop/ocsensorapi/compare/6.2.1...6.2.2)**
* Bugfix in scenario service

**[opencontent/ocsupport-ls changes between c4e37a9 and 84a0708](https://github.com/OpencontentCoop/ocsupport/compare/c4e37a9...84a0708)**
* enable view cache in cron sqli handler


## [3.0.0-rc5](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc4...3.0.0-rc5) - 2022-02-16


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                     |
|-------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocsensorapi | 6.2.0 | 6.2.1 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.2.0...6.2.1) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 6.2.0 and 6.2.1](https://github.com/OpencontentCoop/ocsensorapi/compare/6.2.0...6.2.1)**
* Fix scenario cache load permissions


## [3.0.0-rc4](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc3...3.0.0-rc4) - 2022-02-16


#### Code dependencies
| Changes                     | From  | To    | Compare                                                                      |
|-----------------------------|-------|-------|------------------------------------------------------------------------------|
| opencontent/occustomfind-ls | 2.4.2 | 2.4.3 | [...](https://github.com/OpencontentCoop/occustomfind/compare/2.4.2...2.4.3) |
| opencontent/ocsensor-ls     | 6.1.1 | 6.1.2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.1.1...6.1.2)     |
| opencontent/ocsensorapi     | 6.1.0 | 6.2.0 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.1.0...6.2.0)  |


Relevant changes by repository:

**[opencontent/occustomfind-ls changes between 2.4.2 and 2.4.3](https://github.com/OpencontentCoop/occustomfind/compare/2.4.2...2.4.3)**
* Fix edit dataset view connector (temp workaround)

**[opencontent/ocsensor-ls changes between 6.1.1 and 6.1.2](https://github.com/OpencontentCoop/ocsensor/compare/6.1.1...6.1.2)**
* Aggiunge una cache ai setting del repository Evita di caricare il repository in sensor gui controller se non necessario Permette di tradurre le stringhe di login custom Permette di disabilitare la cache degli scenari

**[opencontent/ocsensorapi changes between 6.1.0 and 6.2.0](https://github.com/OpencontentCoop/ocsensorapi/compare/6.1.0...6.2.0)**
* Use postgres to load single entity in searchOne trait method Add scenario cache Check first scenario approvers consistency Add assign_fix_time in SolrMapper using closing_time if missing fix


## [3.0.0-rc3](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc2...3.0.0-rc3) - 2022-02-15


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 6.1.0 | 6.1.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.1.0...6.1.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6.1.0 and 6.1.1](https://github.com/OpencontentCoop/ocsensor/compare/6.1.0...6.1.1)**
* Corregge un problema di visualizzazione dei layers della mappa


## [3.0.0-rc2](https://gitlab.com/opencontent/opensegnalazioni/compare/3.0.0-rc1...3.0.0-rc2) - 2022-02-15


#### Code dependencies
| Changes                     | From     | To       | Compare                                                                                         |
|-----------------------------|----------|----------|-------------------------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.209.16 | 3.209.24 | [...](https://github.com/aws/aws-sdk-php/compare/3.209.16...3.209.24)                           |
| composer/installers         | a241e78  | e6facf8  | [...](https://github.com/composer/installers/compare/a241e78...e6facf8)                         |
| google/apiclient            | 1530583  | c0ae314  | [...](https://github.com/googleapis/google-api-php-client/compare/1530583...c0ae314)            |
| google/apiclient-services   | v0.233.0 | v0.235.0 | [...](https://github.com/googleapis/google-api-php-client-services/compare/v0.233.0...v0.235.0) |
| opencontent/occustomfind-ls | 2.4.0    | 2.4.2    | [...](https://github.com/OpencontentCoop/occustomfind/compare/2.4.0...2.4.2)                    |
| opencontent/ocopendata-ls   | 2.28.0   | 2.28.1   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.28.0...2.28.1)                    |
| opencontent/ocsensor-ls     | 6.0.0    | 6.1.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/6.0.0...6.1.0)                        |
| opencontent/ocsensorapi     | 6.0.0    | 6.1.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/6.0.0...6.1.0)                     |
| php-http/message            | 1.12.0   | 1.13.0   | [...](https://github.com/php-http/message/compare/1.12.0...1.13.0)                              |
| zetacomponents/database     | 1.5.1    | 1.5.2    | [...](https://github.com/zetacomponents/Database/compare/1.5.1...1.5.2)                         |
| zetacomponents/feed         | 1.4      | 1.4.1    | [...](https://github.com/zetacomponents/Feed/compare/1.4...1.4.1)                               |


Relevant changes by repository:

**[opencontent/occustomfind-ls changes between 2.4.0 and 2.4.2](https://github.com/OpencontentCoop/occustomfind/compare/2.4.0...2.4.2)**
* Allow set geo field format Fix translations Add counter settings
* Avoid fatal exposing stats results

**[opencontent/ocopendata-ls changes between 2.28.0 and 2.28.1](https://github.com/OpencontentCoop/ocopendata/compare/2.28.0...2.28.1)**
* Use local filesystem in file storage for performance issue

**[opencontent/ocsensor-ls changes between 6.0.0 and 6.1.0](https://github.com/OpencontentCoop/ocsensor/compare/6.0.0...6.1.0)**
* Introduce la possibilità per il cittadino di allegare file

**[opencontent/ocsensorapi changes between 6.0.0 and 6.1.0](https://github.com/OpencontentCoop/ocsensorapi/compare/6.0.0...6.1.0)**
* Handle post/files field


## [3.0.0-rc1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.6...3.0.0-rc1) - 2022-02-03


#### Code dependencies
| Changes                          | From     | To        | Compare                                                                         |
|----------------------------------|----------|-----------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.209.10 | 3.209.16  | [...](https://github.com/aws/aws-sdk-php/compare/3.209.10...3.209.16)           |
| friendsofsymfony/http-cache      | 2.12.0   | 2.13.0    | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/2.12.0...2.13.0) |
| google/apiclient                 |          | 1530583   |                                                                                 |
| google/apiclient-services        |          | v0.233.0  |                                                                                 |
| google/auth                      |          | v1.18.0   |                                                                                 |
| monolog/monolog                  |          | fb2c324   |                                                                                 |
| opencontent/googlesheet          |          | 7d19be0   |                                                                                 |
| opencontent/oci18n               |          | c67c37e   |                                                                                 |
| opencontent/ocsensor-ls          | 5.19.1   | 6.0.0     | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.19.1...6.0.0)       |
| opencontent/ocsensorapi          | 5.14.3   | 6.0.0     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.3...6.0.0)    |
| opencontent/openpa_sensor-ls     | 4.6.0    | 6.0.0     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.6.0...6.0.0)   |
| paragonie/constant_time_encoding |          | v2.5.0    |                                                                                 |
| paragonie/random_compat          |          | v9.99.100 |                                                                                 |
| phpseclib/phpseclib              |          | 3.0.x-dev |                                                                                 |
| psr/cache                        |          | 1.0.1     |                                                                                 |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.19.1 and 6.0.0](https://github.com/OpencontentCoop/ocsensor/compare/5.19.1...6.0.0)**
* Introduce la versione internazionalizzabile

**[opencontent/ocsensorapi changes between 5.14.3 and 6.0.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.3...6.0.0)**
* Add i18n feature

**[opencontent/openpa_sensor-ls changes between 4.6.0 and 6.0.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.6.0...6.0.0)**
* Adatta il codice alla versione internazionalizzabile



## [2.6.6](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.5...2.6.6) - 2022-01-22


#### Code dependencies
| Changes                 | From    | To       | Compare                                                                       |
|-------------------------|---------|----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.209.9 | 3.209.10 | [...](https://github.com/aws/aws-sdk-php/compare/3.209.9...3.209.10)          |
| opencontent/ocsensor-ls | 5.19.0  | 5.19.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.19.0...5.19.1)    |
| opencontent/ocsensorapi | 5.14.2  | 5.14.3   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.2...5.14.3) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.19.0 and 5.19.1](https://github.com/OpencontentCoop/ocsensor/compare/5.19.0...5.19.1)**
* Hotfix user creation week index plugin

**[opencontent/ocsensorapi changes between 5.14.2 and 5.14.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.2...5.14.3)**
* Hotfix week date parser


## [2.6.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.4...2.6.5) - 2022-01-21


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                       |
|-------------------------|--------|--------|-------------------------------------------------------------------------------|
| opencontent/ocsensorapi | 5.14.1 | 5.14.2 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.1...5.14.2) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.14.1 and 5.14.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.1...5.14.2)**
* Increase facet limit in statistic queries


## [2.6.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.3...2.6.4) - 2022-01-21


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                    |
|-------------------------|--------|--------|----------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.18.1 | 5.19.0 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.18.1...5.19.0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.18.1 and 5.19.0](https://github.com/OpencontentCoop/ocsensor/compare/5.18.1...5.19.0)**
* Visualizza i suggerimenti di selezione delle categorie se è configurato e abilitato un endpoint predictor


## [2.6.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.2...2.6.3) - 2022-01-20


#### Code dependencies
| Changes                      | From    | To      | Compare                                                                       |
|------------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.209.6 | 3.209.9 | [...](https://github.com/aws/aws-sdk-php/compare/3.209.6...3.209.9)           |
| opencontent/ocsensor-ls      | 5.18.0  | 5.18.1  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.18.0...5.18.1)    |
| opencontent/ocsensorapi      | 5.14.0  | 5.14.1  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.0...5.14.1) |
| zetacomponents/console-tools | 1.7.2   | 1.7.3   | [...](https://github.com/zetacomponents/ConsoleTools/compare/1.7.2...1.7.3)   |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.18.0 and 5.18.1](https://github.com/OpencontentCoop/ocsensor/compare/5.18.0...5.18.1)**
* Uniforma la selezione del gruppo di incaricati nei filtri delle statistiche

**[opencontent/ocsensorapi changes between 5.14.0 and 5.14.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.14.0...5.14.1)**
* Reparenting owner groups stats


## [2.6.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.1...2.6.2) - 2022-01-18


#### Code dependencies
| Changes                          | From    | To        | Compare                                                                              |
|----------------------------------|---------|-----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.208.5 | 3.209.6   | [...](https://github.com/aws/aws-sdk-php/compare/3.208.5...3.209.6)                  |
| friendsofsymfony/http-cache      | 2.11.0  | 2.12.0    | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/2.11.0...2.12.0)      |
| lcobucci/jwt                     |         | 3.4.x-dev |                                                                                      |
| opencontent/ocbootstrap-ls       | 1.10.12 | 1.10.13   | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.12...1.10.13)      |
| opencontent/occustomfind-ls      | 2.3.0   | 2.4.0     | [...](https://github.com/OpencontentCoop/occustomfind/compare/2.3.0...2.4.0)         |
| opencontent/ocdedalogin-ls       |         | 1.0.2     |                                                                                      |
| opencontent/ocembed-ls           | 1.4.2   | 1.5.2     | [...](https://github.com/OpencontentCoop/ocembed/compare/1.4.2...1.5.2)              |
| opencontent/ocopendata-ls        | 2.27.4  | 2.28.0    | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.27.4...2.28.0)         |
| opencontent/ocsensor-ls          | 5.17.0  | 5.18.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.17.0...5.18.0)           |
| opencontent/ocsensorapi          | 5.13.1  | 5.14.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.13.1...5.14.0)        |
| opencontent/openpa-ls            | 3.13.5  | 3.13.7    | [...](https://github.com/OpencontentCoop/openpa/compare/3.13.5...3.13.7)             |
| opencontent/openpa_sensor-ls     | 4.5.2   | 4.6.0     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.2...4.6.0)        |
| predis/predis                    | 4c1aada | b3a5bdf   | [...](https://github.com/predis/predis/compare/4c1aada...b3a5bdf)                    |
| symfony/http-client              |         | 5.4.x-dev |                                                                                      |
| symfony/http-client-contracts    |         | 2.5.x-dev |                                                                                      |
| symfony/polyfill-ctype           | 3088518 | v1.24.0   | [...](https://github.com/symfony/polyfill-ctype/compare/3088518...v1.24.0)           |
| symfony/polyfill-intl-idn        | 749045c | v1.24.0   | [...](https://github.com/symfony/polyfill-intl-idn/compare/749045c...v1.24.0)        |
| symfony/polyfill-intl-normalizer | 8590a5f | v1.24.0   | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/8590a5f...v1.24.0) |
| symfony/polyfill-mbstring        | 0abb51d | v1.24.0   | [...](https://github.com/symfony/polyfill-mbstring/compare/0abb51d...v1.24.0)        |
| symfony/polyfill-php72           | 9a14221 | v1.24.0   | [...](https://github.com/symfony/polyfill-php72/compare/9a14221...v1.24.0)           |
| symfony/polyfill-php73           | cc5db0e | v1.24.0   | [...](https://github.com/symfony/polyfill-php73/compare/cc5db0e...v1.24.0)           |
| symfony/polyfill-php80           | 57b712b | v1.24.0   | [...](https://github.com/symfony/polyfill-php80/compare/57b712b...v1.24.0)           |
| symfony/service-contracts        |         | 2.5.x-dev |                                                                                      |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.12 and 1.10.13](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.12...1.10.13)**
* Corregge un bug di visualizzazione del template di modifica di ezcountry

**[opencontent/occustomfind-ls changes between 2.3.0 and 2.4.0](https://github.com/OpencontentCoop/occustomfind/compare/2.3.0...2.4.0)**
* Dataset datatype: add map and counter view, allow import from google sheet, add email and url field type

**[opencontent/ocembed-ls changes between 1.4.2 and 1.5.2](https://github.com/OpencontentCoop/ocembed/compare/1.4.2...1.5.2)**
* Rifattorizza il sistema di cache Introduce un ezpfilter per il contenuto html Introduce un modulo di preview
* Fix bug in autoembed
* Verify max resolution image in youtube workaround

**[opencontent/ocopendata-ls changes between 2.27.4 and 2.28.0](https://github.com/OpencontentCoop/ocopendata/compare/2.27.4...2.28.0)**
* Rebuild parent nodes according to payload params in api update Avoid duplicate locations in api move

**[opencontent/ocsensor-ls changes between 5.17.0 and 5.18.0](https://github.com/OpencontentCoop/ocsensor/compare/5.17.0...5.18.0)**
* Reindicizza in background le segnalazioni all'aggiornamento dei gruppi di un operatore
* Corregge un errore nell'indicizzazione degli utenti
* Migliora l'interfaccia delle statistiche e introduce i tag group selezionabili

**[opencontent/ocsensorapi changes between 5.13.1 and 5.14.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.13.1...5.14.0)**
* Fix some minor bugs
* Allow select single tag group in owner group stats

**[opencontent/openpa-ls changes between 3.13.5 and 3.13.7](https://github.com/OpencontentCoop/openpa/compare/3.13.5...3.13.7)**
* Allow using redis in cluster mode
* Corregge un bug nel sistema di calcolo di cambio stato in caso di attributi di tipo ezdate

**[opencontent/openpa_sensor-ls changes between 4.5.2 and 4.6.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.2...4.6.0)**
* Scatena l'evento on_update_operator all'aggiornamento dell'operatore


## [2.6.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.6.0...2.6.1) - 2021-12-14


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                       |
|-------------------------|--------|--------|-------------------------------------------------------------------------------|
| opencontent/ocsensorapi | 5.13.0 | 5.13.1 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.13.0...5.13.1) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.13.0 and 5.13.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.13.0...5.13.1)**
* Fix user can comment flag


## [2.6.0](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.40...2.6.0) - 2021-12-14
- Add pdf compliant image aliases

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                       |
|-------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.208.1 | 3.208.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.208.1...3.208.5)           |
| opencontent/ocsensor-ls | 5.16.7  | 5.17.0  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.16.7...5.17.0)    |
| opencontent/ocsensorapi | 5.12.6  | 5.13.0  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.6...5.13.0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.16.7 and 5.17.0](https://github.com/OpencontentCoop/ocsensor/compare/5.16.7...5.17.0)**
* Permette di visualizzare in inbox tutte le note private (anche quelle non indirizzate all'utente corrente) Permette di scaricare la singola segnalazione in pdf Introduce i gruppi di utenti per customizzare la selezione delle categorie Permette di abilitare l’accesso a una singola statistica per utente operatore o gruppo di utenti

**[opencontent/ocsensorapi changes between 5.12.6 and 5.13.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.6...5.13.0)**
* Remove SensorUserInfo Add node_id in TreeItem Minor fixes


## [2.5.40](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.39...2.5.40) - 2021-12-06


#### Code dependencies
| Changes                   | From    | To      | Compare                                                                         |
|---------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.205.0 | 3.208.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.205.0...3.208.1)             |
| opencontent/ocinstaller   | 3a84656 | 828b25d | [...](https://github.com/OpencontentCoop/ocinstaller/compare/3a84656...828b25d) |
| opencontent/ocsensor-ls   | 5.16.6  | 5.16.7  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.16.6...5.16.7)      |
| symfony/polyfill-mbstring | 11b9acb | 0abb51d | [...](https://github.com/symfony/polyfill-mbstring/compare/11b9acb...0abb51d)   |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 3a84656 and 828b25d](https://github.com/OpencontentCoop/ocinstaller/compare/3a84656...828b25d)**
* Avoid workflow override

**[opencontent/ocsensor-ls changes between 5.16.6 and 5.16.7](https://github.com/OpencontentCoop/ocsensor/compare/5.16.6...5.16.7)**
* Corregge un errore di assegnazione delle etichette Ripristina il click sulla riga nell'interfaccia Le mie attività


## [2.5.39](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.38...2.5.39) - 2021-11-29


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                    |
|-------------------------|---------|---------|----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.204.4 | 3.205.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.204.4...3.205.0)        |
| mjaschen/phpgeo         | d6b7a08 | 3.2.1   | [...](https://github.com/mjaschen/phpgeo/compare/d6b7a08...3.2.1)          |
| opencontent/ocsensor-ls | 5.16.5  | 5.16.6  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.16.5...5.16.6) |
| php-http/client-common  | 2.4.0   | 2.5.0   | [...](https://github.com/php-http/client-common/compare/2.4.0...2.5.0)     |
| zetacomponents/cache    | 1.6     | 1.6.1   | [...](https://github.com/zetacomponents/Cache/compare/1.6...1.6.1)         |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.16.5 and 5.16.6](https://github.com/OpencontentCoop/ocsensor/compare/5.16.5...5.16.6)**
* Decodifica le entità html in esportazione csv


## [2.5.38](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.37...2.5.38) - 2021-11-24


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                       |
|-------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.203.1 | 3.204.4 | [...](https://github.com/aws/aws-sdk-php/compare/3.203.1...3.204.4)           |
| opencontent/ocsensor-ls | 5.16.4  | 5.16.5  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.16.4...5.16.5)    |
| opencontent/ocsensorapi | 5.12.5  | 5.12.6  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.5...5.12.6) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.16.4 and 5.16.5](https://github.com/OpencontentCoop/ocsensor/compare/5.16.4...5.16.5)**
* Migliora la versione stampabile dei report aggiungendo i numeri di pagina e ricalibrando i parametri di esportazione dei grafici
* Aggiunge il campo descrizione nell'esportazione delle segnalazioni in csv
* Permette di filtrare per macro gruppi nella pagina segnalazioni
* Permette di marcare come preferita una segnalazione dalla dashboard

**[opencontent/ocsensorapi changes between 5.12.5 and 5.12.6](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.5...5.12.6)**
* Fix facet limits in trend stat


## [2.5.37](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.36...2.5.37) - 2021-11-16


#### Code dependencies
| Changes                   | From    | To      | Compare                                                                       |
|---------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.202.1 | 3.203.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.202.1...3.203.1)           |
| opencontent/ocopendata-ls | 2.27.3  | 2.27.4  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.27.3...2.27.4)  |
| opencontent/ocsensor-ls   | 5.16.3  | 5.16.4  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.16.3...5.16.4)    |
| opencontent/ocsensorapi   | 5.12.4  | 5.12.5  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.4...5.12.5) |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.27.3 and 2.27.4](https://github.com/OpencontentCoop/ocopendata/compare/2.27.3...2.27.4)**
* Fix user attribute converter on disabled user

**[opencontent/ocsensor-ls changes between 5.16.3 and 5.16.4](https://github.com/OpencontentCoop/ocsensor/compare/5.16.3...5.16.4)**
* Impedisce l'assegnazione a un operatore disabilitato Espone l'interfaccia di modifica dei settings dell'operatore in modalità ajax
* Espone la lista dei filtri disponibili per i grafici di trend ad uso dei editor di report
* Migliora la versione stampabile del report

**[opencontent/ocsensorapi changes between 5.12.4 and 5.12.5](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.4...5.12.5)**
* Check user enabling in tree node item
* Expose daily report filters id
* Refine StatusPerOwnerGroup stat filters


## [2.5.36](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.35...2.5.36) - 2021-11-12


#### Code dependencies
| Changes                    | From    | To      | Compare                                                                         |
|----------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.200.1 | 3.202.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.200.1...3.202.1)             |
| opencontent/ocbootstrap-ls | 1.10.11 | 1.10.12 | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.11...1.10.12) |
| opencontent/ocopendata-ls  | 2.27.2  | 2.27.3  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.27.2...2.27.3)    |
| opencontent/ocsupport-ls   | 184cf85 | c4e37a9 | [...](https://github.com/OpencontentCoop/ocsupport/compare/184cf85...c4e37a9)   |
| opencontent/openpa-ls      | 3.13.4  | 3.13.5  | [...](https://github.com/OpencontentCoop/openpa/compare/3.13.4...3.13.5)        |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.11 and 1.10.12](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.11...1.10.12)**
* Corregge alcune traduzioni in tedesco

**[opencontent/ocopendata-ls changes between 2.27.2 and 2.27.3](https://github.com/OpencontentCoop/ocopendata/compare/2.27.2...2.27.3)**
* Hotfix reloading modules

**[opencontent/ocsupport-ls changes between 184cf85 and c4e37a9](https://github.com/OpencontentCoop/ocsupport/compare/184cf85...c4e37a9)**
* Add run cronjob handler

**[opencontent/openpa-ls changes between 3.13.4 and 3.13.5](https://github.com/OpencontentCoop/openpa/compare/3.13.4...3.13.5)**
* Remove unused code


## [2.5.35](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.34...2.5.35) - 2021-11-07
- Remove shared volumes in docker-compose-dev file
- Update php and nginx base images to 1.2.3-v2

#### Code dependencies
| Changes                              | From        | To          | Compare                                                                                    |
|--------------------------------------|-------------|-------------|--------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                      | 3.198.5     | 3.200.1     | [...](https://github.com/aws/aws-sdk-php/compare/3.198.5...3.200.1)                        |
| composer/installers                  | v1.0.25     | a241e78     | [...](https://github.com/composer/installers/compare/v1.0.25...a241e78)                    |
| erasys/openapi-php                   | 9885e8f     | 3.3.0       | [...](https://github.com/erasys/openapi-php/compare/9885e8f...3.3.0)                       |
| ezsystems/ezpublish-legacy           | 2020.1000.6 | 2020.1000.7 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.6...2020.1000.7)   |
| ezsystems/ezpublish-legacy-installer | 38e402c     | d803123     | [...](https://github.com/Opencontent/ezpublish-legacy-installer/compare/38e402c...d803123) |
| friendsofsymfony/http-cache          | 239d08d     | 2.11.0      | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/239d08d...2.11.0)           |
| guzzlehttp/promises                  | b2e8301     | fe752ae     | [...](https://github.com/guzzle/promises/compare/b2e8301...fe752ae)                        |
| illuminate/contracts                 | 6.x-dev     | 7.x-dev     | [...](https://github.com/illuminate/contracts/compare/6.x-dev...7.x-dev)                   |
| opencontent/ocbootstrap-ls           | 1.10.10     | 1.10.11     | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.10...1.10.11)            |
| opencontent/ocopendata-ls            | 2.27.0      | 2.27.2      | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.27.0...2.27.2)               |
| opencontent/ocsensor-ls              | 5.16.2      | 5.16.3      | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.16.2...5.16.3)                 |
| opencontent/ocsupport-ls             | 8071c61     | 184cf85     | [...](https://github.com/OpencontentCoop/ocsupport/compare/8071c61...184cf85)              |
| opencontent/ocwebhookserver-ls       | 1.1.4       | 1.1.5       | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.4...1.1.5)            |
| opencontent/openpa-ls                | 3.13.3      | 3.13.4      | [...](https://github.com/OpencontentCoop/openpa/compare/3.13.3...3.13.4)                   |
| psr/simple-cache                     | 66e27ef     | 1.0.1       | [...](https://github.com/php-fig/simple-cache/compare/66e27ef...1.0.1)                     |
| symfony/event-dispatcher             | 5.x-dev     | 5.4.x-dev   | [...](https://github.com/symfony/event-dispatcher/compare/5.x-dev...5.4.x-dev)             |
| symfony/options-resolver             | 5.x-dev     | 5.4.x-dev   | [...](https://github.com/symfony/options-resolver/compare/5.x-dev...5.4.x-dev)             |
| symfony/polyfill-ctype               | f24ae46     | 3088518     | [...](https://github.com/symfony/polyfill-ctype/compare/f24ae46...3088518)                 |
| symfony/polyfill-mbstring            | 344e456     | 11b9acb     | [...](https://github.com/symfony/polyfill-mbstring/compare/344e456...11b9acb)              |
| symfony/yaml                         | 4.4.x-dev   | 5.4.x-dev   | [...](https://github.com/symfony/yaml/compare/4.4.x-dev...5.4.x-dev)                       |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.10 and 1.10.11](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.10...1.10.11)**
* Aggiorna le stringhe di traduzione

**[opencontent/ocopendata-ls changes between 2.27.0 and 2.27.2](https://github.com/OpencontentCoop/ocopendata/compare/2.27.0...2.27.2)**
* Fix http status code in exceptions
* Change base attribute validation (from string to scalar)
* Reload modules in ContentRepository

**[opencontent/ocsensor-ls changes between 5.16.2 and 5.16.3](https://github.com/OpencontentCoop/ocsensor/compare/5.16.2...5.16.3)**
* Aumenta a 15 il numero di suggerimenti restituiti dal geocoder Geoserver

**[opencontent/ocsupport-ls changes between 8071c61 and 184cf85](https://github.com/OpencontentCoop/ocsupport/compare/8071c61...184cf85)**
* Fix github auth api call Add from to options to make-changeòpg

**[opencontent/ocwebhookserver-ls changes between 1.1.4 and 1.1.5](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.4...1.1.5)**
* Add job stats

**[opencontent/openpa-ls changes between 3.13.3 and 3.13.4](https://github.com/OpencontentCoop/openpa/compare/3.13.3...3.13.4)**
* Avoid php 7.3 warning


## [2.5.34](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.33...2.5.34) - 2021-10-15
- Added Monselice to publiccode.yml

#### Code dependencies
| Changes                       | From    | To      | Compare                                                                           |
|-------------------------------|---------|---------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php               | 3.195.0 | 3.198.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.195.0...3.198.5)               |
| friendsofsymfony/http-cache   | 9bdf22d | 239d08d | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/9bdf22d...239d08d) |
| guzzlehttp/promises           | c1dd809 | b2e8301 | [...](https://github.com/guzzle/promises/compare/c1dd809...b2e8301)               |
| opencontent/ocsensorapi       | 5.12.3  | 5.12.4  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.3...5.12.4)     |
| psr/simple-cache              | 5a7b96b | 66e27ef | [...](https://github.com/php-fig/simple-cache/compare/5a7b96b...66e27ef)          |
| symfony/deprecation-contracts | 6f981ee |         |                                                                                   |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.12.3 and 5.12.4](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.3...5.12.4)**
* Fix group and operators read access in mail notification listener


## [2.5.33](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.32...2.5.33) - 2021-09-28


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                    |
|-------------------------|--------|--------|----------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.16.1 | 5.16.2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.16.1...5.16.2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.16.1 and 5.16.2](https://github.com/OpencontentCoop/ocsensor/compare/5.16.1...5.16.2)**
* Fix typo
* Disabilita per default HighchartsExport server


## [2.5.32](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.31...2.5.32) - 2021-09-28



#### Installer
- Add sensor_report_item images

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                    |
|-------------------------|---------|---------|----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.194.3 | 3.195.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.194.3...3.195.0)        |
| opencontent/ocsensor-ls | 5.15.7  | 5.16.1  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.15.7...5.16.1) |
| ramsey/uuid             | 4.2.1   | 4.2.3   | [...](https://github.com/ramsey/uuid/compare/4.2.1...4.2.3)                |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.15.7 and 5.16.1](https://github.com/OpencontentCoop/ocsensor/compare/5.15.7...5.16.1)**
* Corregge un bug nel parsing del link del report item
* Migliora la visualizzazione della lista di report
* Espone una versione stampabile del report
* Espone il bottone per stampare il report


## [2.5.31](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.30...2.5.31) - 2021-09-23


#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.194.1 | 3.194.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.194.1...3.194.3)               |
| friendsofsymfony/http-cache | a501495 | 9bdf22d | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/a501495...9bdf22d) |
| opencontent/ocopendata-ls   | 2.25.7  | 2.27.0  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.7...2.27.0)      |
| opencontent/ocsensor-ls     | 5.15.6  | 5.15.7  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.15.6...5.15.7)        |
| php-http/discovery          | 1.14.0  | 1.14.1  | [...](https://github.com/php-http/discovery/compare/1.14.0...1.14.1)              |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.25.7 and 2.27.0](https://github.com/OpencontentCoop/ocopendata/compare/2.25.7...2.27.0)**
* Add delete and move api
* Add upsert api

**[opencontent/ocsensor-ls changes between 5.15.6 and 5.15.7](https://github.com/OpencontentCoop/ocsensor/compare/5.15.6...5.15.7)**
* Corregge un bug nella traduzione dei giorni della settimana nei report


## [2.5.30](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.29...2.5.30) - 2021-09-18


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                    |
|-------------------------|--------|--------|----------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.15.5 | 5.15.6 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.15.5...5.15.6) |

## [2.5.29](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.28...2.5.29) - 2021-09-18



#### Installer
- Add report item field to avoid single parameter override

#### Code dependencies
| Changes                   | From    | To      | Compare                                                                       |
|---------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.193.1 | 3.194.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.193.1...3.194.1)           |
| opencontent/ocsensor-ls   | 5.15.3  | 5.15.5  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.15.3...5.15.5)    |
| symfony/polyfill-ctype    | 46cd957 | f24ae46 | [...](https://github.com/symfony/polyfill-ctype/compare/46cd957...f24ae46)    |
| symfony/polyfill-intl-idn | 65bd267 | 749045c | [...](https://github.com/symfony/polyfill-intl-idn/compare/65bd267...749045c) |
| symfony/polyfill-mbstring | 9174a3d | 344e456 | [...](https://github.com/symfony/polyfill-mbstring/compare/9174a3d...344e456) |
| symfony/polyfill-php73    | fba8933 | cc5db0e | [...](https://github.com/symfony/polyfill-php73/compare/fba8933...cc5db0e)    |
| symfony/polyfill-php80    | 1100343 | 57b712b | [...](https://github.com/symfony/polyfill-php80/compare/1100343...57b712b)    |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.15.3 and 5.15.5](https://github.com/OpencontentCoop/ocsensor/compare/5.15.3...5.15.5)**
* Corregge alcuni problemi di visualizzazione dei report
* Permette di selezionare quali parametri preservare in report item


## [2.5.28](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.27...2.5.28) - 2021-09-10


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                    |
|-------------------------|--------|--------|----------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.15.2 | 5.15.3 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.15.2...5.15.3) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.15.2 and 5.15.3](https://github.com/OpencontentCoop/ocsensor/compare/5.15.2...5.15.3)**
* Corregge alcuni bug nella gui dei reports


## [2.5.27](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.26...2.5.27) - 2021-09-10



#### Installer
- Add sensor_report_item/avoid_override field update to v0.0.3

#### Code dependencies
| Changes                    | From    | To      | Compare                                                                        |
|----------------------------|---------|---------|--------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.192.1 | 3.193.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.192.1...3.193.1)            |
| opencontent/ocbootstrap-ls | 1.10.9  | 1.10.10 | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.9...1.10.10) |
| opencontent/ocsensor-ls    | 5.15.1  | 5.15.2  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.15.1...5.15.2)     |
| opencontent/ocsensorapi    | 5.12.2  | 5.12.3  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.2...5.12.3)  |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.9 and 1.10.10](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.9...1.10.10)**
* Corregge un bug nel formato delle date prodotte dal calendario di supporto di ocevent

**[opencontent/ocsensor-ls changes between 5.15.1 and 5.15.2](https://github.com/OpencontentCoop/ocsensor/compare/5.15.1...5.15.2)**
* Permette di escludere una pagina del report dalle sovrascritture dei parametri

**[opencontent/ocsensorapi changes between 5.12.2 and 5.12.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.2...5.12.3)**
* Operator can read unmoderated comments


## [2.5.26](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.25...2.5.26) - 2021-09-08



#### Installer
- Add override_link_parameters field in sensor_report

#### Code dependencies
| Changes                        | From    | To      | Compare                                                                         |
|--------------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-crt-php                |         | v1.0.2  |                                                                                 |
| aws/aws-sdk-php                | 3.191.7 | 3.192.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.191.7...3.192.1)             |
| guzzlehttp/promises            | 8e7d04f | c1dd809 | [...](https://github.com/guzzle/promises/compare/8e7d04f...c1dd809)             |
| opencontent/ocsensor-ls        | 5.15.0  | 5.15.1  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.15.0...5.15.1)      |
| opencontent/ocsensorapi        | 5.12.1  | 5.12.2  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.1...5.12.2)   |
| opencontent/ocwebhookserver-ls | 1.1.3   | 1.1.4   | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.3...1.1.4) |
| opencontent/openpa-ls          | 3.13.2  | 3.13.3  | [...](https://github.com/OpencontentCoop/openpa/compare/3.13.2...3.13.3)        |
| predis/predis                  | 54775a0 | 4c1aada | [...](https://github.com/predis/predis/compare/54775a0...4c1aada)               |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.15.0 and 5.15.1](https://github.com/OpencontentCoop/ocsensor/compare/5.15.0...5.15.1)**
* Corregge un bug dell'interfaccia per cui un utente urp poteva auto assegnarsi una segnalazione
* Corregge un problema nel form di inserimento per cui le informazioni geografiche non venivano correttamente resettate alla cancellazione dell'indirizzo
* Permette di archiviare i report
* Corregge un bug nella traduzione dei giorni della settimana nelle statistiche
* Permette di configurare dei parametri di override per i link dei report

**[opencontent/ocsensorapi changes between 5.12.1 and 5.12.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.1...5.12.2)**
* Avoid auto assign for default approver group
* Allow use guid as postId in api

**[opencontent/ocwebhookserver-ls changes between 1.1.3 and 1.1.4](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.3...1.1.4)**
* Add bootstrapitalia design

**[opencontent/openpa-ls changes between 3.13.2 and 3.13.3](https://github.com/OpencontentCoop/openpa/compare/3.13.2...3.13.3)**
* Corregge le regole di default del robots.txt per permettere l'indicizzazione dei contenuti opendata


## [2.5.25](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.24...2.5.25) - 2021-09-03


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                       |
|-------------------------|--------|--------|-------------------------------------------------------------------------------|
| opencontent/ocsensorapi | 5.12.0 | 5.12.1 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.0...5.12.1) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.12.0 and 5.12.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.12.0...5.12.1)**
* Fix areas api result


## [2.5.24](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.23...2.5.24) - 2021-08-31



#### Installer
- add sensor_category/disabled_areas field update to 2.4.6

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                       |
|-------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.190.1 | 3.191.7 | [...](https://github.com/aws/aws-sdk-php/compare/3.190.1...3.191.7)           |
| brick/math              | 0.9.2   | 0.9.3   | [...](https://github.com/brick/math/compare/0.9.2...0.9.3)                    |
| opencontent/ocsensor-ls | 5.14.5  | 5.15.0  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.14.5...5.15.0)    |
| opencontent/ocsensorapi | 5.11.1  | 5.12.0  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.11.1...5.12.0) |
| php-http/message        | 1.11.2  | 1.12.0  | [...](https://github.com/php-http/message/compare/1.11.2...1.12.0)            |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.14.5 and 5.15.0](https://github.com/OpencontentCoop/ocsensor/compare/5.14.5...5.15.0)**
* Permette di definire le categorie in base alle aree Permette di selezionare una zona predefinita interpretando la variabile get area

**[opencontent/ocsensorapi changes between 5.11.1 and 5.12.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.11.1...5.12.0)**
* Add area/disabled_relations field


## [2.5.23](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.22...2.5.23) - 2021-08-12


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                       |
|-------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.190.0 | 3.190.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.190.0...3.190.1)           |
| opencontent/ocsensorapi | 5.11.0  | 5.11.1  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.11.0...5.11.1) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.11.0 and 5.11.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.11.0...5.11.1)**
* Hotfix in PostService


## [2.5.22](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.21...2.5.22) - 2021-08-12


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                    |
|-------------------------|--------|--------|----------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.14.4 | 5.14.5 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.14.4...5.14.5) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.14.4 and 5.14.5](https://github.com/OpencontentCoop/ocsensor/compare/5.14.4...5.14.5)**
* hotfix report view
* Corregge un bug nell'export in csv


## [2.5.21](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.20...2.5.21) - 2021-08-12
- Update changelog and publiccode
- Disable ezuserformtoken output logs

#### Code dependencies
| Changes                        | From     | To      | Compare                                                                         |
|--------------------------------|----------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.185.17 | 3.190.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.185.17...3.190.0)            |
| brick/math                     |          | 0.9.2   |                                                                                 |
| opencontent/ocopendata-ls      | 2.25.6   | 2.25.7  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.6...2.25.7)    |
| opencontent/ocsensorapi        | 5.10.2   | 5.11.0  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.10.2...5.11.0)   |
| opencontent/ocwebhookserver-ls | 1.1.1    | 1.1.3   | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.1...1.1.3) |
| php-http/message               | 1.11.1   | 1.11.2  | [...](https://github.com/php-http/message/compare/1.11.1...1.11.2)              |
| ramsey/collection              |          | 1.1.4   |                                                                                 |
| ramsey/uuid                    |          | 4.2.1   |                                                                                 |
| symfony/polyfill-php80         | 19d03c3  | 1100343 | [...](https://github.com/symfony/polyfill-php80/compare/19d03c3...1100343)      |
| zetacomponents/base            | 1.9.1    | 1.9.3   | [...](https://github.com/zetacomponents/Base/compare/1.9.1...1.9.3)             |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.25.6 and 2.25.7](https://github.com/OpencontentCoop/ocopendata/compare/2.25.6...2.25.7)**
* Corregge il percorso di download nella conversione a ckan della risorsa di tipo eZBinaryFile
* Corregge una potenziale vulnerabilità nel repository delle api

**[opencontent/ocsensorapi changes between 5.10.2 and 5.11.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.10.2...5.11.0)**
* Add post uuid and considers it when creating a post Fix CanForceFix permission

**[opencontent/ocwebhookserver-ls changes between 1.1.1 and 1.1.3](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.1...1.1.3)**
* Make pusher request timeout configurable by ini
* Fix retry calculation
* Add single job view
* Fix cleanup on maximum number of attempts reached


## [2.5.20](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.19...2.5.20) - 2021-07-21


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                       |
|-------------------------|--------|--------|-------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.14.2 | 5.14.4 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.14.2...5.14.4)    |
| opencontent/ocsensorapi | 5.10.1 | 5.10.2 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.10.1...5.10.2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.14.2 and 5.14.4](https://github.com/OpencontentCoop/ocsensor/compare/5.14.2...5.14.4)**
* Typo fix
* Corregge un problema nella staticizzazione del report

**[opencontent/ocsensorapi changes between 5.10.1 and 5.10.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.10.1...5.10.2)**
* Fix StatisticFactory properties visibility


## [2.5.19](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.18...2.5.19) - 2021-07-21



#### Installer
- Add sensor_report static fields

#### Code dependencies
| Changes                 | From     | To       | Compare                                                                    |
|-------------------------|----------|----------|----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.185.16 | 3.185.17 | [...](https://github.com/aws/aws-sdk-php/compare/3.185.16...3.185.17)      |
| opencontent/ocsensor-ls | 5.14.1   | 5.14.2   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.14.1...5.14.2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.14.1 and 5.14.2](https://github.com/OpencontentCoop/ocsensor/compare/5.14.1...5.14.2)**
* Permette di clonare un report Corregge alcuni bug di visualizzazione dei report
* Permette di staticizzare i dati dei report


## [2.5.18](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.17...2.5.18) - 2021-07-20
- Add RUN_INSTALLER_REPORTS in install script


#### Installer
- Add reports installer module v0.0.1

#### Code dependencies
| Changes                 | From     | To       | Compare                                                                      |
|-------------------------|----------|----------|------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.185.13 | 3.185.16 | [...](https://github.com/aws/aws-sdk-php/compare/3.185.13...3.185.16)        |
| opencontent/ocsensor-ls | 5.13.2   | 5.14.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.13.2...5.14.1)   |
| opencontent/ocsensorapi | 5.9.4    | 5.10.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.9.4...5.10.1) |
| predis/predis           | 367bd72  | 54775a0  | [...](https://github.com/predis/predis/compare/367bd72...54775a0)            |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.13.2 and 5.14.1](https://github.com/OpencontentCoop/ocsensor/compare/5.13.2...5.14.1)**
* Permette di filtrare i risultati della inbox per tipologia di segnalazione
* Introduce una modifica ai grafici per cui il motore dei grafici in modo potrà essere sostituito
* Corregge un big di visualizzazione nella inbox
* Introduce il modulo reports (beta)
* Permette di dis/abilitare da configurazione i filtri e le azioni veloci della inbox

**[opencontent/ocsensorapi changes between 5.9.4 and 5.10.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.9.4...5.10.1)**
* Add stat format parameters Expose stat config from api
* Avoid php notice in stat export
* Fix trend stat visibility if has filter
* Avoid policy error in stat queries
* Fix OpenHistoryPerOwnerGroup stat query Fix PostAging stat title


## [2.5.17](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.16...2.5.17) - 2021-07-15



#### Installer
- Bugfix in webhook sql update

#### Code dependencies
| Changes                        | From   | To     | Compare                                                                         |
|--------------------------------|--------|--------|---------------------------------------------------------------------------------|
| opencontent/ocsensor-ls        | 5.13.1 | 5.13.2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.13.1...5.13.2)      |
| opencontent/ocsensorapi        | 5.9.2  | 5.9.4  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.9.2...5.9.4)     |
| opencontent/ocwebhookserver-ls | 1.1.0  | 1.1.1  | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.0...1.1.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.13.1 and 5.13.2](https://github.com/OpencontentCoop/ocsensor/compare/5.13.1...5.13.2)**
* Corregge un bug nel grafico di stato

**[opencontent/ocsensorapi changes between 5.9.2 and 5.9.4](https://github.com/OpencontentCoop/ocsensorapi/compare/5.9.2...5.9.4)**
* Add sort parameters in get posts api
* Fix group read access in assign action
* Hide OnFixNotification to standard user
* Bugfix in ExecutionTrend stat

**[opencontent/ocwebhookserver-ls changes between 1.1.0 and 1.1.1](https://github.com/OpencontentCoop/ocwebhookserver/compare/1.1.0...1.1.1)**
* Bugfix sql update


## [2.5.16](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.15...2.5.16) - 2021-07-15
- Add RUN_INSTALLER in docker-compose
- Avoid duplicate log tail


#### Installer
- Add webhooks update sql Minor bug fixes

#### Code dependencies
| Changes                            | From    | To        | Compare                                                                                 |
|------------------------------------|---------|-----------|-----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.185.9 | 3.185.13  | [...](https://github.com/aws/aws-sdk-php/compare/3.185.9...3.185.13)                    |
| mjaschen/phpgeo                    |         | d6b7a08   |                                                                                         |
| opencontent/ocsensor-ls            | 5.13.0  | 5.13.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.13.0...5.13.1)              |
| opencontent/ocsensorapi            | 5.9.1   | 5.9.2     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.9.1...5.9.2)             |
| opencontent/ocwebhookserver-ls     | a60d571 | 1.1.0     | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/a60d571...1.1.0)       |
| psr/log                            | d49695b | 1.1.4     | [...](https://github.com/php-fig/log/compare/d49695b...1.1.4)                           |
| symfony/deprecation-contracts      | 36b691b | 6f981ee   | [...](https://github.com/symfony/deprecation-contracts/compare/36b691b...6f981ee)       |
| symfony/event-dispatcher-contracts | v2.4.0  | 2.5.x-dev | [...](https://github.com/symfony/event-dispatcher-contracts/compare/v2.4.0...2.5.x-dev) |
| symfony/polyfill-php80             | eca0bf4 | 19d03c3   | [...](https://github.com/symfony/polyfill-php80/compare/eca0bf4...19d03c3)              |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.13.0 and 5.13.1](https://github.com/OpencontentCoop/ocsensor/compare/5.13.0...5.13.1)**
* Aggiunge il filtro per gruppo e la selezione del periodo nel grafico di stato
* Corregge un problema di visibilità dei settings in sensor repository
* Aggiunge il grafico Storico aperte per gruppo

**[opencontent/ocsensorapi changes between 5.9.1 and 5.9.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.9.1...5.9.2)**
* Add range filter in StatusPercentage stat
* Validate Area and GeoLocation consistency
* Add OpenHistoryPerOwnerGroup stat

**[opencontent/ocwebhookserver-ls changes between a60d571 and 1.1.0](https://github.com/OpencontentCoop/ocwebhookserver/compare/a60d571...1.1.0)**
* Add retry system


## [2.5.15](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.14...2.5.15) - 2021-07-09
- Fix docker-compose
- Add solr-backup directory in gitignore
- Fix Dockerfile.solr volume Add solr-backup script

#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.185.4 | 3.185.9 | [...](https://github.com/aws/aws-sdk-php/compare/3.185.4...3.185.9)               |
| friendsofsymfony/http-cache | cbc3f7d | a501495 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/cbc3f7d...a501495) |
| opencontent/ocsensor-ls     | 5.12.4  | 5.13.0  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.12.4...5.13.0)        |
| opencontent/ocsensorapi     | 5.8.3   | 5.9.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.8.3...5.9.1)       |
| php-http/client-common      | e37e46c | 2.4.0   | [...](https://github.com/php-http/client-common/compare/e37e46c...2.4.0)          |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.12.4 and 5.13.0](https://github.com/OpencontentCoop/ocsensor/compare/5.12.4...5.13.0)**
* Introduce l'assegnazione della categoria negli automatismi
* Permette di disattivare i colori nei grafici Aggiorna il grafico di stato con lo storico del tempo

**[opencontent/ocsensorapi changes between 5.8.3 and 5.9.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.8.3...5.9.1)**
* Fix some debugging messages
* Add category in scenario avoiding recursion
* Add intervals in status stat Add read/assign/fix/close count in SolrMapper Use UseStatCalculatedColor setting in stat
* Avoid warning in status stat


## [2.5.14](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.13...2.5.14) - 2021-07-01
- Update changelog and publiccode
- Comment auth and idp services from docker-compose
- Truncate cache table if needed before run installer Add tail sensor.log
- Log redis connector in node

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                    |
|-------------------------|---------|---------|----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.185.3 | 3.185.4 | [...](https://github.com/aws/aws-sdk-php/compare/3.185.3...3.185.4)        |
| opencontent/ocsensor-ls | 5.12.3  | 5.12.4  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.12.3...5.12.4) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.12.3 and 5.12.4](https://github.com/OpencontentCoop/ocsensor/compare/5.12.3...5.12.4)**
* Corregge un bug nel modulo inbox


## [2.5.13](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.12...2.5.13) - 2021-06-30
- Add docker-compose.dev.yml and ignore docker-compose.override.yml

#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.184.6 | 3.185.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.184.6...3.185.3)               |
| friendsofsymfony/http-cache | 6dcfc42 | cbc3f7d | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/6dcfc42...cbc3f7d) |
| mtdowling/jmespath.php      | 14fe6ef | 9b87907 | [...](https://github.com/jmespath/jmespath.php/compare/14fe6ef...9b87907)         |
| opencontent/occustomfind-ls | 2.2.3   | 2.3.0   | [...](https://github.com/OpencontentCoop/occustomfind/compare/2.2.3...2.3.0)      |
| opencontent/ocinstaller     | 95b1463 | 3a84656 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/95b1463...3a84656)   |
| opencontent/ocopendata-ls   | 2.25.5  | 2.25.6  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.5...2.25.6)      |
| opencontent/ocsensor-ls     | 5.12.2  | 5.12.3  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.12.2...5.12.3)        |
| opencontent/ocsensorapi     | 5.8.2   | 5.8.3   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.8.2...5.8.3)       |


Relevant changes by repository:

**[opencontent/occustomfind-ls changes between 2.2.3 and 2.3.0](https://github.com/OpencontentCoop/occustomfind/compare/2.2.3...2.3.0)**
* Add opendatadataset openapi provider

**[opencontent/ocinstaller changes between 95b1463 and 3a84656](https://github.com/OpencontentCoop/ocinstaller/compare/95b1463...3a84656)**
* Fix patch content attribute parser

**[opencontent/ocopendata-ls changes between 2.25.5 and 2.25.6](https://github.com/OpencontentCoop/ocopendata/compare/2.25.5...2.25.6)**
* Evita di ridondare le virgolette nelle parser delle query in formato stringa

**[opencontent/ocsensor-ls changes between 5.12.2 and 5.12.3](https://github.com/OpencontentCoop/ocsensor/compare/5.12.2...5.12.3)**
* Introduce le azioni rapide in inbox per la chiusura delle segnalazioni Migliora il caricamento della segnalazione

**[opencontent/ocsensorapi changes between 5.8.2 and 5.8.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.8.2...5.8.3)**
* Avoid some php notice
* Fix embed parmeter in pagination post result api


## [2.5.12](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.11...2.5.12) - 2021-06-18
- Activate occustomfind extension
- Allow empty redis password in node server
- Allow redis password in node server adapter

#### Code dependencies
| Changes                            | From    | To      | Compare                                                                               |
|------------------------------------|---------|---------|---------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.183.6 | 3.184.6 | [...](https://github.com/aws/aws-sdk-php/compare/3.183.6...3.184.6)                   |
| mtdowling/jmespath.php             | 30dfa00 | 14fe6ef | [...](https://github.com/jmespath/jmespath.php/compare/30dfa00...14fe6ef)             |
| opencontent/ocbootstrap-ls         | 1.10.7  | 1.10.9  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.7...1.10.9)         |
| opencontent/occustomfind-ls        |         | 2.2.3   |                                                                                       |
| opencontent/ocsensor-ls            | 5.10.2  | 5.12.2  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.10.2...5.12.2)            |
| opencontent/ocsensorapi            | 5.7.2   | 5.8.2   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.7.2...5.8.2)           |
| opencontent/openpa-ls              | c68a756 | 3.13.2  | [...](https://github.com/OpencontentCoop/openpa/compare/c68a756...3.13.2)             |
| opencontent/openpa_sensor-ls       | 4.5.1   | 4.5.2   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.1...4.5.2)         |
| php-http/discovery                 | 1.13.0  | 1.14.0  | [...](https://github.com/php-http/discovery/compare/1.13.0...1.14.0)                  |
| predis/predis                      | 1d7ccb6 | 367bd72 | [...](https://github.com/predis/predis/compare/1d7ccb6...367bd72)                     |
| symfony/deprecation-contracts      | 5f38c88 | 36b691b | [...](https://github.com/symfony/deprecation-contracts/compare/5f38c88...36b691b)     |
| symfony/event-dispatcher-contracts | 69fee1a | v2.4.0  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/69fee1a...v2.4.0) |
| symfony/polyfill-intl-idn          | 780e280 | 65bd267 | [...](https://github.com/symfony/polyfill-intl-idn/compare/780e280...65bd267)         |
| symfony/polyfill-mbstring          | 9ad2f3c | 9174a3d | [...](https://github.com/symfony/polyfill-mbstring/compare/9ad2f3c...9174a3d)         |
| symfony/polyfill-php72             | 95695b8 | 9a14221 | [...](https://github.com/symfony/polyfill-php72/compare/95695b8...9a14221)            |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.7 and 1.10.9](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.7...1.10.9)**
* Introduce un workaround per evitare che Chrome auto-popoli il campo ezuser
* Aggiorna le stringhe di traduzione

**[opencontent/ocsensor-ls changes between 5.10.2 and 5.12.2](https://github.com/OpencontentCoop/ocsensor/compare/5.10.2...5.12.2)**
* Corregge il percorso di cache degli avatars
* Evita un warning sulla chiamata a parametri di configurazione non popolati
* Visualizza il grafico trend come area
* Permette di selezionare un gruppo di operatori come Riferimento di default e verifica l'applicabilità della configurazione ForceUrpApproverOnFix
* Visualizza il numero di telefono vicino al nome del segnalatore
* Riporta la visualizzazione del grafico trend a barre
* Introduce le notifiche desktop
* Corregge un bug nella visualizzazione delle impostazioni delle notifiche
* Introduce un'indicizzazione custom per esporre i grafici di trend giornalieri
* Nei grafici utilizza una palette di colori univoca
* Migliora la visualizzazione degli ultimi assegnatari di una segnalazione e ne permette la ricerca

**[opencontent/ocsensorapi changes between 5.7.2 and 5.8.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.7.2...5.8.2)**
* Avoid php warning in StatusPercentage stat
* Reorder trend stat intervals (avoiding highcharts error)
* Avoid php warning in stat
* Add eZCollaborationItemStatus for group user (using approver as group)
* Remove duplicate on_create event
* Fix bug retrieving sensor_last_owner_user_id_i value Make generateDateTimeIndexes as global utils method
* Add render settings property in StatisticFactory
* Add ClosingTrend  stat using SensorDailyReportRepository
* Calculate the stat color from name or id
* Fix assign timeline message adding already owners
* Fix bug in scenario criteria match logic
* Reset last owner and last group if needed
* Fix stat colors

**[opencontent/openpa-ls changes between c68a756 and 3.13.2](https://github.com/OpencontentCoop/openpa/compare/c68a756...3.13.2)**
* Permette l'accesso a openpa/roles a gui custom (bootstrapitalia)
* Migliora lo script di creazione di una nuova istanza
* Corregge alcuni potenziali problemi di sicurezza xss
* Evoluzione di robots.txt per bloccare i bad crawler
* Coregge un problema nella creazione dell'oranigramma  2874
* Considera content_tag_menu nel calcolo del tree menu
* In openpa/object viene considerato il main node
* Aggiunge WhatsApp e Telegram tra i valori possibili dei contatti
* Aggiunge il codice SDI ai contatti
* Aggiunge la possibilità di escludere alberature dai menu
* Rimuovo ruolo e ruolo2 da ContentMain/AbstractIdentifiers  1940
* Aggiunge TikTok ai possibili contatti
* Aggiunge il link all'area personale ai valori inseribili nei contatti
* Corregge un errore nella funzione di template per la ricerca delle strutture
* Corregge un bug nella definizione degli handler dei blocchi
* Permette la personalizzazione dei valori head/meta da openpa/seo
* Visualizza la versione dell'installer se presente in CreditSettings/CodeVersion
* Corregge un bug in openpa/object
* Corregge la fetch per il controllo degli stati
* Permette a content_link di riconoscere se un link è al nodo o a un nodo/link esterno
* Corregge un possibile errore nel calcolo delle aree tematiche
* Corregge un typo in cookie
* Migliora la visualizzazione della versione del software
* Corregge l'id del tree menu in caso di menu virtualizzato su alberatura di tag
* Permette la configurazione di Google Recaptcha v3
* Aggiorna le configurazioni degli attributi di classe direttamente dal openpa/recaptcha
* Permette l'inserimento del codice Web Analytics Italia
* Fixed key definition name in openpaini
* Permette di gestire tramite permessi il menu trasparenza
* Merge branch 'master' into version3   Conflicts:  	bin/php/create_instance.php  	classes/openpaini.php  	design/standard/templates/openpa/seo.tpl
* Corregge un bug nel modulo usage/metrics
* Corregge un possibile errore nel salvataggio della matrice dei contatti (e introduce uno script per il fix del pregresso)
* Merge branch 'master' into version3 Corregge un possibile errore nel salvataggio della matrice dei contatti (e introduce uno script per il fix del pregresso)
* Ordina le serie nel grafico delle partecipazioni
* Merge branch 'master' into version3
* Considera l'effettiva capacità di lettura dell'utente degli oggetti correlati nella definizione di hasContent per attributi di tipo eZObjectRelationListType
* Corregge alcuni bug minori
* Nella configurazione dispatcher cluster: sostituisce l'handler local con s3_private_cache e rende i bucket configurabili separatamente
* Corregge la lingua di destinazione nel modulo per la copia dell'oggetto (openpa/add)
* Corregge una vulnerabilità xss nel nome utente
* Merge branch 'master' into version3

**[opencontent/openpa_sensor-ls changes between 4.5.1 and 4.5.2](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.1...4.5.2)**
* Corregge il punto di emissione dell'evento on_create


## [2.5.11](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.10...2.5.11) - 2021-05-27


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                     |
|-------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocsensorapi | 5.7.1 | 5.7.2 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.7.1...5.7.2) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.7.1 and 5.7.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.7.1...5.7.2)**
* Fix image api url encoding


## [2.5.10](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.9...2.5.10) - 2021-05-26


#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.183.1 | 3.183.6 | [...](https://github.com/aws/aws-sdk-php/compare/3.183.1...3.183.6)               |
| friendsofsymfony/http-cache | fa9abf6 | 6dcfc42 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/fa9abf6...6dcfc42) |
| opencontent/ocopendata-ls   | 2.25.4  | 2.25.5  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.4...2.25.5)      |
| opencontent/ocsensor-ls     | 5.10.1  | 5.10.2  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.10.1...5.10.2)        |
| opencontent/ocsensorapi     | 5.7.0   | 5.7.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.7.0...5.7.1)       |
| php-http/message            | 1.11.0  | 1.11.1  | [...](https://github.com/php-http/message/compare/1.11.0...1.11.1)                |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.25.4 and 2.25.5](https://github.com/OpencontentCoop/ocopendata/compare/2.25.4...2.25.5)**
* Permette di definire una blacklist dei metadati esposti agli utenti anonimi

**[opencontent/ocsensor-ls changes between 5.10.1 and 5.10.2](https://github.com/OpencontentCoop/ocsensor/compare/5.10.1...5.10.2)**
* Allow api header settings injection from env

**[opencontent/ocsensorapi changes between 5.7.0 and 5.7.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.7.0...5.7.1)**
* Allow empty images field in api post


## [2.5.9](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.8...2.5.9) - 2021-05-21


#### Code dependencies
| Changes                 | From   | To     | Compare                                                                    |
|-------------------------|--------|--------|----------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.10.0 | 5.10.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.10.0...5.10.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.10.0 and 5.10.1](https://github.com/OpencontentCoop/ocsensor/compare/5.10.0...5.10.1)**
* Corregge un bug nel sistema di erogazione del token JWT


## [2.5.8](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.7...2.5.8) - 2021-05-20


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                     |
|-------------------------|---------|---------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.181.2 | 3.183.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.181.2...3.183.1)         |
| opencontent/ocsensor-ls | 5.9.2   | 5.10.0  | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.9.2...5.10.0)   |
| opencontent/ocsensorapi | 5.6.3   | 5.7.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.3...5.7.0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.9.2 and 5.10.0](https://github.com/OpencontentCoop/ocsensor/compare/5.9.2...5.10.0)**
* Permette l'accesso a immagini e file via api in base al valore di un header della richiesta e di configurare il contesto di ricezione delle immagini nelle api
* Introduce il grafico Tempi di lavorazione e il filtro per tipologia nei grafici

**[opencontent/ocsensorapi changes between 5.6.3 and 5.7.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.3...5.7.0)**
* Add stream_context to downloadImage if needed
* Add execution trend and type filter in stat
* Add user patch apis Add post types apis


## [2.5.7](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.6...2.5.7) - 2021-05-17


#### Code dependencies
| Changes                    | From    | To      | Compare                                                                       |
|----------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.180.2 | 3.181.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.180.2...3.181.2)           |
| opencontent/ocbootstrap-ls | 1.10.5  | 1.10.7  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.5...1.10.7) |
| opencontent/ocsiracauth-ls | 1.1     | 1.1.1   | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.1...1.1.1)     |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.5 and 1.10.7](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.5...1.10.7)**
* Protegge con CSRF token la chiamata post ajax di ocmultibinary
* Aggiorna la stringhe di traduzione
* Aggiorna la stringhe di traduzione

**[opencontent/ocsiracauth-ls changes between 1.1 and 1.1.1](https://github.com/OpencontentCoop/ocsiracauth/compare/1.1...1.1.1)**
* Introduce la compatibilità con openlogin-oauth


## [2.5.6](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.5...2.5.6) - 2021-05-05


#### Code dependencies
| Changes                         | From    | To        | Compare                                                                            |
|---------------------------------|---------|-----------|------------------------------------------------------------------------------------|
| aws/aws-sdk-php                 | 3.179.2 | 3.180.2   | [...](https://github.com/aws/aws-sdk-php/compare/3.179.2...3.180.2)                |
| composer/installers             | 207a91f | v1.0.25   | [...](https://github.com/composer/installers/compare/207a91f...v1.0.25)            |
| opencontent/ocopendata_forms-ls | 1.6.10  | 1.6.11    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.10...1.6.11) |
| opencontent/ocsensorapi         | 5.6.2   | 5.6.3     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.2...5.6.3)        |
| predis/predis                   | d72f067 | 1d7ccb6   | [...](https://github.com/predis/predis/compare/d72f067...1d7ccb6)                  |
| psr/container                   | 381524e | 1.1.x-dev | [...](https://github.com/php-fig/container/compare/381524e...1.1.x-dev)            |


Relevant changes by repository:

**[opencontent/ocopendata_forms-ls changes between 1.6.10 and 1.6.11](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.10...1.6.11)**
* Corregge il valore di default del campo BooleanField in accordo con i parametri dell'attributo di classe

**[opencontent/ocsensorapi changes between 5.6.2 and 5.6.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.2...5.6.3)**
* Splits the PostAging 7-30 bucket in 7-15 and 15-30
* Allow api post embed fields Refactor serializer


## [2.5.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.4...2.5.5) - 2021-05-04
- Update ezpublish to 2020.1000.6

#### Code dependencies
| Changes                    | From        | To          | Compare                                                                                  |
|----------------------------|-------------|-------------|------------------------------------------------------------------------------------------|
| ezsystems/ezpublish-legacy | 2020.1000.4 | 2020.1000.6 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.4...2020.1000.6) |
| psr/log                    | a18c1e6     | d49695b     | [...](https://github.com/php-fig/log/compare/a18c1e6...d49695b)                          |



## [2.5.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.3...2.5.4) - 2021-05-01
- Fix changelog links

#### Code dependencies
| Changes                   | From    | To      | Compare                                                                      |
|---------------------------|---------|---------|------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.179.1 | 3.179.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.179.1...3.179.2)          |
| opencontent/ocopendata-ls | 2.25.3  | 2.25.4  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.3...2.25.4) |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.25.3 and 2.25.4](https://github.com/OpencontentCoop/ocopendata/compare/2.25.3...2.25.4)**
* Force la rigenerazione delle cache delle classi per evitare un fatal error


## [2.5.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.2...2.5.3) - 2021-04-30
- Update php e nginx base images to 1.2.1

#### Code dependencies
| Changes                 | From  | To    | Compare                                                                     |
|-------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocsensorapi | 5.6.1 | 5.6.2 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.1...5.6.2) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.6.1 and 5.6.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.1...5.6.2)**
* Fix typo in trend stat


## [2.5.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.1...2.5.2) - 2021-04-30


#### Code dependencies
| Changes                   | From    | To      | Compare                                                                       |
|---------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.178.7 | 3.179.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.178.7...3.179.1)           |
| opencontent/ocsensor-ls   | 5.9.1   | 5.9.2   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.9.1...5.9.2)      |
| opencontent/ocsensorapi   | 5.6.0   | 5.6.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.0...5.6.1)   |
| symfony/polyfill-mbstring | 298b87c | 9ad2f3c | [...](https://github.com/symfony/polyfill-mbstring/compare/298b87c...9ad2f3c) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.9.1 and 5.9.2](https://github.com/OpencontentCoop/ocsensor/compare/5.9.1...5.9.2)**
* Introduce una configurazione ocsensor.ini per permettere la modifica del riferimento da gui
* Corregge l'export in csv di aree e categorie
* Visualizza un filtro per macrocategoria per le faq
* Introduce l'invio di una mail di benvenuto per gli utenti creati da operatore o via api

**[opencontent/ocsensorapi changes between 5.6.0 and 5.6.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.0...5.6.1)**
* Send OnAddApproverNotification also to users
* Fix stat trend title
* Fire on_generate_user event when user is created from api or operator Add WelcomeUserListener fired on on_generate_user event


## [2.5.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.0...2.5.1) - 2021-04-22
- Force nginx client_max_body_size


#### Installer
- Update version to 2.4.3

#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.9.0 | 5.9.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.9.0...5.9.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.9.0 and 5.9.1](https://github.com/OpencontentCoop/ocsensor/compare/5.9.0...5.9.1)**
* Corregge un bug per cui non era possibile esportare correttamente le segnalazioni in csv


## [2.5.0](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.8...2.5.0) - 2021-04-22



#### Installer
- Add faq class and container

#### Code dependencies
| Changes                          | From    | To      | Compare                                                                              |
|----------------------------------|---------|---------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.178.2 | 3.178.7 | [...](https://github.com/aws/aws-sdk-php/compare/3.178.2...3.178.7)                  |
| firebase/php-jwt                 |         | v5.2.1  |                                                                                      |
| opencontent/ocinstaller          | e701855 | 95b1463 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/e701855...95b1463)      |
| opencontent/ocopendata-ls        | 2.25.2  | 2.25.3  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.2...2.25.3)         |
| opencontent/ocsensor-ls          | 5.8.0   | 5.9.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.8.0...5.9.0)             |
| opencontent/ocsensorapi          | 5.5.1   | 5.6.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.1...5.6.0)          |
| symfony/polyfill-ctype           | c6c942b | 46cd957 | [...](https://github.com/symfony/polyfill-ctype/compare/c6c942b...46cd957)           |
| symfony/polyfill-intl-idn        | 3709eb8 | 780e280 | [...](https://github.com/symfony/polyfill-intl-idn/compare/3709eb8...780e280)        |
| symfony/polyfill-intl-normalizer | 43a0283 | 8590a5f | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/43a0283...8590a5f) |
| symfony/polyfill-mbstring        | 5232de9 | 298b87c | [...](https://github.com/symfony/polyfill-mbstring/compare/5232de9...298b87c)        |
| symfony/polyfill-php72           | cc6e6f9 | 95695b8 | [...](https://github.com/symfony/polyfill-php72/compare/cc6e6f9...95695b8)           |
| symfony/polyfill-php73           | a678b42 | fba8933 | [...](https://github.com/symfony/polyfill-php73/compare/a678b42...fba8933)           |
| symfony/polyfill-php80           | dc3063b | eca0bf4 | [...](https://github.com/symfony/polyfill-php80/compare/dc3063b...eca0bf4)           |


Relevant changes by repository:

**[opencontent/ocinstaller changes between e701855 and 95b1463](https://github.com/OpencontentCoop/ocinstaller/compare/e701855...95b1463)**
* Add load_policies param in role step (se false to avoid role regeneration)
* avoid warning in IOTools

**[opencontent/ocopendata-ls changes between 2.25.2 and 2.25.3](https://github.com/OpencontentCoop/ocopendata/compare/2.25.2...2.25.3)**
* Evita un possibile errore nel calcolo della paginazione a cursore

**[opencontent/ocsensor-ls changes between 5.8.0 and 5.9.0](https://github.com/OpencontentCoop/ocsensor/compare/5.8.0...5.9.0)**
* Introduce il metodo di autenticazione alle api con Bearer JWT
* Rimuove il bottone csv export dalle categorie e lo introduce negli automatismi Corregge alcuni bug minori
* Introduce un'interfaccia per la visualizzazione e la modifica delle faq
* Aggiunto il numero di risposte e di note nell'export csv delle segnalazioni
* Inserisce i claim di hasura nella generazione del token JWT
* Corregge una vulnerabilità di tipo user enumeration nel modulo di recupero password
* Introduce il selettore delle categorie nell'interfaccia di pubblicazione ajax Introduce le chiavi meta persistenti PersistentMetaKeys per impedire la sovrascrittura di campi meta in modifica della segnalazione
* Introduce la possibilità di modificare la tipologia di segnalazione
* Permette al browser di usare la cache per le chiamate alle immagini avatar Introduce gli avatar nei settings di gruppi e operatori
* Merge branch 'development' of https://github.com/OpencontentCoop/ocsensor into development
* Introduce il grafico del trend apertura/chiusura Introduce uno script di reindicizzazione dei soli sensor_post Corregge la lingua utilizzata da Highcharts
* Introduce uno script per modificare la proprietà delle segnalazioni da un utente a un altro

**[opencontent/ocsensorapi changes between 5.5.1 and 5.6.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.1...5.6.0)**
* Rename UnauthorizedException in ForbiddenException
* Add auth openapi doc if SensorJwtManager is enabled
* Minor bugfixes
* Add faq service
* Remove deprecated CategoryAutomaticAssignListener and all operator relations in category and area entities (using scenarios instead)
* Show operators in owner_groups stat filtered by group
* Add application/vnd.geo+json media type in api GET /posts and boost geojson post search
* Add api user/current GET and PUT
* Build api post doc based on the sensor_post class specification
* Add SetType action and CanSetType permission
* Add api category.parent Fix api search parameters
* Add first assignment and closing date parser in SolrMapper Add trend stat


## [2.4.8](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.7...2.4.8) - 2021-04-13



#### Installer
- Update installer version
- Remove stat access policies from roles
- Add sensor_group/tag field

#### Code dependencies
| Changes                      | From    | To      | Compare                                                                         |
|------------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.176.5 | 3.178.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.176.5...3.178.2)             |
| opencontent/ocbootstrap-ls   | 1.10.3  | 1.10.5  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.3...1.10.5)   |
| opencontent/ocinstaller      | 995b7bc | e701855 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/995b7bc...e701855) |
| opencontent/ocmultibinary-ls | 2.3.0   | 2.3.1   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.3.0...2.3.1)   |
| opencontent/ocsensor-ls      | 5.7.1   | 5.8.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.7.1...5.8.0)        |
| opencontent/ocsensorapi      | 5.5.0   | 5.5.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.0...5.5.1)     |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.3 and 1.10.5](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.3...1.10.5)**
* Aggiorna le traduzioni delle stringhe
* Protegge con CSRF token la chiamata post ajax di ocevents

**[opencontent/ocinstaller changes between 995b7bc and e701855](https://github.com/OpencontentCoop/ocinstaller/compare/995b7bc...e701855)**
* Add rename_tag step
* Fix rename_tag step

**[opencontent/ocmultibinary-ls changes between 2.3.0 and 2.3.1](https://github.com/OpencontentCoop/ocmultibinary/compare/2.3.0...2.3.1)**
* Add CSRF token in ajax post

**[opencontent/ocsensor-ls changes between 5.7.1 and 5.8.0](https://github.com/OpencontentCoop/ocsensor/compare/5.7.1...5.8.0)**
* Aggiunge il token csrf se configurato alle richieste post in ajax
* Introduce un'interfaccia di configurazione per abilitare l'accesso alle statistiche
* Introduce un nuovo campo tag per raggruppare i gruppi nelle statistiche Visualizza il filtro per macro categorie sui grafici di stato e di tipo per categoria

**[opencontent/ocsensorapi changes between 5.5.0 and 5.5.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.0...5.5.1)**
* Add main category filter
* Add tag in group and tag group filter in stat
* Fix category stat filtered by main category


## [2.4.7](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.6...2.4.7) - 2021-04-08


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.7.0 | 5.7.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.7.0...5.7.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.7.0 and 5.7.1](https://github.com/OpencontentCoop/ocsensor/compare/5.7.0...5.7.1)**
* Evita un errore in fase di registrazione se il codice fiscale non è obbligatorio


## [2.4.6](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.5...2.4.6) - 2021-04-01



#### Installer
- Add sensor_post_root/additional_map_layers Add sensor_scenario/expiry Fix sensor_scenario labels

#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.174.3 | 3.176.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.174.3...3.176.5)                    |
| opencontent/ocopendata-ls          | 2.25.1  | 2.25.2  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.1...2.25.2)           |
| opencontent/ocsensor-ls            | 5.6.3   | 5.7.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.3...5.7.0)               |
| opencontent/ocsensorapi            | 5.4.4   | 5.5.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.4...5.5.0)            |
| symfony/deprecation-contracts      | 49dc45a | 5f38c88 | [...](https://github.com/symfony/deprecation-contracts/compare/49dc45a...5f38c88)      |
| symfony/event-dispatcher-contracts | 9b7cabf | 69fee1a | [...](https://github.com/symfony/event-dispatcher-contracts/compare/9b7cabf...69fee1a) |
| zetacomponents/persistent-object   | 1.8     | 1.8.1   | [...](https://github.com/zetacomponents/PersistentObject/compare/1.8...1.8.1)          |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.25.1 and 2.25.2](https://github.com/OpencontentCoop/ocopendata/compare/2.25.1...2.25.2)**
* Permette di specificate le preferenze di lingua in TagRepository

**[opencontent/ocsensor-ls changes between 5.6.3 and 5.7.0](https://github.com/OpencontentCoop/ocsensor/compare/5.6.3...5.7.0)**
* Permette di configurare i layer di mappa addizionali direttamente da applicazione (attraverso l'attributo sensor_post_root/additional_map_layers)
* Introduce il grafico "Tipologia per categoria"
* Permette di configurare la scadenza della segnalazione negli automatismi

**[opencontent/ocsensorapi changes between 5.4.4 and 5.5.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.4...5.5.0)**
* Allow set post expiration in scenario
* Set scenario owner_group in category tree item group. Clear category tree cache on create/edit scenario
* Add type_categories stat


## [2.4.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.4...2.4.5) - 2021-03-19


#### Code dependencies
| Changes                      | From     | To      | Compare                                                                       |
|------------------------------|----------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.173.27 | 3.174.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.27...3.174.3)          |
| opencontent/ocsensor-ls      | 5.6.2    | 5.6.3   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.2...5.6.3)      |
| opencontent/ocsensorapi      | 5.4.3    | 5.4.4   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.3...5.4.4)   |
| opencontent/openpa_sensor-ls | 4.5.0    | 4.5.1   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.0...4.5.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.6.2 and 5.6.3](https://github.com/OpencontentCoop/ocsensor/compare/5.6.2...5.6.3)**
* Nasconde la registrazione utente in caso di login template attivato
* Corregge i parametri di richiesta del range di date nei grafici
* Introduce il grafico 'Aperte per gruppo di incaricati'
* Corregge le visualizzazioni dei grafici
* Introduce una visualizzazione della cronologia della segnalazione ad uso degli amministratori
* Evita di produrre un warning nel modulo sensor/posts
* Visualizza sempre l'ultimo operatore/gruppo di operatori incaricato
* Considera i permessi nell'esportazione in csv e aggiunge il campo risposta al csv.
* Corregge un bug sulla paginazione del download csv
* Considera il filtro per gruppo di incaricati nell'export in csv
* Introduce un permesso dedicato per accedere al modulo sensor/user

**[opencontent/ocsensorapi changes between 5.4.3 and 5.4.4](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.3...5.4.4)**
* Fix stat range filter Add OpenPerOwnerGroup as StatusPerOwnerGroup extension
* Fix stat range filter for field published
* Add audit on send notifications
* Remove debug code comment
* Add latestOwner and latestOwnerGroup post fields
* Allow empty query in search service

**[opencontent/openpa_sensor-ls changes between 4.5.0 and 4.5.1](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.0...4.5.1)**
* Corregge il controllo dei permessi per visualizzare il menu utenti


## [2.4.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.3...2.4.4) - 2021-03-11
- Hotfix remove debug settings



## [2.4.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.2...2.4.3) - 2021-03-11


#### Code dependencies
| Changes                            | From     | To       | Compare                                                                             |
|------------------------------------|----------|----------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.173.25 | 3.173.27 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.25...3.173.27)               |
| opencontent/ezpostgresqlcluster-ls | f37c194  | ef754ff  | [...](https://github.com/Opencontent/ezpostgresqlcluster/compare/f37c194...ef754ff) |
| opencontent/ocsearchtools-ls       | 1.11.0   | 1.11.1   | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.0...1.11.1)     |
| opencontent/ocsensor-ls            | 5.6.1    | 5.6.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.1...5.6.2)            |
| opencontent/ocsensorapi            | 5.4.1    | 5.4.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.1...5.4.3)         |


Relevant changes by repository:

**[opencontent/ezpostgresqlcluster-ls changes between f37c194 and ef754ff](https://github.com/Opencontent/ezpostgresqlcluster/compare/f37c194...ef754ff)**
* Add static method storeClusterizedFileMetadata for migrations

**[opencontent/ocsearchtools-ls changes between 1.11.0 and 1.11.1](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.0...1.11.1)**
* Evita un errore nella sincronizzazione dei gruppi di classe

**[opencontent/ocsensor-ls changes between 5.6.1 and 5.6.2](https://github.com/OpencontentCoop/ocsensor/compare/5.6.1...5.6.2)**
* Aggiunge il filtro per gruppo nelle statistiche e corregge alcuni errori sui filtri di ricerca

**[opencontent/ocsensorapi changes between 5.4.1 and 5.4.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.1...5.4.3)**
* Count StatusPerOwnerGroup per last_owner instead of history_owner
* Add owner group filter in stats


## [2.4.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.1...2.4.2) - 2021-03-10
- Update to traefik2 Add openlogin and spid test Remove varnish

#### Code dependencies
| Changes                    | From        | To          | Compare                                                                                  |
|----------------------------|-------------|-------------|------------------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.173.21    | 3.173.25    | [...](https://github.com/aws/aws-sdk-php/compare/3.173.21...3.173.25)                    |
| ezsystems/ezpublish-legacy | 2020.1000.3 | 2020.1000.4 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.3...2020.1000.4) |
| guzzlehttp/promises        | ddfeedf     | 8e7d04f     | [...](https://github.com/guzzle/promises/compare/ddfeedf...8e7d04f)                      |
| opencontent/ocinstaller    | ae76fef     | 995b7bc     | [...](https://github.com/OpencontentCoop/ocinstaller/compare/ae76fef...995b7bc)          |
| opencontent/ocsensor-ls    | 5.6.0       | 5.6.1       | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.0...5.6.1)                 |
| opencontent/ocsensorapi    | 5.4.0       | 5.4.1       | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.0...5.4.1)              |
| opencontent/ocsiracauth-ls | 1.0         | 1.1         | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.0...1.1)                  |
| opencontent/ocsupport-ls   | 54e52b0     | 8071c61     | [...](https://github.com/OpencontentCoop/ocsupport/compare/54e52b0...8071c61)            |


Relevant changes by repository:

**[opencontent/ocinstaller changes between ae76fef and 995b7bc](https://github.com/OpencontentCoop/ocinstaller/compare/ae76fef...995b7bc)**
* Allow deprecate topic without replacement
* Avoid error in DeprecateTopic installer
* Fix pagination in DepracateTopic
* Move node before remove location avoiding block inconsistency

**[opencontent/ocsensor-ls changes between 5.6.0 and 5.6.1](https://github.com/OpencontentCoop/ocsensor/compare/5.6.0...5.6.1)**
* Migliora l'interfaccia e il flusso di controllo della registrazione utenti
* Introduce uno script per rimuovere le registrazioni bloccate
* Aggiunge il filtro sullo stato della segnalazione nella pagina segnalazioni
* Aggiunge il totale dei risultati della ricerca nella pagina segnalazioni
* Introduce il grafico post aging
* Filtra le notifiche attivabili in base ai permessi dell'utente corrente
* Migliora la navigazione delle pagine di configurazione

**[opencontent/ocsensorapi changes between 5.4.0 and 5.4.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.0...5.4.1)**
* Fix auto assign timeline message
* Force DateTimeZone in DateTime constructors
* Change notification group to operator in OnAddCommentToModerateNotificationType and OnSendPrivateMessageNotificationType
* Add last_owner_user_id and last_owner_group_id fields in SolrMapper
* Add PostAging stat
* Force status privacy in PostInitializer if HidePrivacyChoice setting is enabled

**[opencontent/ocsiracauth-ls changes between 1.0 and 1.1](https://github.com/OpencontentCoop/ocsiracauth/compare/1.0...1.1)**
* Migliora la gestione degli utenti già presenti a sistema Introduce un handler dedicato a openlogin

**[opencontent/ocsupport-ls changes between 54e52b0 and 8071c61](https://github.com/OpencontentCoop/ocsupport/compare/54e52b0...8071c61)**
* Fix tag listing tool
* Bugifx in tag list


## [2.4.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.0...2.4.1) - 2021-03-04


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                         |
|---------------------------|----------|----------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.173.19 | 3.173.21 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.19...3.173.21)           |
| opencontent/ocinstaller   | 306ea1c  | ae76fef  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/306ea1c...ae76fef) |
| opencontent/ocopendata-ls | 2.25.0   | 2.25.1   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.0...2.25.1)    |
| opencontent/ocsensor-ls   | 5.5.0    | 5.6.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.5.0...5.6.0)        |
| opencontent/ocsensorapi   | 5.3.0    | 5.4.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.3.0...5.4.0)     |
| php-http/guzzle6-adapter  | 6f108cf  | 9d1a45e  | [...](https://github.com/php-http/guzzle6-adapter/compare/6f108cf...9d1a45e)    |
| psr/log                   | dd738d0  | a18c1e6  | [...](https://github.com/php-fig/log/compare/dd738d0...a18c1e6)                 |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 306ea1c and ae76fef](https://github.com/OpencontentCoop/ocinstaller/compare/306ea1c...ae76fef)**
* Add deprecate_topic installer step

**[opencontent/ocopendata-ls changes between 2.25.0 and 2.25.1](https://github.com/OpencontentCoop/ocopendata/compare/2.25.0...2.25.1)**
* Corregge un bug nella validazione api delle relazioni

**[opencontent/ocsensor-ls changes between 5.5.0 and 5.6.0](https://github.com/OpencontentCoop/ocsensor/compare/5.5.0...5.6.0)**
* Introduce il grafico di Pareto nel grafico Stato per categoria Aggiorna la libreria highchart
* Introduce il grafico di stato per gruppo di incaricati
* Migliora le performance di caricamento
* Corregge un bug per cui non veniva inviata la notifica alla creazione di una segnalazione
* Introduce la barra di ricerca avanzata per gli operatori sulla pagina segnalazioni
* Aggiunge uno script cli per rimuovere gli osservatori da una segnalazione

**[opencontent/ocsensorapi changes between 5.3.0 and 5.4.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.3.0...5.4.0)**
* Reorder data in StatusPerCategory stat
* Add StatusPerOwnerGroup stat
* Boost repository and search service performance
* Fix SendMailListener on on_create event
* Add owner_group_id_list in SolrMapper


## [2.4.0](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.5...2.4.0) - 2021-03-02
- Update php and nginx docker base image (adding healtcheck)
- Fix minio createbucket command
- Fix AssignRole in browse.ini
- Add changelog
- Updated list of municipalities, latest release date and number and links to external resources using short-urls
- Change base image with php7.2.33-clean
- Fix category access in sensor anonymous role

#### Code dependencies
| Changes                            | From    | To       | Compare                                                                                |
|------------------------------------|---------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.173.7 | 3.173.19 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.7...3.173.19)                   |
| opencontent/ocopendata_forms-ls    | 1.6.9   | 1.6.10   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.9...1.6.10)      |
| opencontent/ocsensor-ls            | 5.4.2   | 5.5.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.4.2...5.5.0)               |
| opencontent/ocsensorapi            | 5.2.1   | 5.3.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.1...5.3.0)            |
| opencontent/ocsupport-ls           | 2ee7846 | 54e52b0  | [...](https://github.com/OpencontentCoop/ocsupport/compare/2ee7846...54e52b0)          |
| opencontent/ocwebhookserver-ls     | 67a56b7 | a60d571  | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/67a56b7...a60d571)    |
| opencontent/openpa_sensor-ls       | 4.4.0   | 4.5.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.4.0...4.5.0)          |
| symfony/deprecation-contracts      | d940483 | 49dc45a  | [...](https://github.com/symfony/deprecation-contracts/compare/d940483...49dc45a)      |
| symfony/event-dispatcher-contracts | 5e8ae4d | 9b7cabf  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5e8ae4d...9b7cabf) |
| symfony/polyfill-intl-idn          | 0eb8293 | 3709eb8  | [...](https://github.com/symfony/polyfill-intl-idn/compare/0eb8293...3709eb8)          |
| symfony/polyfill-intl-normalizer   | 6e971c8 | 43a0283  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/6e971c8...43a0283)   |
| symfony/polyfill-mbstring          | f377a3d | 5232de9  | [...](https://github.com/symfony/polyfill-mbstring/compare/f377a3d...5232de9)          |


Relevant changes by repository:

**[opencontent/ocopendata_forms-ls changes between 1.6.9 and 1.6.10](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.9...1.6.10)**
* Corregge alcuni bug minori
* Corregge un bug nella ricerca del punto sulla mappa nel contesto di un form dinamico

**[opencontent/ocsensor-ls changes between 5.4.2 and 5.5.0](https://github.com/OpencontentCoop/ocsensor/compare/5.4.2...5.5.0)**
* Introduce gli scenari (automatismi) per organizzare in maniera organica e flessibile le assegnazioni automatiche
* Invia una mail di benvenuto al nuovo operatore inserito
* Corregge il testo della mail di benvenuto all'operatore
* Corregge alcuni bug minori
* Aggiunge un connettore custom per gli scenari
* Visualizza i messaggi di audit
* Corregge un bug del custom payload serializer
* Utilizza i filtri impostati nell'interfaccia delle statistiche anche nell'esportazione in csv

**[opencontent/ocsensorapi changes between 5.2.1 and 5.3.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.1...5.3.0)**
* Add WelcomeOperatorListener on event on_new_operator
* Introducing ScenarioService
* Add scenario criterion description Fix scenario search limit
* Always trigger on_add_category event
* Add audit messages
* Hide operator name in public comment if needed
* Remove author from on_add_comment_to_moderate notification
* Fix stat titles

**[opencontent/ocsupport-ls changes between 2ee7846 and 54e52b0](https://github.com/OpencontentCoop/ocsupport/compare/2ee7846...54e52b0)**
* Add changelog tools
* changelog bugfixes

**[opencontent/ocwebhookserver-ls changes between 67a56b7 and a60d571](https://github.com/OpencontentCoop/ocwebhookserver/compare/67a56b7...a60d571)**
* Fix custom payload check in webhook edit

**[opencontent/openpa_sensor-ls changes between 4.4.0 and 4.5.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.4.0...4.5.0)**
* Aggiunto evento in post publish alla creazione di un nuovo operatore


## [2.3.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.4...2.3.5) - 2021-02-12


#### Code dependencies
| Changes                           | From     | To       | Compare                                                                                |
|-----------------------------------|----------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                   | 3.173.3  | 3.173.7  | [...](https://github.com/aws/aws-sdk-php/compare/3.173.3...3.173.7)                    |
| ezsystems/ezmultiupload-ls        | v5.3.1.2 | v5.3.1.3 | [...](https://github.com/ezsystems/ezmultiupload/compare/v5.3.1.2...v5.3.1.3)          |
| opencontent/ocmap-ls              | 93c8432  | 9bc1f0a  | [...](https://github.com/OpencontentCoop/ocmap/compare/93c8432...9bc1f0a)              |
| opencontent/ocsearchtools-ls      | 1.10.9   | 1.11.0   | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.9...1.11.0)        |
| opencontent/ocsensor-ls           | 5.4.1    | 5.4.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.4.1...5.4.2)               |
| opencontent/ocsensorapi           | 5.2.0    | 5.2.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.0...5.2.1)            |
| opencontent/openpa-ls             | 4f20fd8  | c68a756  | [...](https://github.com/OpencontentCoop/openpa/compare/4f20fd8...c68a756)             |
| opencontent/openpa_userprofile-ls | 4c7a67e  | 3998da2  | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/4c7a67e...3998da2) |
| psr/event-dispatcher              | 3ef040d  | aa4f89e  | [...](https://github.com/php-fig/event-dispatcher/compare/3ef040d...aa4f89e)           |


Relevant changes by repository:

**[opencontent/ocmap-ls changes between 93c8432 and 9bc1f0a](https://github.com/OpencontentCoop/ocmap/compare/93c8432...9bc1f0a)**
* Corregge un bug che emetteva un warning in caso di attributo vuoto

**[opencontent/ocsearchtools-ls changes between 1.10.9 and 1.11.0](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.9...1.11.0)**
* Rimuove di default le cache calendartaxonomy e calendarquery che vanno attivate qualora di utilizzino le funzionalità

**[opencontent/ocsensor-ls changes between 5.4.1 and 5.4.2](https://github.com/OpencontentCoop/ocsensor/compare/5.4.1...5.4.2)**
* Aggiunge il grafico di stato per categoria
* Aggiunge un grafico a torta su categorie e zone
* Corregge la logica di inbox per l'operatore e il permesso di accesso al modulo user
* Nasconde il nome degli operatori nelle notifiche in caso di flag HideOperatorNames attivato

**[opencontent/ocsensorapi changes between 5.2.0 and 5.2.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.0...5.2.1)**
* Add StatusPerCategory statistic
* Sort Users stat data
* Fix StatusPerCategory stat

**[opencontent/openpa-ls changes between 4f20fd8 and c68a756](https://github.com/OpencontentCoop/openpa/compare/4f20fd8...c68a756)**
* Minor bugfix

**[opencontent/openpa_userprofile-ls changes between 4c7a67e and 3998da2](https://github.com/OpencontentCoop/openpa_userprofile/compare/4c7a67e...3998da2)**
* Corregge una vulnerabilità XSS


## [2.3.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.3...2.3.4) - 2021-02-05


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.4.0 | 5.4.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.4.0...5.4.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.4.0 and 5.4.1](https://github.com/OpencontentCoop/ocsensor/compare/5.4.0...5.4.1)**
* Corregge un bug per cui nell'inbox non venivano correttamente visualizzati i titoli dei widget


## [2.3.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.2...2.3.3) - 2021-02-05


#### Code dependencies
| Changes                        | From        | To          | Compare                                                                                  |
|--------------------------------|-------------|-------------|------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.172.2     | 3.173.3     | [...](https://github.com/aws/aws-sdk-php/compare/3.172.2...3.173.3)                      |
| ezsystems/ezpublish-legacy     | 2020.1000.1 | 2020.1000.3 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.1...2020.1000.3) |
| opencontent/occodicefiscale-ls | 2d040a8     | 24253b0     | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/2d040a8...24253b0)      |
| opencontent/ocrecaptcha-ls     | 1.4         | 1.5         | [...](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.4...1.5)                  |
| opencontent/ocsensor-ls        | 5.3.0       | 5.4.0       | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.3.0...5.4.0)                 |
| opencontent/ocsensorapi        | 5.1.0       | 5.2.0       | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.1.0...5.2.0)              |
| opencontent/ocwebhookserver-ls | aabd35a     | 67a56b7     | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/aabd35a...67a56b7)      |
| opencontent/openpa_sensor-ls   | 4.3.0       | 4.4.0       | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.3.0...4.4.0)            |
| php-http/message               | c6f875f     | 1.11.0      | [...](https://github.com/php-http/message/compare/c6f875f...1.11.0)                      |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between 2d040a8 and 24253b0](https://github.com/OpencontentCoop/occodicefiscale/compare/2d040a8...24253b0)**
* Corregge un bug nel connettore di ocdescriptionboolean
* Aggiunge la validazione del codice fiscale nel connettore opendata

**[opencontent/ocrecaptcha-ls changes between 1.4 and 1.5](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.4...1.5)**
* Verifica la presenza del valore post in validateObjectAttributeHTTPInput
* Verifica la presenza del valore post in validateCollectionAttributeHTTPInput
* Disabilita il connettore recaptcha se l'utente corrente è autenticato

**[opencontent/ocsensor-ls changes between 5.3.0 and 5.4.0](https://github.com/OpencontentCoop/ocsensor/compare/5.3.0...5.4.0)**
* Corregge un problema dell'esportatore dei csv e un errore del form di inserimento segnalazione
* Corregge alcuni bug minori sulle api e sui template
* Abilita l'interfaccia di rifiuto di un commento da moderare
* Introduce uno script per modificare massivamente la privacy delle segnalazioni
* Merge branch 'development' into reject-comment
* Corregge il caricamento dei font
* Corregge un bug nel connettore semplificato dello user account
* Introduce il modulo sensor/user ad uso degli operatori
* Impone l'utente anonimo come default in caso di segnalazione per conto di
* Corregge la visualizzazione dei controlli su mappa

**[opencontent/ocsensorapi changes between 5.1.0 and 5.2.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.1.0...5.2.0)**
* Add rejected comment flag
* Api bugfix
* Add author email in api schema
* Remove group as default target in observer notification
* Add user/phone field
* Refactor assign per category

**[opencontent/ocwebhookserver-ls changes between aabd35a and 67a56b7](https://github.com/OpencontentCoop/ocwebhookserver/compare/aabd35a...67a56b7)**
* Allow payload customizations via trigger configuration

**[opencontent/openpa_sensor-ls changes between 4.3.0 and 4.4.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.3.0...4.4.0)**
* Visualizza il menu UTENTI se abilitato


## [2.3.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.1...2.3.2) - 2021-01-28


#### Code dependencies
| Changes                          | From     | To      | Compare                                                                             |
|----------------------------------|----------|---------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.20 | 3.172.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.20...3.172.2)                |
| opencontent/ocbootstrap-ls       | 1.10.2   | 1.10.3  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.2...1.10.3)       |
| opencontent/ocsensor-ls          | 5.2.0    | 5.3.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.2.0...5.3.0)            |
| opencontent/ocsensorapi          | 5.0.3    | 5.1.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.3...5.1.0)         |
| opencontent/openpa-ls            | c036b88  | 4f20fd8 | [...](https://github.com/OpencontentCoop/openpa/compare/c036b88...4f20fd8)          |
| opencontent/openpa_theme_2014-ls | 2.15.3   | 2.15.4  | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.3...2.15.4) |
| php-http/message                 | d23ac28  | c6f875f | [...](https://github.com/php-http/message/compare/d23ac28...c6f875f)                |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.2 and 1.10.3](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.2...1.10.3)**
* Corregge una vulnerabilità XSS

**[opencontent/ocsensor-ls changes between 5.2.0 and 5.3.0](https://github.com/OpencontentCoop/ocsensor/compare/5.2.0...5.3.0)**
* Aggiunge un input di ricerca alla dashboard dedicata agli utenti
* Introduce la selezione di un range di date nei grafici
* Consente di visualizzare ulteriori layer WMS nelle mappe
* Corregge un bug nel sistema di esportazione in csv
* Corregge un problema per cui al clone di una segnalazione il canale veniva preselezionato al primo valore
* Rimuove la configurazione AdditionalMapLayers di test
* Rimuove il range di tempo per il filtro mensile
* Introduce il selettore dei layer nelle mappe

**[opencontent/ocsensorapi changes between 5.0.3 and 5.1.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.3...5.1.0)**
* Add stat range filter

**[opencontent/openpa-ls changes between c036b88 and 4f20fd8](https://github.com/OpencontentCoop/openpa/compare/c036b88...4f20fd8)**
* Add redis cluster connector verbose logs

**[opencontent/openpa_theme_2014-ls changes between 2.15.3 and 2.15.4](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.3...2.15.4)**
* Corregge una vulnerabilità XSS


## [2.3.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.27...2.3.1) - 2021-01-26
- Add optional socketio redis adapter

#### Code dependencies
| Changes                      | From     | To       | Compare                                                                       |
|------------------------------|----------|----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.171.19 | 3.171.20 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.19...3.171.20)         |
| opencontent/ocsensor-ls      | 5.1.1    | 5.2.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.1.1...5.2.0)      |
| opencontent/ocsensorapi      | 5.0.2    | 5.0.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)   |
| opencontent/openpa_sensor-ls | 4.2.5    | 4.3.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.5...4.3.0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.1.1 and 5.2.0](https://github.com/OpencontentCoop/ocsensor/compare/5.1.1...5.2.0)**
* Introduce un’interfaccia responsive di gestione delle segnalazioni ad uso dell’operatore attivabile in homepage o in menu. Richiede la configurazione di un server socket dedicato

**[opencontent/ocsensorapi changes between 5.0.2 and 5.0.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)**
* Fix some SolrMapper bug

**[opencontent/openpa_sensor-ls changes between 4.2.5 and 4.3.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.5...4.3.0)**
* Visualizza il menu INBOX se abilitato


## [2.2.27](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.0...2.2.27) - 2021-01-19


#### Code dependencies
| Changes                      | From     | To       | Compare                                                                       |
|------------------------------|----------|----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.171.20 | 3.171.19 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.20...3.171.19)         |
| opencontent/ocsensor-ls      | 5.2.0    | 5.1.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.2.0...5.1.1)      |
| opencontent/ocsensorapi      | 5.0.3    | 5.0.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.3...5.0.2)   |
| opencontent/openpa_sensor-ls | 4.3.0    | 4.2.5    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.3.0...4.2.5) |



## [2.3.0](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.26...2.3.0) - 2021-01-18


#### Code dependencies
| Changes                          | From     | To       | Compare                                                                             |
|----------------------------------|----------|----------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.17 | 3.171.20 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.17...3.171.20)               |
| opencontent/ocfoshttpcache-ls    | faa918c  | 5ab0d91  | [...](https://github.com/OpencontentCoop/ocfoshttpcache/compare/faa918c...5ab0d91)  |
| opencontent/ocinstaller          | 7a4ec2b  | 306ea1c  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/7a4ec2b...306ea1c)     |
| opencontent/ocsensor-ls          | 5.1.0    | 5.2.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.1.0...5.2.0)            |
| opencontent/ocsensorapi          | 5.0.2    | 5.0.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)         |
| opencontent/openpa_sensor-ls     | 4.2.4    | 4.3.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.4...4.3.0)       |
| opencontent/openpa_theme_2014-ls | 2.15.2   | 2.15.3   | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.2...2.15.3) |


Relevant changes by repository:

**[opencontent/ocfoshttpcache-ls changes between faa918c and 5ab0d91](https://github.com/OpencontentCoop/ocfoshttpcache/compare/faa918c...5ab0d91)**
* Minor bugfix

**[opencontent/ocinstaller changes between 7a4ec2b and 306ea1c](https://github.com/OpencontentCoop/ocinstaller/compare/7a4ec2b...306ea1c)**
* Minor fixes

**[opencontent/ocsensor-ls changes between 5.1.0 and 5.2.0](https://github.com/OpencontentCoop/ocsensor/compare/5.1.0...5.2.0)**
* Corregge un bug nella visualizzazione della mappa nella segnalazione
* Introduce un’interfaccia responsive di gestione delle segnalazioni ad uso dell’operatore attivabile in homepage o in menu. Richiede la configurazione di un server socket dedicato

**[opencontent/ocsensorapi changes between 5.0.2 and 5.0.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)**
* Fix some SolrMapper bug

**[opencontent/openpa_sensor-ls changes between 4.2.4 and 4.3.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.4...4.3.0)**
* Rifattorizza alcuni template di default
* Visualizza il menu INBOX se abilitato

**[opencontent/openpa_theme_2014-ls changes between 2.15.2 and 2.15.3](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.2...2.15.3)**
* Aggiunge codice fiscale, partita iva e codice sdi in footer


## [2.2.26](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.25...2.2.26) - 2021-01-14


#### Code dependencies
| Changes                 | From     | To       | Compare                                                                     |
|-------------------------|----------|----------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.16 | 3.171.17 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.16...3.171.17)       |
| opencontent/ocsensor-ls | 5.0.0    | 5.1.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.0.0...5.1.0)    |
| opencontent/ocsensorapi | 5.0.1    | 5.0.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.1...5.0.2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.0.0 and 5.1.0](https://github.com/OpencontentCoop/ocsensor/compare/5.0.0...5.1.0)**
* Introduce la possibilità di impostare un geoserver come geocoder
* Espone l’interfaccia di caricamento ajax anche all’operatore

**[opencontent/ocsensorapi changes between 5.0.1 and 5.0.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.1...5.0.2)**
* Fix behalf creation in api controller
* Fix hidden-operator-name view data
* Fix hidden-operator-name observers view data


## [2.2.25](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.24...2.2.25) - 2021-01-12


#### Code dependencies
| Changes                 | From     | To       | Compare                                                                     |
|-------------------------|----------|----------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.15 | 3.171.16 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.15...3.171.16)       |
| opencontent/ocsensorapi | 5.0.0    | 5.0.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.0...5.0.1) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.0.0 and 5.0.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.0...5.0.1)**
* Fix group access in participant service


## [2.2.24](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.23...2.2.24) - 2021-01-12


#### Code dependencies
| Changes                          | From     | To       | Compare                                                                              |
|----------------------------------|----------|----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.12 | 3.171.15 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.12...3.171.15)                |
| opencontent/ocembed-ls           | 1.4.1    | 1.4.2    | [...](https://github.com/OpencontentCoop/ocembed/compare/1.4.1...1.4.2)              |
| opencontent/ocopendata_forms-ls  | 1.6.8    | 1.6.9    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.8...1.6.9)     |
| opencontent/ocsensor-ls          | cac54ad  | 5.0.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/cac54ad...5.0.0)           |
| opencontent/ocsensorapi          | de2dbc2  | 5.0.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/de2dbc2...5.0.0)        |
| symfony/polyfill-ctype           | 7130f34  | c6c942b  | [...](https://github.com/symfony/polyfill-ctype/compare/7130f34...c6c942b)           |
| symfony/polyfill-intl-idn        | 947d45e  | 0eb8293  | [...](https://github.com/symfony/polyfill-intl-idn/compare/947d45e...0eb8293)        |
| symfony/polyfill-intl-normalizer | 3a79a22  | 6e971c8  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/3a79a22...6e971c8) |
| symfony/polyfill-mbstring        | de14691  | f377a3d  | [...](https://github.com/symfony/polyfill-mbstring/compare/de14691...f377a3d)        |
| symfony/polyfill-php72           | 4a4465f  | cc6e6f9  | [...](https://github.com/symfony/polyfill-php72/compare/4a4465f...cc6e6f9)           |
| symfony/polyfill-php73           | 8c0d39c  | a678b42  | [...](https://github.com/symfony/polyfill-php73/compare/8c0d39c...a678b42)           |
| symfony/polyfill-php80           | 54cc82c  | dc3063b  | [...](https://github.com/symfony/polyfill-php80/compare/54cc82c...dc3063b)           |


Relevant changes by repository:

**[opencontent/ocembed-ls changes between 1.4.1 and 1.4.2](https://github.com/OpencontentCoop/ocembed/compare/1.4.1...1.4.2)**
* Evita la richiesta justcheckurl per workaround a problema youtube

**[opencontent/ocopendata_forms-ls changes between 1.6.8 and 1.6.9](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.8...1.6.9)**
* Impedisce l'invio del form alla pressione del tasto invio solo nell'ambito di alpaca-form

**[opencontent/ocsensorapi changes between de2dbc2 and 5.0.0](https://github.com/OpencontentCoop/ocsensorapi/compare/de2dbc2...5.0.0)**
* hotfix reporter as observer


## [2.2.23](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.22-genova...2.2.23) - 2021-01-05


#### Code dependencies
| Changes                          | From    | To       | Compare                                                                              |
|----------------------------------|---------|----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.3 | 3.171.12 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.3...3.171.12)                 |
| opencontent/occodicefiscale-ls   | daac5a8 | 2d040a8  | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/daac5a8...2d040a8)  |
| opencontent/ocopendata-ls        | 2.24.7  | 2.25.0   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.7...2.25.0)         |
| opencontent/ocsensor-ls          | 9dbbdb3 | cac54ad  | [...](https://github.com/OpencontentCoop/ocsensor/compare/9dbbdb3...cac54ad)         |
| opencontent/ocsensorapi          | aa88b99 | de2dbc2  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/aa88b99...de2dbc2)      |
| opencontent/openpa-ls            | 90cedd4 | c036b88  | [...](https://github.com/OpencontentCoop/openpa/compare/90cedd4...c036b88)           |
| symfony/polyfill-ctype           | fade6de | 7130f34  | [...](https://github.com/symfony/polyfill-ctype/compare/fade6de...7130f34)           |
| symfony/polyfill-intl-idn        | 4c489fd | 947d45e  | [...](https://github.com/symfony/polyfill-intl-idn/compare/4c489fd...947d45e)        |
| symfony/polyfill-intl-normalizer | 69609f9 | 3a79a22  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/69609f9...3a79a22) |
| symfony/polyfill-mbstring        | 401c9d9 | de14691  | [...](https://github.com/symfony/polyfill-mbstring/compare/401c9d9...de14691)        |
| symfony/polyfill-php80           | 3a11f3d | 54cc82c  | [...](https://github.com/symfony/polyfill-php80/compare/3a11f3d...54cc82c)           |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between daac5a8 and 2d040a8](https://github.com/OpencontentCoop/occodicefiscale/compare/daac5a8...2d040a8)**
* Espone i datatype in ocopendata_forms

**[opencontent/ocopendata-ls changes between 2.24.7 and 2.25.0](https://github.com/OpencontentCoop/ocopendata/compare/2.24.7...2.25.0)**
* Permette di bypassare le policy di creazione/aggiornamento in api content repository

**[opencontent/ocsensor-ls changes between 9dbbdb3 and cac54ad](https://github.com/OpencontentCoop/ocsensor/compare/9dbbdb3...cac54ad)**
* Aggiunge la statistica delle adesioni
* Corregge un errore nel template dei messaggi
* Rende dinamico il form contestuale di creazione utente in caso di segnalazione "per conto di"
* Introduce le icone dedicate ai canali (modalità di segnalazione) Impone l'inserimento di una nota privata prima di impostare la segnalazione come "intervento terminato" Permette la chiusura della segnalazione contestualmente all'inserimento di una risposta ufficiale Corregge alcuni bug di visualizzazione Permette la ricerca per id Indica nella dashboard la presenza di commenti in attesa di moderazione
* Corregge la select del tipo di segnalazione nell'interfaccia di creazione ajax

**[opencontent/ocsensorapi changes between aa88b99 and de2dbc2](https://github.com/OpencontentCoop/ocsensorapi/compare/aa88b99...de2dbc2)**
* Add user stat
* Allow user search to user with behalfOfMode active Fix api pagination parameters
* Add ChannelService
* Fix openapi schema
* Set reporter as observer by default
* Add commentsToModerate method
* Add getParticipantNameListByType method (used in csv exporter)

**[opencontent/openpa-ls changes between 90cedd4 and c036b88](https://github.com/OpencontentCoop/openpa/compare/90cedd4...c036b88)**
* Aggiunge il protocollo (https per default) in aws dfs public handler


## [2.2.22-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.21-genova...2.2.22-genova) - 2020-12-21


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.1 | 3.171.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.1...3.171.3)             |
| opencontent/ocsensorapi | d301c78 | aa88b99 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/d301c78...aa88b99) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between d301c78 and aa88b99](https://github.com/OpencontentCoop/ocsensorapi/compare/d301c78...aa88b99)**
* Avoid solr exceptions in multiple owners


## [2.2.21-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.20-genova...2.2.21-genova) - 2020-12-21




## [2.2.20-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.19-genova...2.2.20-genova) - 2020-12-18


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 457cec2 | 9dbbdb3 | [...](https://github.com/OpencontentCoop/ocsensor/compare/457cec2...9dbbdb3)    |
| opencontent/ocsensorapi | 3298301 | d301c78 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/3298301...d301c78) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 457cec2 and 9dbbdb3](https://github.com/OpencontentCoop/ocsensor/compare/457cec2...9dbbdb3)**
* Corregge i percorsi dei file di cache per compatibilità con il cluster in redis

**[opencontent/ocsensorapi changes between 3298301 and d301c78](https://github.com/OpencontentCoop/ocsensorapi/compare/3298301...d301c78)**
* Hotfix treenode cache file path


## [2.2.19-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.18-genova...2.2.19-genova) - 2020-12-18


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.0 | 3.171.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.0...3.171.1)             |
| opencontent/ocsensorapi | 93627a0 | 3298301 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/93627a0...3298301) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 93627a0 and 3298301](https://github.com/OpencontentCoop/ocsensorapi/compare/93627a0...3298301)**
* Hotfix treenode cache file path


## [2.2.18-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.17-genova...2.2.18-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 4f22e79 | 457cec2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4f22e79...457cec2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4f22e79 and 457cec2](https://github.com/OpencontentCoop/ocsensor/compare/4f22e79...457cec2)**
* Evita di emettere un log notice in api controller
* Migliora il messaggio di aiuto nel form di inserimento commento per un operatore


## [2.2.17-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.16-genova...2.2.17-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 7a15dc3 | 4f22e79 | [...](https://github.com/OpencontentCoop/ocsensor/compare/7a15dc3...4f22e79)    |
| opencontent/ocsensorapi | 36aab74 | 93627a0 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/36aab74...93627a0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7a15dc3 and 4f22e79](https://github.com/OpencontentCoop/ocsensor/compare/7a15dc3...4f22e79)**
* Corregge i percorsi dei file di cache per compatibilità con il cluster in redis

**[opencontent/ocsensorapi changes between 36aab74 and 93627a0](https://github.com/OpencontentCoop/ocsensorapi/compare/36aab74...93627a0)**
* Fix all sensor cache paths (compat with redis cluster handler)
* Avoid xss


## [2.2.16-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.15-debug...2.2.16-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensorapi | 2d276ac | 36aab74 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/2d276ac...36aab74) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 2d276ac and 36aab74](https://github.com/OpencontentCoop/ocsensorapi/compare/2d276ac...36aab74)**
* Fix treenode cache file path


## [2.2.15-debug](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.15-genova...2.2.15-debug) - 2020-12-17
- Output debug in install script



## [2.2.15-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.14-genova...2.2.15-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.170.0 | 3.171.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.170.0...3.171.0)             |
| opencontent/ocsensor-ls | 4176da2 | 7a15dc3 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4176da2...7a15dc3)    |
| opencontent/ocsensorapi | 05a1611 | 2d276ac | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/05a1611...2d276ac) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4176da2 and 7a15dc3](https://github.com/OpencontentCoop/ocsensor/compare/4176da2...7a15dc3)**
* Migliora l'importazione delle mappe da openstreetmap
* Migliora la visualizzazione degli input form di inserimento

**[opencontent/ocsensorapi changes between 05a1611 and 2d276ac](https://github.com/OpencontentCoop/ocsensorapi/compare/05a1611...2d276ac)**
* Refactorize authorfiscalcode stat filter creating dedicated endpoint


## [2.2.14-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.13-genova...2.2.14-genova) - 2020-12-16


#### Code dependencies
| Changes                      | From    | To      | Compare                                                                         |
|------------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.166.0 | 3.170.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.166.0...3.170.0)             |
| opencontent/ocmultibinary-ls | 2.2.5   | 2.3.0   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.5...2.3.0)   |
| opencontent/ocsensor-ls      | dda7c28 | 4176da2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/dda7c28...4176da2)    |
| opencontent/ocsensorapi      | e79fb0e | 05a1611 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/e79fb0e...05a1611) |
| opencontent/openpa-ls        | a84de51 | 90cedd4 | [...](https://github.com/OpencontentCoop/openpa/compare/a84de51...90cedd4)      |
| php-http/message             | 6e2c574 | d23ac28 | [...](https://github.com/php-http/message/compare/6e2c574...d23ac28)            |


Relevant changes by repository:

**[opencontent/ocmultibinary-ls changes between 2.2.5 and 2.3.0](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.5...2.3.0)**
* Allow to clear all files in fromString method

**[opencontent/ocsensor-ls changes between dda7c28 and 4176da2](https://github.com/OpencontentCoop/ocsensor/compare/dda7c28...4176da2)**
* Migliora il motore di ricerca delle segnalazioni e lo rende interrogabile dalla pagine delle statistiche
* Consente di offuscare il nome degli operatori
* Abilita AnnounceKit

**[opencontent/ocsensorapi changes between e79fb0e and 05a1611](https://github.com/OpencontentCoop/ocsensorapi/compare/e79fb0e...05a1611)**
* Fix PerCategory stat
* Add author fiscal code api filter, add count in search result, bugfix in schema builder
* Does not send AddComment notification to observer group
* Hide operator names if needed by HideOperatorNames setting

**[opencontent/openpa-ls changes between a84de51 and 90cedd4](https://github.com/OpencontentCoop/openpa/compare/a84de51...90cedd4)**
* Permette l'inserimento del codice Web Analytics Italia


## [2.2.13-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.12-genova...2.2.13-genova) - 2020-12-03


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | e1cbc92 | dda7c28 | [...](https://github.com/OpencontentCoop/ocsensor/compare/e1cbc92...dda7c28) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between e1cbc92 and dda7c28](https://github.com/OpencontentCoop/ocsensor/compare/e1cbc92...dda7c28)**
* Corregge un bug nella selezione delle aree nella nuova interfaccia di inserimento


## [2.2.12-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.11-genova...2.2.12-genova) - 2020-12-03


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 84afb0f | e1cbc92 | [...](https://github.com/OpencontentCoop/ocsensor/compare/84afb0f...e1cbc92) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 84afb0f and e1cbc92](https://github.com/OpencontentCoop/ocsensor/compare/84afb0f...e1cbc92)**
* Corregge un bug sulla selezione della privacy nella nuova interfaccia di inserimento


## [2.2.11-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.10-genova...2.2.11-genova) - 2020-12-03


#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.165.0 | 3.166.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.165.0...3.166.0)               |
| friendsofsymfony/http-cache | 42c96a0 | fa9abf6 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/42c96a0...fa9abf6) |
| opencontent/ocsensor-ls     | 26f737a | 84afb0f | [...](https://github.com/OpencontentCoop/ocsensor/compare/26f737a...84afb0f)      |
| php-http/message            | 39db36d | 6e2c574 | [...](https://github.com/php-http/message/compare/39db36d...6e2c574)              |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 26f737a and 84afb0f](https://github.com/OpencontentCoop/ocsensor/compare/26f737a...84afb0f)**
* Corregge alcuni problemi di visualizzazione della nuova interfaccia di inserimento


## [2.2.10-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.9-genova...2.2.10-genova) - 2020-12-01


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.164.1 | 3.165.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.164.1...3.165.0)          |
| opencontent/ocsensor-ls | 3e928a1 | 26f737a | [...](https://github.com/OpencontentCoop/ocsensor/compare/3e928a1...26f737a) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 3e928a1 and 26f737a](https://github.com/OpencontentCoop/ocsensor/compare/3e928a1...26f737a)**
* Migliora la versione mobile della nuova interfaccia di creazione segnalazione
* Corregge un bug nel reset del form nella nuova interfaccia di creazione segnalazione


## [2.2.9-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.8-genova...2.2.9-genova) - 2020-12-01


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 909a79b | 3e928a1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/909a79b...3e928a1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 909a79b and 3e928a1](https://github.com/OpencontentCoop/ocsensor/compare/909a79b...3e928a1)**
* Resetta il form di inserimento alla pressione del tasto annulla


## [2.2.8-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.7-genova...2.2.8-genova) - 2020-12-01


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.164.0 | 3.164.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.164.0...3.164.1)             |
| opencontent/ocsensor-ls | 794f869 | 909a79b | [...](https://github.com/OpencontentCoop/ocsensor/compare/794f869...909a79b)    |
| opencontent/ocsensorapi | 4052045 | e79fb0e | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4052045...e79fb0e) |
| php-http/discovery      | 1.12.0  | 1.13.0  | [...](https://github.com/php-http/discovery/compare/1.12.0...1.13.0)            |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 794f869 and 909a79b](https://github.com/OpencontentCoop/ocsensor/compare/794f869...909a79b)**
* Permette all'urp di moderare i commenti Migliora la nuova interfaccia di creazione segnalazione

**[opencontent/ocsensorapi changes between 4052045 and e79fb0e](https://github.com/OpencontentCoop/ocsensorapi/compare/4052045...e79fb0e)**
* Add comment moderation feature


## [2.2.7-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.6-genova...2.2.7-genova) - 2020-11-26


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | fff6e3b | 794f869 | [...](https://github.com/OpencontentCoop/ocsensor/compare/fff6e3b...794f869) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between fff6e3b and 794f869](https://github.com/OpencontentCoop/ocsensor/compare/fff6e3b...794f869)**
* Corregge alcune imprecisioni dell'interfaccia


## [2.2.6-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.5-genova...2.2.6-genova) - 2020-11-26



#### Installer
- add hide_type_choice and show_smart_gui fields in sensor_root
#### Code dependencies
| Changes                   | From    | To      | Compare                                                                         |
|---------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.163.1 | 3.164.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.163.1...3.164.0)             |
| opencontent/ocopendata-ls | 2.24.6  | 2.24.7  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.6...2.24.7)    |
| opencontent/ocsensor-ls   | 70f673b | fff6e3b | [...](https://github.com/OpencontentCoop/ocsensor/compare/70f673b...fff6e3b)    |
| opencontent/ocsensorapi   | 2b64cfc | 4052045 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/2b64cfc...4052045) |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.24.6 and 2.24.7](https://github.com/OpencontentCoop/ocopendata/compare/2.24.6...2.24.7)**
* opendataTools corregge un bug sul metodo i18n

**[opencontent/ocsensor-ls changes between 70f673b and fff6e3b](https://github.com/OpencontentCoop/ocsensor/compare/70f673b...fff6e3b)**
* Espone in frontend il modulo user/unactivated per il controllo delle utenze in registrazione
* Corregge la selezione dei destinatari dei messaggi privati e preseleziona gli incaricati
* Permette di spuntare il flag Proposta di risposta nei messaggi privati
* Corregge il formato di esportazione in csv per evitare un problema con Excel
* L'autore della segnalazione può ora aggiungere e rimuovere immagini
* Introduce una nuova interfaccia di inserimento (feature flagged)

**[opencontent/ocsensorapi changes between 2b64cfc and 4052045](https://github.com/OpencontentCoop/ocsensorapi/compare/2b64cfc...4052045)**
* Set post status as assigned if a group is selected
* Deny empty message
* Add isResponseProposal field in PrivateMessage
* Add addImage and removeImage actions
* Ad meta property to post structs


## [2.2.5-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.4-genova...2.2.5-genova) - 2020-11-19


#### Code dependencies
| Changes                         | From    | To      | Compare                                                                          |
|---------------------------------|---------|---------|----------------------------------------------------------------------------------|
| aws/aws-sdk-php                 | 3.161.0 | 3.163.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.161.0...3.163.1)              |
| opencontent/ocmultibinary-ls    | 2.2.4   | 2.2.5   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.4...2.2.5)    |
| opencontent/ocopendata-ls       | 2.24.4  | 2.24.6  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.4...2.24.6)     |
| opencontent/ocopendata_forms-ls | 1.6.7   | 1.6.8   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.7...1.6.8) |
| opencontent/ocsensor-ls         | 4df2aa4 | 70f673b | [...](https://github.com/OpencontentCoop/ocsensor/compare/4df2aa4...70f673b)     |
| opencontent/ocsensorapi         | 63645af | 2b64cfc | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/63645af...2b64cfc)  |


Relevant changes by repository:

**[opencontent/ocmultibinary-ls changes between 2.2.4 and 2.2.5](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.4...2.2.5)**
* Add ini setting to configure allowed file extension per attribute

**[opencontent/ocopendata-ls changes between 2.24.4 and 2.24.6](https://github.com/OpencontentCoop/ocopendata/compare/2.24.4...2.24.6)**
* Corregge un bug che si verificava nella pubblicazione di contenuti in installazioni multilingua
* Mantiene la stessa dimensione dell'array di risposta nelle richieste select-fields anche se manca la traduzione nella lingua corrente
* opendataTools i18n visualizza la prima traduzione esistente qualora non esistesse nella lingua corrente né nella lingua di fallback

**[opencontent/ocopendata_forms-ls changes between 1.6.7 and 1.6.8](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.7...1.6.8)**
* Permette di tradurre di un contenuto qualora si tenti di modificarlo in una lingua non ancora presente

**[opencontent/ocsensor-ls changes between 4df2aa4 and 70f673b](https://github.com/OpencontentCoop/ocsensor/compare/4df2aa4...70f673b)**
* Migliora il messaggio di errore nell'accesso a una segnalazione privata
* Migliora la funzionalità dei messaggi privati, dando la possibilità di specificare i destinatari
* Specifica i tipi di file ammessi nel campo senso_post/images

**[opencontent/ocsensorapi changes between 63645af and 2b64cfc](https://github.com/OpencontentCoop/ocsensorapi/compare/63645af...2b64cfc)**
* Send notification on private message only to specified receivers


## [2.2.4-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.3-genova...2.2.4-genova) - 2020-11-13


#### Code dependencies
| Changes                      | From    | To      | Compare                                                                       |
|------------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.159.1 | 3.161.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.159.1...3.161.0)           |
| opencontent/ocmultibinary-ls | 2.2.3   | 2.2.4   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.3...2.2.4) |
| php-http/guzzle6-adapter     | bd7b405 | 6f108cf | [...](https://github.com/php-http/guzzle6-adapter/compare/bd7b405...6f108cf)  |


Relevant changes by repository:

**[opencontent/ocmultibinary-ls changes between 2.2.3 and 2.2.4](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.3...2.2.4)**
* Add error translations


## [2.2.3-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.2-genova...2.2.3-genova) - 2020-11-11
- Change base image with php7.2.33-clean
- Fix category access in sensor anonymous role

#### Code dependencies
| Changes                          | From     | To      | Compare                                                                              |
|----------------------------------|----------|---------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.158.13 | 3.159.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.158.13...3.159.1)                 |
| composer/installers              | f69761f  | 207a91f | [...](https://github.com/composer/installers/compare/f69761f...207a91f)              |
| opencontent/ocinstaller          | 35c8665  | 7a4ec2b | [...](https://github.com/OpencontentCoop/ocinstaller/compare/35c8665...7a4ec2b)      |
| opencontent/ocmultibinary-ls     | 2.2.2    | 2.2.3   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.2...2.2.3)        |
| opencontent/ocsensor-ls          | b836850  | 4df2aa4 | [...](https://github.com/OpencontentCoop/ocsensor/compare/b836850...4df2aa4)         |
| opencontent/openpa-ls            | fd363e3  | a84de51 | [...](https://github.com/OpencontentCoop/openpa/compare/fd363e3...a84de51)           |
| php-http/message                 | 1.9.1    | 39db36d | [...](https://github.com/php-http/message/compare/1.9.1...39db36d)                   |
| symfony/polyfill-ctype           | f4ba089  | fade6de | [...](https://github.com/symfony/polyfill-ctype/compare/f4ba089...fade6de)           |
| symfony/polyfill-intl-idn        | 3b75acd  | 4c489fd | [...](https://github.com/symfony/polyfill-intl-idn/compare/3b75acd...4c489fd)        |
| symfony/polyfill-intl-normalizer | 727d109  | 69609f9 | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/727d109...69609f9) |
| symfony/polyfill-mbstring        | 39d483b  | 401c9d9 | [...](https://github.com/symfony/polyfill-mbstring/compare/39d483b...401c9d9)        |
| symfony/polyfill-php72           | cede45f  | 4a4465f | [...](https://github.com/symfony/polyfill-php72/compare/cede45f...4a4465f)           |
| symfony/polyfill-php73           | 8ff431c  | 8c0d39c | [...](https://github.com/symfony/polyfill-php73/compare/8ff431c...8c0d39c)           |
| symfony/polyfill-php80           | e70aa8b  | 3a11f3d | [...](https://github.com/symfony/polyfill-php80/compare/e70aa8b...3a11f3d)           |
| zetacomponents/console-tools     | 1.7.1    | 1.7.2   | [...](https://github.com/zetacomponents/ConsoleTools/compare/1.7.1...1.7.2)          |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 35c8665 and 7a4ec2b](https://github.com/OpencontentCoop/ocinstaller/compare/35c8665...7a4ec2b)**
* Fix dump_tag_tree tool, add recaptcha3 installer

**[opencontent/ocmultibinary-ls changes between 2.2.2 and 2.2.3](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.2...2.2.3)**
* Add missing translation

**[opencontent/ocsensor-ls changes between b836850 and 4df2aa4](https://github.com/OpencontentCoop/ocsensor/compare/b836850...4df2aa4)**
* Migliora la visualizzazione del menu di selezione delle zone se disabilitato

**[opencontent/openpa-ls changes between fd363e3 and a84de51](https://github.com/OpencontentCoop/openpa/compare/fd363e3...a84de51)**
* Permette la configurazione di Google Recaptcha v3
* Aggiorna le configurazioni degli attributi di classe direttamente dal openpa/recaptcha


## [2.2.2-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.1-genova...2.2.2-genova) - 2020-10-24


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensorapi | 9f636eb | 63645af | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/9f636eb...63645af) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 9f636eb and 63645af](https://github.com/OpencontentCoop/ocsensorapi/compare/9f636eb...63645af)**
* Fix  ocmultibinary download url


## [2.2.1-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.0-genova...2.2.1-genova) - 2020-10-24


#### Code dependencies
| Changes                          | From      | To      | Compare                                                                              |
|----------------------------------|-----------|---------|--------------------------------------------------------------------------------------|
| opencontent/ocsensor-ls          | 6e8d7ee   | b836850 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6e8d7ee...b836850)         |
| paragonie/random_compat          | v9.99.100 |         |                                                                                      |
| symfony/polyfill-ctype           | aed5969   | f4ba089 | [...](https://github.com/symfony/polyfill-ctype/compare/aed5969...f4ba089)           |
| symfony/polyfill-intl-idn        | fd17ae0   | 3b75acd | [...](https://github.com/symfony/polyfill-intl-idn/compare/fd17ae0...3b75acd)        |
| symfony/polyfill-intl-normalizer | 8db0ae7   | 727d109 | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/8db0ae7...727d109) |
| symfony/polyfill-mbstring        | b5f7b93   | 39d483b | [...](https://github.com/symfony/polyfill-mbstring/compare/b5f7b93...39d483b)        |
| symfony/polyfill-php70           | 3fe4140   |         |                                                                                      |
| symfony/polyfill-php72           | beecef6   | cede45f | [...](https://github.com/symfony/polyfill-php72/compare/beecef6...cede45f)           |
| symfony/polyfill-php73           | 9d920e3   | 8ff431c | [...](https://github.com/symfony/polyfill-php73/compare/9d920e3...8ff431c)           |
| symfony/polyfill-php80           | f54ef00   | e70aa8b | [...](https://github.com/symfony/polyfill-php80/compare/f54ef00...e70aa8b)           |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6e8d7ee and b836850](https://github.com/OpencontentCoop/ocsensor/compare/6e8d7ee...b836850)**
* Evita di esporre il codice fiscale nel csv export
* Visualizza gli utenti disabilitati in sensor/settings


## [2.2.0-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.1.0-genova...2.2.0-genova) - 2020-10-24


#### Code dependencies
| Changes                          | From           | To        | Compare                                                                              |
|----------------------------------|----------------|-----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.158.6        | 3.158.13  | [...](https://github.com/aws/aws-sdk-php/compare/3.158.6...3.158.13)                 |
| composer/installers              | c462a69        | f69761f   | [...](https://github.com/composer/installers/compare/c462a69...f69761f)              |
| guzzlehttp/promises              | 60d379c        | ddfeedf   | [...](https://github.com/guzzle/promises/compare/60d379c...ddfeedf)                  |
| opencontent/ocinstaller          | f5c1cc8        | 35c8665   | [...](https://github.com/OpencontentCoop/ocinstaller/compare/f5c1cc8...35c8665)      |
| opencontent/ocsensor-ls          | b31cc90        | 6e8d7ee   | [...](https://github.com/OpencontentCoop/ocsensor/compare/b31cc90...6e8d7ee)         |
| opencontent/ocsensorapi          | e890c7f        | 9f636eb   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/e890c7f...9f636eb)      |
| opencontent/openpa_sensor-ls     | 4.2.3          | 4.2.4     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.3...4.2.4)        |
| paragonie/random_compat          | v9.99.99.x-dev | v9.99.100 | [...](https://github.com/paragonie/random_compat/compare/v9.99.99.x-dev...v9.99.100) |
| symfony/polyfill-ctype           | 1c30264        | aed5969   | [...](https://github.com/symfony/polyfill-ctype/compare/1c30264...aed5969)           |
| symfony/polyfill-intl-idn        | 045643b        | fd17ae0   | [...](https://github.com/symfony/polyfill-intl-idn/compare/045643b...fd17ae0)        |
| symfony/polyfill-intl-normalizer | 37078a8        | 8db0ae7   | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/37078a8...8db0ae7) |
| symfony/polyfill-mbstring        | 48928d4        | b5f7b93   | [...](https://github.com/symfony/polyfill-mbstring/compare/48928d4...b5f7b93)        |
| symfony/polyfill-php70           | 0dd93f2        | 3fe4140   | [...](https://github.com/symfony/polyfill-php70/compare/0dd93f2...3fe4140)           |
| symfony/polyfill-php72           | dc6ef20        | beecef6   | [...](https://github.com/symfony/polyfill-php72/compare/dc6ef20...beecef6)           |
| symfony/polyfill-php73           | fffa1a5        | 9d920e3   | [...](https://github.com/symfony/polyfill-php73/compare/fffa1a5...9d920e3)           |
| symfony/polyfill-php80           | c3616e7        | f54ef00   | [...](https://github.com/symfony/polyfill-php80/compare/c3616e7...f54ef00)           |


Relevant changes by repository:

**[opencontent/ocinstaller changes between f5c1cc8 and 35c8665](https://github.com/OpencontentCoop/ocinstaller/compare/f5c1cc8...35c8665)**
* Make bigint type to ezcontentobject.published and ezcontentobject.modified
* add install pgcrypto script

**[opencontent/ocsensor-ls changes between b31cc90 and 6e8d7ee](https://github.com/OpencontentCoop/ocsensor/compare/b31cc90...6e8d7ee)**
* Disattiva di default la selezione dei destinatari nelle note private Corregge un bug nel comportamento della select dei gruppi/operatori Espone le configurazioni dei commenti e dei destinatari nelle note private in sensor/config
* Corregge i valori della tendina Tipo nella ricerca segnalazioni in base alla definizione della classe sensor_post

**[opencontent/ocsensorapi changes between e890c7f and 9f636eb](https://github.com/OpencontentCoop/ocsensorapi/compare/e890c7f...9f636eb)**
* Add CanSelectReceiverInPrivateMessage permission and use UseDirectPrivateMessage settings
* Add PostTypeService, fix some warnings

**[opencontent/openpa_sensor-ls changes between 4.2.3 and 4.2.4](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.3...4.2.4)**
* Reindicizza in background gli operatori quando viene modificato un gruppo


## [2.1.0-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.8...2.1.0-genova) - 2020-10-19


#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.154.5 | 3.158.6 | [...](https://github.com/aws/aws-sdk-php/compare/3.154.5...3.158.6)                    |
| clue/stream-filter                 | v1.4.1  | v1.5.0  | [...](https://github.com/clue/php-stream-filter/compare/v1.4.1...v1.5.0)               |
| guzzlehttp/promises                | bbf3b20 | 60d379c | [...](https://github.com/guzzle/promises/compare/bbf3b20...60d379c)                    |
| league/event                       | 7823f10 | 2.2.0   | [...](https://github.com/thephpleague/event/compare/7823f10...2.2.0)                   |
| opencontent/occodicefiscale-ls     | 542aa99 | daac5a8 | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/542aa99...daac5a8)    |
| opencontent/ocinstaller            | 8a20be4 | f5c1cc8 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/8a20be4...f5c1cc8)        |
| opencontent/ocopendata-ls          | 2.24.3  | 2.24.4  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.3...2.24.4)           |
| opencontent/ocopendata_forms-ls    | 1.6.4   | 1.6.7   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.4...1.6.7)       |
| opencontent/ocsensor-ls            | 4.8.1   | b31cc90 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.8.1...b31cc90)             |
| opencontent/ocsensorapi            | 4.8.0   | e890c7f | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.8.0...e890c7f)          |
| opencontent/openpa-ls              | 9f93297 | fd363e3 | [...](https://github.com/OpencontentCoop/openpa/compare/9f93297...fd363e3)             |
| opencontent/openpa_sensor-ls       | 4.2.2   | 4.2.3   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.2...4.2.3)          |
| php-http/discovery                 | 1.10.0  | 1.12.0  | [...](https://github.com/php-http/discovery/compare/1.10.0...1.12.0)                   |
| php-http/message                   | 1.9.0   | 1.9.1   | [...](https://github.com/php-http/message/compare/1.9.0...1.9.1)                       |
| psr/container                      | b1fbdff | 381524e | [...](https://github.com/php-fig/container/compare/b1fbdff...381524e)                  |
| psr/http-client                    | 2dfb5f6 | 22b2ef5 | [...](https://github.com/php-fig/http-client/compare/2dfb5f6...22b2ef5)                |
| symfony/deprecation-contracts      | 5fa56b4 | d940483 | [...](https://github.com/symfony/deprecation-contracts/compare/5fa56b4...d940483)      |
| symfony/event-dispatcher           | 98e8d61 | 5.x-dev | [...](https://github.com/symfony/event-dispatcher/compare/98e8d61...5.x-dev)           |
| symfony/event-dispatcher-contracts | 0ba7d54 | 5e8ae4d | [...](https://github.com/symfony/event-dispatcher-contracts/compare/0ba7d54...5e8ae4d) |
| symfony/options-resolver           | c2565c6 | 5.x-dev | [...](https://github.com/symfony/options-resolver/compare/c2565c6...5.x-dev)           |
| symfony/polyfill-php72             | 639447d | dc6ef20 | [...](https://github.com/symfony/polyfill-php72/compare/639447d...dc6ef20)             |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between 542aa99 and daac5a8](https://github.com/OpencontentCoop/occodicefiscale/compare/542aa99...daac5a8)**
* Considera solo gli oggetti pubblicati nel calcolo dei doppioni

**[opencontent/ocinstaller changes between 8a20be4 and f5c1cc8](https://github.com/OpencontentCoop/ocinstaller/compare/8a20be4...f5c1cc8)**
* Add remove extra locations feature in content tree installer
* Now inside a resource can import another resource

**[opencontent/ocopendata-ls changes between 2.24.3 and 2.24.4](https://github.com/OpencontentCoop/ocopendata/compare/2.24.3...2.24.4)**
* Aumento il limite massimo di ricerca in env default a 300 (per compatibilità con ocopendata_forms)

**[opencontent/ocopendata_forms-ls changes between 1.6.4 and 1.6.7](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.4...1.6.7)**
* Permette l'utilizzo della libreria UploadFieldConnector in esecuzioni da linea di comando
* Rimuove un log di debugging
* Corregge un problema di configurazione

**[opencontent/ocsensor-ls changes between 4.8.1 and b31cc90](https://github.com/OpencontentCoop/ocsensor/compare/4.8.1...b31cc90)**
* Abilita l'approver a modificare il riferimento per il cittadino
* Abilita l'approver a modificare il riferimento per il cittadino
* Introduce la gestione dei gruppi di incaricati
* Corregge la selezione degli operatori nell'interfaccia di modifica incaricato
* Permette di filtrare l'esecuzione dei webhook per gruppo
* Considera le sottocategorie e le sottoaree nei filtri di categoria e di area del motore di ricerca della pagina Segnalazioni
* Corregge un bug per cui non venivano correttamente visualizzati i filtri dei webhook

**[opencontent/ocsensorapi changes between 4.8.0 and e890c7f](https://github.com/OpencontentCoop/ocsensorapi/compare/4.8.0...e890c7f)**
* Introduce la gestione dei gruppi di incaricati
* Corregge la versione della dipendenza league/event
* Fix assign action logic
* Add notification target configurable per notification type
* Fix log messages and filename
* Fix code comment
* Add addCurrentUserAddress flag
* Set all mail receivers as primary receiver

**[opencontent/openpa-ls changes between 9f93297 and fd363e3](https://github.com/OpencontentCoop/openpa/compare/9f93297...fd363e3)**
* Corregge l'id del tree menu in caso di menu virtualizzato su alberatura di tag

**[opencontent/openpa_sensor-ls changes between 4.2.2 and 4.2.3](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.2...4.2.3)**
* Svuota la cache degli operatori e dei gruppi alla modifica


## [2.0.8](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.7...2.0.8) - 2020-09-18


#### Code dependencies
| Changes                            | From    | To      | Compare                                                                             |
|------------------------------------|---------|---------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.152.0 | 3.154.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.152.0...3.154.5)                 |
| opencontent/ezpostgresqlcluster-ls | 1c50dd6 | f37c194 | [...](https://github.com/Opencontent/ezpostgresqlcluster/compare/1c50dd6...f37c194) |
| opencontent/ocopendata-ls          | 2.24.2  | 2.24.3  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.2...2.24.3)        |
| opencontent/ocsensor-ls            | 4.8.0   | 4.8.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.8.0...4.8.1)            |
| opencontent/ocsensorapi            | 4.7.0   | 4.8.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.7.0...4.8.0)         |
| psr/container                      | fc1bc36 | b1fbdff | [...](https://github.com/php-fig/container/compare/fc1bc36...b1fbdff)               |
| psr/event-dispatcher               | 3b1c107 | 3ef040d | [...](https://github.com/php-fig/event-dispatcher/compare/3b1c107...3ef040d)        |
| psr/http-factory                   | 1a2099a | 36fa03d | [...](https://github.com/php-fig/http-factory/compare/1a2099a...36fa03d)            |
| psr/log                            | 0f73288 | dd738d0 | [...](https://github.com/php-fig/log/compare/0f73288...dd738d0)                     |
| symfony/polyfill-mbstring          | a6977d6 | 48928d4 | [...](https://github.com/symfony/polyfill-mbstring/compare/a6977d6...48928d4)       |
| symfony/polyfill-php80             | d87d576 | c3616e7 | [...](https://github.com/symfony/polyfill-php80/compare/d87d576...c3616e7)          |


Relevant changes by repository:

**[opencontent/ezpostgresqlcluster-ls changes between 1c50dd6 and f37c194](https://github.com/Opencontent/ezpostgresqlcluster/compare/1c50dd6...f37c194)**
* Check OpenPABase class exists

**[opencontent/ocopendata-ls changes between 2.24.2 and 2.24.3](https://github.com/OpencontentCoop/ocopendata/compare/2.24.2...2.24.3)**
* Corregge un bug sul parsing dei tag nel AttributeConverter

**[opencontent/ocsensor-ls changes between 4.8.0 and 4.8.1](https://github.com/OpencontentCoop/ocsensor/compare/4.8.0...4.8.1)**
* Aggiunge uno script per configurare il password lifetime

**[opencontent/ocsensorapi changes between 4.7.0 and 4.8.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.7.0...4.8.0)**
* Add super user capabilities


## [2.0.7](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.6...2.0.7) - 2020-09-07


#### Code dependencies
| Changes                            | From     | To      | Compare                                                                                |
|------------------------------------|----------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.147.12 | 3.152.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.147.12...3.152.0)                   |
| mtdowling/jmespath.php             | 42dae2c  | 30dfa00 | [...](https://github.com/jmespath/jmespath.php/compare/42dae2c...30dfa00)              |
| opencontent/ocbootstrap-ls         | 1.10.1   | 1.10.2  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.1...1.10.2)          |
| opencontent/ocopendata_forms-ls    | 1.6.3    | 1.6.4   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.3...1.6.4)       |
| opencontent/ocsensor-ls            | 4.7.0    | 4.8.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.7.0...4.8.0)               |
| opencontent/ocsensorapi            | 4.6.0    | 4.7.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.6.0...4.7.0)            |
| php-http/discovery                 | 64a18cc  | 1.10.0  | [...](https://github.com/php-http/discovery/compare/64a18cc...1.10.0)                  |
| php-http/message                   | 226ba9f  | 1.9.0   | [...](https://github.com/php-http/message/compare/226ba9f...1.9.0)                     |
| symfony/deprecation-contracts      | 5e20b83  | 5fa56b4 | [...](https://github.com/symfony/deprecation-contracts/compare/5e20b83...5fa56b4)      |
| symfony/event-dispatcher           | bbb461a  | 98e8d61 | [...](https://github.com/symfony/event-dispatcher/compare/bbb461a...98e8d61)           |
| symfony/event-dispatcher-contracts | f6f613d  | 0ba7d54 | [...](https://github.com/symfony/event-dispatcher-contracts/compare/f6f613d...0ba7d54) |
| symfony/options-resolver           | e25bc4b  | c2565c6 | [...](https://github.com/symfony/options-resolver/compare/e25bc4b...c2565c6)           |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.1 and 1.10.2](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.1...1.10.2)**
* Corregge un bug che impediva la visualizzazione dei testi di aiuto nei form dinamici

**[opencontent/ocopendata_forms-ls changes between 1.6.3 and 1.6.4](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.3...1.6.4)**
* Espone l'indirizzo da geocoder nominatim in modo meno verboso

**[opencontent/ocsensor-ls changes between 4.7.0 and 4.8.0](https://github.com/OpencontentCoop/ocsensor/compare/4.7.0...4.8.0)**
* Permette di rimuovere un osservatore da interfaccia

**[opencontent/ocsensorapi changes between 4.6.0 and 4.7.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.6.0...4.7.0)**
* Add remove observer action


## [2.0.6](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.5...2.0.6) - 2020-08-05
- Update publiccode
- Fix Admin role, sensor operator group select

#### Code dependencies
| Changes                              | From    | To             | Compare                                                                                |
|--------------------------------------|---------|----------------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                      | 3.143.1 | 3.147.12       | [...](https://github.com/aws/aws-sdk-php/compare/3.143.1...3.147.12)                   |
| friendsofsymfony/http-cache          | e6e9218 | 42c96a0        | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/e6e9218...42c96a0)      |
| mtdowling/jmespath.php               | 52168cb | 42dae2c        | [...](https://github.com/jmespath/jmespath.php/compare/52168cb...42dae2c)              |
| opencontent/ocbootstrap-ls           | 1.9.8   | 1.10.1         | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.8...1.10.1)           |
| opencontent/ocinstaller              | 5a366f4 | 8a20be4        | [...](https://github.com/OpencontentCoop/ocinstaller/compare/5a366f4...8a20be4)        |
| opencontent/ocoperatorscollection-ls | 2.1.1   | 2.2.0          | [...](https://github.com/OpencontentCoop/ocoperatorscollection/compare/2.1.1...2.2.0)  |
| opencontent/ocsensor-ls              | 4.5.5   | 4.7.0          | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.5.5...4.7.0)               |
| opencontent/ocsensorapi              | 4.5.3   | 4.6.0          | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.3...4.6.0)            |
| opencontent/openpa-ls                | c9105eb | 9f93297        | [...](https://github.com/OpencontentCoop/openpa/compare/c9105eb...9f93297)             |
| opencontent/openpa_sensor-ls         | 4.2.1   | 4.2.2          | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.1...4.2.2)          |
| opencontent/openpa_theme_2014-ls     | 2.15.0  | 2.15.2         | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.0...2.15.2)    |
| paragonie/random_compat              |         | v9.99.99.x-dev |                                                                                        |
| php-http/client-common               | 7c27fe6 | e37e46c        | [...](https://github.com/php-http/client-common/compare/7c27fe6...e37e46c)             |
| php-http/discovery                   | 1.8.0   | 64a18cc        | [...](https://github.com/php-http/discovery/compare/1.8.0...64a18cc)                   |
| php-http/httplug                     | c9b2424 | 191a0a1        | [...](https://github.com/php-http/httplug/compare/c9b2424...191a0a1)                   |
| php-http/promise                     | 02ee67f | 4c4c1f9        | [...](https://github.com/php-http/promise/compare/02ee67f...4c4c1f9)                   |
| psr/http-client                      | fd5d37a | 2dfb5f6        | [...](https://github.com/php-fig/http-client/compare/fd5d37a...2dfb5f6)                |
| psr/http-factory                     |         | 1a2099a        |                                                                                        |
| symfony/event-dispatcher             | 0403953 | bbb461a        | [...](https://github.com/symfony/event-dispatcher/compare/0403953...bbb461a)           |
| symfony/event-dispatcher-contracts   | 3bf9307 | f6f613d        | [...](https://github.com/symfony/event-dispatcher-contracts/compare/3bf9307...f6f613d) |
| symfony/options-resolver             | 9ae2b94 | e25bc4b        | [...](https://github.com/symfony/options-resolver/compare/9ae2b94...e25bc4b)           |
| symfony/polyfill-ctype               | 2edd75b | 1c30264        | [...](https://github.com/symfony/polyfill-ctype/compare/2edd75b...1c30264)             |
| symfony/polyfill-intl-idn            | a57f816 | 045643b        | [...](https://github.com/symfony/polyfill-intl-idn/compare/a57f816...045643b)          |
| symfony/polyfill-intl-normalizer     |         | 37078a8        |                                                                                        |
| symfony/polyfill-mbstring            | 7110338 | a6977d6        | [...](https://github.com/symfony/polyfill-mbstring/compare/7110338...a6977d6)          |
| symfony/polyfill-php70               |         | 0dd93f2        |                                                                                        |
| symfony/polyfill-php72               | 3d9c70f | 639447d        | [...](https://github.com/symfony/polyfill-php72/compare/3d9c70f...639447d)             |
| symfony/polyfill-php73               | fa0837f | fffa1a5        | [...](https://github.com/symfony/polyfill-php73/compare/fa0837f...fffa1a5)             |
| symfony/polyfill-php80               | 4a5b6bb | d87d576        | [...](https://github.com/symfony/polyfill-php80/compare/4a5b6bb...d87d576)             |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.8 and 1.10.1](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.8...1.10.1)**
* Corregge un bug che impediva la corretta visualizzazione dei checkbox nei form dinamici
* Corregge un errore nella pagina di conferma pubblicazione
* Introduce i template bootstrap 4 per ezsurvey
* Corregge la visualizzazione dei form per ezsurvey in bootstrap 4

**[opencontent/ocinstaller changes between 5a366f4 and 8a20be4](https://github.com/OpencontentCoop/ocinstaller/compare/5a366f4...8a20be4)**
* Write installer log to file
* Fix workflow installer
* Fix expiryPassword in dryrun
* Add change_state, change_section, tag_description Bugifx
* Add reindex step

**[opencontent/ocoperatorscollection-ls changes between 2.1.1 and 2.2.0](https://github.com/OpencontentCoop/ocoperatorscollection/compare/2.1.1...2.2.0)**
* Avoid switch to user with admin capabilities

**[opencontent/ocsensor-ls changes between 4.5.5 and 4.7.0](https://github.com/OpencontentCoop/ocsensor/compare/4.5.5...4.7.0)**
* Aggiunge a sensor la notifica per l'evento add_approver e corregge alcuni bug di implementazione dei template di notifica
* Evita di rimuovere l'operatore spostandolo nel gruppo utenti
* Inietta nelle definizioni di CanClose e CanResponde gli utenti urp nel caso in cui sia attiva la configurazione ForceUrpApproverOnFix Imposta la configurazione ForceUrpApproverOnFix attiva per default

**[opencontent/ocsensorapi changes between 4.5.3 and 4.6.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.3...4.6.0)**
* Add on_add_approver notification and fix the participant mail address finder
* Set restrict responders in CanClose and CanRespond permission definitions

**[opencontent/openpa-ls changes between c9105eb and 9f93297](https://github.com/OpencontentCoop/openpa/compare/c9105eb...9f93297)**
* Corregge un bug in openpa/object
* Corregge la fetch per il controllo degli stati
* Permette a content_link di riconoscere se un link è al nodo o a un nodo/link esterno
* Corregge un possibile errore nel calcolo delle aree tematiche
* Corregge un typo in cookie
* Migliora la visualizzazione della versione del software

**[opencontent/openpa_sensor-ls changes between 4.2.1 and 4.2.2](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.1...4.2.2)**
* Corregge un bug sul calcolo dell'url degli asset

**[opencontent/openpa_theme_2014-ls changes between 2.15.0 and 2.15.2](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.0...2.15.2)**
* Rimuove lo spellcheck di solr
* Corregge il link al download csv in trasparenza


## [2.0.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.4...2.0.5) - 2020-06-26


#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.138.8 | 3.143.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.138.8...3.143.1)                    |
| composer/installers                | 8669edf | c462a69 | [...](https://github.com/composer/installers/compare/8669edf...c462a69)                |
| guzzlehttp/promises                | 89b1a76 | bbf3b20 | [...](https://github.com/guzzle/promises/compare/89b1a76...bbf3b20)                    |
| justinrainbow/json-schema          | 5.2.9   | 5.x-dev | [...](https://github.com/justinrainbow/json-schema/compare/5.2.9...5.x-dev)            |
| opencontent/ocinstaller            | 7ef45b7 | 5a366f4 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/7ef45b7...5a366f4)        |
| opencontent/ocsensor-ls            | 4.5.4   | 4.5.5   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.5.4...4.5.5)               |
| opencontent/ocsensorapi            | 4.5.2   | 4.5.3   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.2...4.5.3)            |
| opencontent/openpa-ls              | 5dc1290 | c9105eb | [...](https://github.com/OpencontentCoop/openpa/compare/5dc1290...c9105eb)             |
| opencontent/openpa_userprofile-ls  | 2e192b1 | 4c7a67e | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/2e192b1...4c7a67e) |
| php-http/discovery                 | 7e30d5c | 1.8.0   | [...](https://github.com/php-http/discovery/compare/7e30d5c...1.8.0)                   |
| symfony/deprecation-contracts      | ede224d | 5e20b83 | [...](https://github.com/symfony/deprecation-contracts/compare/ede224d...5e20b83)      |
| symfony/event-dispatcher           | 8a53fd3 | 0403953 | [...](https://github.com/symfony/event-dispatcher/compare/8a53fd3...0403953)           |
| symfony/event-dispatcher-contracts | 405952c | 3bf9307 | [...](https://github.com/symfony/event-dispatcher-contracts/compare/405952c...3bf9307) |
| symfony/options-resolver           | f3f9b9c | 9ae2b94 | [...](https://github.com/symfony/options-resolver/compare/f3f9b9c...9ae2b94)           |
| symfony/polyfill-ctype             | e94c8b1 | 2edd75b | [...](https://github.com/symfony/polyfill-ctype/compare/e94c8b1...2edd75b)             |
| symfony/polyfill-intl-idn          | v1.17.0 | a57f816 | [...](https://github.com/symfony/polyfill-intl-idn/compare/v1.17.0...a57f816)          |
| symfony/polyfill-mbstring          | fa79b11 | 7110338 | [...](https://github.com/symfony/polyfill-mbstring/compare/fa79b11...7110338)          |
| symfony/polyfill-php72             | f048e61 | 3d9c70f | [...](https://github.com/symfony/polyfill-php72/compare/f048e61...3d9c70f)             |
| symfony/polyfill-php73             | a760d89 | fa0837f | [...](https://github.com/symfony/polyfill-php73/compare/a760d89...fa0837f)             |
| symfony/polyfill-php80             | 5e30b27 | 4a5b6bb | [...](https://github.com/symfony/polyfill-php80/compare/5e30b27...4a5b6bb)             |
| zetacomponents/mail                | 1.9.1   | 1.9.2   | [...](https://github.com/zetacomponents/Mail/compare/1.9.1...1.9.2)                    |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 7ef45b7 and 5a366f4](https://github.com/OpencontentCoop/ocinstaller/compare/7ef45b7...5a366f4)**
* Add classid var function calling eZContentClass::classIDByIdentifier
* add classattributeid and classattributeid_list expression function
* Check trash Allow extra root datadir path
* Add --force option to ignore version check Add add_tag, remove_tag, move_tag to handle single tag modification
* Add patch_content to update single content attribute
* Fix compatibility with eZ version 5.90.0
* Add sort data in patch_content Fix patch_content error handling Improve version_compare Fix bug in tagtree synonyms Add dump script tools

**[opencontent/ocsensor-ls changes between 4.5.4 and 4.5.5](https://github.com/OpencontentCoop/ocsensor/compare/4.5.4...4.5.5)**
* Assegna gli osservatori configurati nelle zone al primo accesso al post del riferimento per il cittadino

**[opencontent/ocsensorapi changes between 4.5.2 and 4.5.3](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.2...4.5.3)**
* Fire on_approver_first_read custom event and add ApproverFirstReadListener

**[opencontent/openpa-ls changes between 5dc1290 and c9105eb](https://github.com/OpencontentCoop/openpa/compare/5dc1290...c9105eb)**
* Corregge il riconoscimento automatico del software in uso per la visualizzazione delle metriche di utilizzo
* Corregge un bug nella definizione degli handler dei blocchi
* Permette la personalizzazione dei valori head/meta da openpa/seo
* Visualizza la versione dell'installer se presente in CreditSettings/CodeVersion

**[opencontent/openpa_userprofile-ls changes between 2e192b1 and 4c7a67e](https://github.com/OpencontentCoop/openpa_userprofile/compare/2e192b1...4c7a67e)**
* Considera gli attributi raccomandati per la validazione del profilo


## [2.0.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.3...2.0.4) - 2020-06-10
- Fix gitlab ci branch/tag name



## [2.0.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.2...2.0.3) - 2020-06-10
- Make solr data dir in Dockerfile.solr



## [2.0.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.1...2.0.2) - 2020-06-08
- Added publiccode
- Update deps

#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.137.5 | 3.138.8 | [...](https://github.com/aws/aws-sdk-php/compare/3.137.5...3.138.8)                    |
| friendsofsymfony/http-cache        | f0cd186 | e6e9218 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/f0cd186...e6e9218)      |
| opencontent/ocsensor-ls            | 4.5.2   | 4.5.4   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.5.2...4.5.4)               |
| opencontent/ocsensorapi            | 4.5.0   | 4.5.2   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.0...4.5.2)            |
| opencontent/openpa-ls              | ffa1c9f | 5dc1290 | [...](https://github.com/OpencontentCoop/openpa/compare/ffa1c9f...5dc1290)             |
| php-http/client-common             | ca984d8 | 7c27fe6 | [...](https://github.com/php-http/client-common/compare/ca984d8...7c27fe6)             |
| php-http/discovery                 | 82dbef6 | 7e30d5c | [...](https://github.com/php-http/discovery/compare/82dbef6...7e30d5c)                 |
| php-http/message                   | fcf9b45 | 226ba9f | [...](https://github.com/php-http/message/compare/fcf9b45...226ba9f)                   |
| psr/event-dispatcher               | cfea31e | 3b1c107 | [...](https://github.com/php-fig/event-dispatcher/compare/cfea31e...3b1c107)           |
| symfony/event-dispatcher           | bc8f774 | 8a53fd3 | [...](https://github.com/symfony/event-dispatcher/compare/bc8f774...8a53fd3)           |
| symfony/event-dispatcher-contracts | 5a749bd | 405952c | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5a749bd...405952c) |
| symfony/options-resolver           | 0ab7e5a | f3f9b9c | [...](https://github.com/symfony/options-resolver/compare/0ab7e5a...f3f9b9c)           |
| symfony/polyfill-ctype             | 4719fa9 | e94c8b1 | [...](https://github.com/symfony/polyfill-ctype/compare/4719fa9...e94c8b1)             |
| symfony/polyfill-intl-idn          | 47bd6aa | v1.17.0 | [...](https://github.com/symfony/polyfill-intl-idn/compare/47bd6aa...v1.17.0)          |
| symfony/polyfill-mbstring          | 81ffd3a | fa79b11 | [...](https://github.com/symfony/polyfill-mbstring/compare/81ffd3a...fa79b11)          |
| symfony/polyfill-php72             | 41d115a | f048e61 | [...](https://github.com/symfony/polyfill-php72/compare/41d115a...f048e61)             |
| symfony/polyfill-php73             | 0f27e9f | a760d89 | [...](https://github.com/symfony/polyfill-php73/compare/0f27e9f...a760d89)             |
| symfony/polyfill-php80             | cb64c50 | 5e30b27 | [...](https://github.com/symfony/polyfill-php80/compare/cb64c50...5e30b27)             |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.5.2 and 4.5.4](https://github.com/OpencontentCoop/ocsensor/compare/4.5.2...4.5.4)**
* Migliora la visualizzazione dell'associazione gruppo-categoria
* Migliora la visualizzazione della dashboard operatori suddividendo i post per stato interno
* Permette di rigenerare le immagini avatar svuotando la cache da backend
* Evidenzia maggiormente l'avatar di un utente rimosso

**[opencontent/ocsensorapi changes between 4.5.0 and 4.5.2](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.0...4.5.2)**
* Avoid duplicated message in same second
* Check approvers consistency

**[opencontent/openpa-ls changes between ffa1c9f and 5dc1290](https://github.com/OpencontentCoop/openpa/compare/ffa1c9f...5dc1290)**
* Corregge un errore nella funzione di template per la ricerca delle strutture
* Corregge un bug in OpenPASMTPTransport


## [2.0.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.0...2.0.1) - 2020-05-07
- Add copy post feature
- Remove solr dev data + fix gitignore

#### Code dependencies
| Changes                        | From    | To      | Compare                                                                             |
|--------------------------------|---------|---------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.134.5 | 3.137.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.134.5...3.137.5)                 |
| composer/installers            | b93bcf0 | 8669edf | [...](https://github.com/composer/installers/compare/b93bcf0...8669edf)             |
| friendsofsymfony/http-cache    | 7560f30 | f0cd186 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/7560f30...f0cd186)   |
| opencontent/ocfoshttpcache-ls  | aa42fb9 | faa918c | [...](https://github.com/OpencontentCoop/ocfoshttpcache/compare/aa42fb9...faa918c)  |
| opencontent/ocinstaller        | 83adb0a | 7ef45b7 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/83adb0a...7ef45b7)     |
| opencontent/ocopendata-ls      | 2.24.1  | 2.24.2  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.1...2.24.2)        |
| opencontent/ocsensor-ls        | 4.4.2   | 4.5.2   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.4.2...4.5.2)            |
| opencontent/ocsensorapi        | 4.4.0   | 4.5.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.4.0...4.5.0)         |
| opencontent/ocsupport-ls       | 49a42aa | 2ee7846 | [...](https://github.com/OpencontentCoop/ocsupport/compare/49a42aa...2ee7846)       |
| opencontent/ocwebhookserver-ls | 32df3cf | aabd35a | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/32df3cf...aabd35a) |
| opencontent/openpa-ls          | 5353d99 | ffa1c9f | [...](https://github.com/OpencontentCoop/openpa/compare/5353d99...ffa1c9f)          |
| psr/simple-cache               | 408d5ea | 5a7b96b | [...](https://github.com/php-fig/simple-cache/compare/408d5ea...5a7b96b)            |
| symfony/event-dispatcher       | d364824 | bc8f774 | [...](https://github.com/symfony/event-dispatcher/compare/d364824...bc8f774)        |
| symfony/options-resolver       | 1cfd933 | 0ab7e5a | [...](https://github.com/symfony/options-resolver/compare/1cfd933...0ab7e5a)        |
| symfony/polyfill-intl-idn      |         | 47bd6aa |                                                                                     |
| symfony/polyfill-php72         |         | 41d115a |                                                                                     |
| symfony/polyfill-php80         | 8854dc8 | cb64c50 | [...](https://github.com/symfony/polyfill-php80/compare/8854dc8...cb64c50)          |


Relevant changes by repository:

**[opencontent/ocfoshttpcache-ls changes between aa42fb9 and faa918c](https://github.com/OpencontentCoop/ocfoshttpcache/compare/aa42fb9...faa918c)**
* Permette di inserire l'id utente nel context hash

**[opencontent/ocinstaller changes between 83adb0a and 7ef45b7](https://github.com/OpencontentCoop/ocinstaller/compare/83adb0a...7ef45b7)**
* Accept custom date() var
* Add remove role assignment Add version compare condition

**[opencontent/ocopendata-ls changes between 2.24.1 and 2.24.2](https://github.com/OpencontentCoop/ocopendata/compare/2.24.1...2.24.2)**
* Corregge l'esposizione di oggetti correlati in default environment qualora essi siano convertiti in formato esteso

**[opencontent/ocsensor-ls changes between 4.4.2 and 4.5.2](https://github.com/OpencontentCoop/ocsensor/compare/4.4.2...4.5.2)**
* Aggiunge il pulsante che permette di duplicare una segnalazione
* Visualizza le segnalazioni correlate nel corpo della segnalazione
* Corregge un problema di generazione delle chiavi di cache per Varnish

**[opencontent/ocsensorapi changes between 4.4.0 and 4.5.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.4.0...4.5.0)**
* Add relatedItems post field

**[opencontent/ocsupport-ls changes between 49a42aa and 2ee7846](https://github.com/OpencontentCoop/ocsupport/compare/49a42aa...2ee7846)**
* Add installer info

**[opencontent/ocwebhookserver-ls changes between 32df3cf and aabd35a](https://github.com/OpencontentCoop/ocwebhookserver/compare/32df3cf...aabd35a)**
* Add simple endpoint api
* Add class definition shared_link
* Fix trigger check, add response to test

**[opencontent/openpa-ls changes between 5353d99 and ffa1c9f](https://github.com/OpencontentCoop/openpa/compare/5353d99...ffa1c9f)**
* Aggiunge il link all'area personale ai valori inseribili nei contatti


## [2.0.0](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.13...2.0.0) - 2020-04-08
- Update docker-compose with ini env vars Update installer Update deps
- Update installer to 1.2.6

#### Code dependencies
| Changes                           | From     | To          | Compare                                                                                |
|-----------------------------------|----------|-------------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                   | 3.133.27 | 3.134.5     | [...](https://github.com/aws/aws-sdk-php/compare/3.133.27...3.134.5)                   |
| composer/installers               | 7d610d5  | b93bcf0     | [...](https://github.com/composer/installers/compare/7d610d5...b93bcf0)                |
| ezsystems/ezpublish-legacy        | 855cc50  | 2020.1000.1 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/855cc50...2020.1000.1)   |
| opencontent/ocbootstrap-ls        | 1.9.6    | 1.9.8       | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.6...1.9.8)            |
| opencontent/ocinstaller           | ce5b6e1  | 83adb0a     | [...](https://github.com/OpencontentCoop/ocinstaller/compare/ce5b6e1...83adb0a)        |
| opencontent/ocmultibinary-ls      | 2.2.1    | 2.2.2       | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.1...2.2.2)          |
| opencontent/ocopendata-ls         | 2.23.5   | 2.24.1      | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.23.5...2.24.1)           |
| opencontent/ocsensor-ls           | 4.3.11   | 4.4.2       | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.11...4.4.2)              |
| opencontent/ocsensorapi           | 4.3.5    | 4.4.0       | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.5...4.4.0)            |
| opencontent/openpa-ls             | 20665ce  | 5353d99     | [...](https://github.com/OpencontentCoop/openpa/compare/20665ce...5353d99)             |
| opencontent/openpa_sensor-ls      | 4.1.4    | 4.2.1       | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.4...4.2.1)          |
| opencontent/openpa_userprofile-ls | 20b44fb  | 2e192b1     | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/20b44fb...2e192b1) |
| php-http/client-common            | 03086a2  | ca984d8     | [...](https://github.com/php-http/client-common/compare/03086a2...ca984d8)             |
| psr/log                           | e1cb6ec  | 0f73288     | [...](https://github.com/php-fig/log/compare/e1cb6ec...0f73288)                        |
| symfony/event-dispatcher          | 7d4f83e  | d364824     | [...](https://github.com/symfony/event-dispatcher/compare/7d4f83e...d364824)           |
| symfony/options-resolver          | 1942f69  | 1cfd933     | [...](https://github.com/symfony/options-resolver/compare/1942f69...1cfd933)           |
| symfony/polyfill-mbstring         | 766ee47  | 81ffd3a     | [...](https://github.com/symfony/polyfill-mbstring/compare/766ee47...81ffd3a)          |
| symfony/polyfill-php80            |          | 8854dc8     |                                                                                        |
| zetacomponents/console-tools      | 1.7      | 1.7.1       | [...](https://github.com/zetacomponents/ConsoleTools/compare/1.7...1.7.1)              |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.6 and 1.9.8](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.6...1.9.8)**
* Disabilita il drag and drop nell'edit di un attributo ezpage
* Evita in warning in siteaccess di backend per override mancante

**[opencontent/ocinstaller changes between ce5b6e1 and 83adb0a](https://github.com/OpencontentCoop/ocinstaller/compare/ce5b6e1...83adb0a)**
* Add installer type (for submodules)
* Show create/update tag in debug message
* Add sort and priority in content steps Fix var replacer Add sql_copy_from_tsv type Fix bugs
* Bugfixes
* Allow content update Assign role by remote
* Fix dump table

**[opencontent/ocmultibinary-ls changes between 2.2.1 and 2.2.2](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.1...2.2.2)**
* Permette il download se l'oggetto è in draft e l'utente corrente ne è il proprietario (per preview immagini)

**[opencontent/ocopendata-ls changes between 2.23.5 and 2.24.1](https://github.com/OpencontentCoop/ocopendata/compare/2.23.5...2.24.1)**
* Ottimizza la creazione di sinonimi e di traduzioni di tag via api
* Aggiunge il remote id del main node all'Api Content Metadata
* Permette la creazione e la modifica via api di attributi di tipo ezpage
* Corregge un problema nel calcolo del nodo principale in Metadata
* Merge branch 'ezpage_rest_field'    ezpage_rest_field:   Permette la creazione e la modifica via api di attributi di tipo ezpage
* Corregge lo svuotamento cache del Section repository

**[opencontent/ocsensor-ls changes between 4.3.11 and 4.4.2](https://github.com/OpencontentCoop/ocsensor/compare/4.3.11...4.4.2)**
* Migliora l'interfaccia utente anonimo e autenticato Introduce la possibilità di inserire più immagini Corregge il bug per cui era possibile scaricare un'immagine di una segnalazione privata Introduce il controllo per ip dei permessi di lettura delle api delle immagini Introduce le configurazioni per nascondere la scelta del consenso di pubblicazione e per nascondere la timeline dettagliata Introduce i settings per attivare l'assegnazione casuale dell'operatore di categoria e per la reimpostazione forzata dell'urp a intervento terminato Corregge il termine Assegnatario in Incaricato Corregge l'autenticazione di default nelle api Aggiunge il codice fiscale dell'autore della segnalazione nell'esportazione del csv
* Corregge asset_url nelle clusterizzazioni su aws
* Rimuove le PolicyOmitList di ocsensor in quanto assegnate di default dal ruolo Sensor Anonymous

**[opencontent/ocsensorapi changes between 4.3.5 and 4.4.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.5...4.4.0)**
* Avoid warning in category service
* Fix AddApproverAction Use CategoryAutomaticAssignToRandomOperator setting in AddCategoryAction Remove all owners in CloseAction Use ForceUrpApproverOnFix setting in FixAction Use HideTimelineDetails setting in PostService::setUserPostAware Cache groups and operators tree Handle multiple images field in post api Handle fiscal code field in user api Use new api url for file Fix minor bugs Change api version in abb5bf40-077d-42f5-b0fe-079538e6d650

**[opencontent/openpa-ls changes between 20665ce and 5353d99](https://github.com/OpencontentCoop/openpa/compare/20665ce...5353d99)**
* In openpa/object viene considerato il main node
* Aggiunge il codice SDI ai contatti
* Aggiunge la possibilità di escludere alberature dai menu
* Permette la configurazione del mittente email da applicazione
* Rimuovo ruolo e ruolo2 da ContentMain/AbstractIdentifiers  1940
* Considera la variabile d'ambiente EZ_INSTANCE per la definizione dell'identificativo dell'istanza corrente
* Carica la versione del software in CreditsSettings::CodeVersion da file VERSION se presente
* Corregge lo script di migrazione da nfs a s3
* Rimuove codice inutilizzato o obsoleto
* Nella configurazione cluster la cache di ocopendata viene salvata in locale invece che in redis
* Aggiunge TikTok ai possibili contatti
* Considera la variabile d'ambiente EZ_INSTANCE per la definizione dell'identificativo del siteaccess custom per evitare conflitti con i nomi dei moduli

**[opencontent/openpa_sensor-ls changes between 4.1.4 and 4.2.1](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.4...4.2.1)**
* Nasconde editor tools Corregge livello di log in workflow
* Evita la riassegnazione del post all'aggiornamento
* Svuota la cache di operatori e gruppi
* Corregge page_url e asset_url nelle clusterizzazioni su aws

**[opencontent/openpa_userprofile-ls changes between 20b44fb and 2e192b1](https://github.com/OpencontentCoop/openpa_userprofile/compare/20b44fb...2e192b1)**
* Impone il redirect su user/edit


## [1.2.13](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.12...1.2.13) - 2020-03-03
- Update installer to 1.2.5 Add category owner + Fix anonymous role
- Added version file to build phase
- Reduce the building work: get the generic image built for opencontent/ezpublish

#### Code dependencies
| Changes                       | From     | To       | Compare                                                                           |
|-------------------------------|----------|----------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php               | 3.133.20 | 3.133.27 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.20...3.133.27)             |
| friendsofsymfony/http-cache   | 1658d8a  | 7560f30  | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/1658d8a...7560f30) |
| opencontent/ocbootstrap-ls    | 1.9.4    | 1.9.6    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.4...1.9.6)       |
| opencontent/ocsensor-ls       | 4.3.9    | 4.3.11   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.9...4.3.11)         |
| opencontent/ocsensorapi       | 4.3.4    | 4.3.5    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.4...4.3.5)       |
| opencontent/ocsocialdesign-ls | 1.6.2    | 1.6.3    | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.2...1.6.3)    |
| opencontent/openpa_sensor-ls  | 4.1.1    | 4.1.4    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.1...4.1.4)     |
| psr/log                       | 5628725  | e1cb6ec  | [...](https://github.com/php-fig/log/compare/5628725...e1cb6ec)                   |
| symfony/event-dispatcher      | ec966bd  | 7d4f83e  | [...](https://github.com/symfony/event-dispatcher/compare/ec966bd...7d4f83e)      |
| symfony/polyfill-ctype        | fbdeaec  | 4719fa9  | [...](https://github.com/symfony/polyfill-ctype/compare/fbdeaec...4719fa9)        |
| symfony/polyfill-mbstring     | 34094cf  | 766ee47  | [...](https://github.com/symfony/polyfill-mbstring/compare/34094cf...766ee47)     |
| symfony/polyfill-php73        | 5e66a0f  | 0f27e9f  | [...](https://github.com/symfony/polyfill-php73/compare/5e66a0f...0f27e9f)        |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.4 and 1.9.6](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.4...1.9.6)**
* Aggiorna le traduzioni in greco
* Aggiorna le traduzioni in greco

**[opencontent/ocsensor-ls changes between 4.3.9 and 4.3.11](https://github.com/OpencontentCoop/ocsensor/compare/4.3.9...4.3.11)**
* Aggiunge expiry per sensor cache
* Corregge alcuni bug del'interfaccia

**[opencontent/ocsensorapi changes between 4.3.4 and 4.3.5](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.4...4.3.5)**
* Intercept category owner

**[opencontent/ocsocialdesign-ls changes between 1.6.2 and 1.6.3](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.2...1.6.3)**
* Permette l'inserimento di link nel testo dei credits

**[opencontent/openpa_sensor-ls changes between 4.1.1 and 4.1.4](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.1...4.1.4)**
* Aggiunge i template per sensor root e sensor post root
* Aggiunge template di default
* Legge il file VERSION (se presente) per esporre la versione corrente nei credits


## [1.2.12](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.11...1.2.12) - 2020-02-24


#### Code dependencies
| Changes                 | From     | To       | Compare                                                                     |
|-------------------------|----------|----------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.133.18 | 3.133.20 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.18...3.133.20)       |
| opencontent/ocsensor-ls | 4.3.8    | 4.3.9    | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.8...4.3.9)    |
| opencontent/ocsensorapi | 4.3.3    | 4.3.4    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.3...4.3.4) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.8 and 4.3.9](https://github.com/OpencontentCoop/ocsensor/compare/4.3.8...4.3.9)**
* Corregge un bug di digitazione
* Corregge un bug nell'interfaccia di upload di un allegato

**[opencontent/ocsensorapi changes between 4.3.3 and 4.3.4](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.3...4.3.4)**
* Corregge un'eccezione in caso di inserimento comment in post chiuso
* Fix send notification to group


## [1.2.11](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.10...1.2.11) - 2020-02-20


#### Code dependencies
| Changes                        | From    | To      | Compare                                                                             |
|--------------------------------|---------|---------|-------------------------------------------------------------------------------------|
| opencontent/ocwebhookserver-ls | 38bc369 | 32df3cf | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/38bc369...32df3cf) |


Relevant changes by repository:

**[opencontent/ocwebhookserver-ls changes between 38bc369 and 32df3cf](https://github.com/OpencontentCoop/ocwebhookserver/compare/38bc369...32df3cf)**
* Add hook filter gui


## [1.2.10](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.9...1.2.10) - 2020-02-20


#### Code dependencies
| Changes                            | From     | To       | Compare                                                                                |
|------------------------------------|----------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.133.13 | 3.133.18 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.13...3.133.18)                  |
| ezsystems/ezpublish-legacy         | 19701b9  | 855cc50  | [...](https://github.com/Opencontent/ezpublish-legacy/compare/19701b9...855cc50)       |
| guzzlehttp/promises                | 6379353  | 89b1a76  | [...](https://github.com/guzzle/promises/compare/6379353...89b1a76)                    |
| opencontent/ocbootstrap-ls         | 1.9.3    | 1.9.4    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.3...1.9.4)            |
| opencontent/ocopendata-ls          | 2.23.4   | 2.23.5   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.23.4...2.23.5)           |
| opencontent/ocopendata_forms-ls    | 1.6.2    | 1.6.3    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.2...1.6.3)       |
| opencontent/ocsensor-ls            | 4.3.7    | 4.3.8    | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.7...4.3.8)               |
| opencontent/ocsensorapi            | 4.3.2    | 4.3.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.2...4.3.3)            |
| opencontent/ocsocialdesign-ls      | 1.6.1    | 1.6.2    | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.1...1.6.2)         |
| opencontent/openpa-ls              | d3e73a0  | 20665ce  | [...](https://github.com/OpencontentCoop/openpa/compare/d3e73a0...20665ce)             |
| symfony/deprecation-contracts      | f315c0a  | ede224d  | [...](https://github.com/symfony/deprecation-contracts/compare/f315c0a...ede224d)      |
| symfony/event-dispatcher-contracts | 5531ac7  | 5a749bd  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5531ac7...5a749bd) |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.3 and 1.9.4](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.3...1.9.4)**
* Corregge un bug sulle traduzioni di edit ocevents

**[opencontent/ocopendata-ls changes between 2.23.4 and 2.23.5](https://github.com/OpencontentCoop/ocopendata/compare/2.23.4...2.23.5)**
* Hotfix in SearchGateway query with no limitations

**[opencontent/ocopendata_forms-ls changes between 1.6.2 and 1.6.3](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.2...1.6.3)**
* Corregge alcune stringhe di traduzione in greco

**[opencontent/ocsensor-ls changes between 4.3.7 and 4.3.8](https://github.com/OpencontentCoop/ocsensor/compare/4.3.7...4.3.8)**
* Imposta il filtro per categoria sui webhook
* Aggiunge la notifica su messaggio privato
* Corregge un bug dei template nei post senza geolocalizzazione
* Visualizza l'autore del timeline item Corregge il log del webhook

**[opencontent/ocsensorapi changes between 4.3.2 and 4.3.3](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.2...4.3.3)**
* Accept urp as group Add on_send_private_message notification Fix can_respond permission check

**[opencontent/ocsocialdesign-ls changes between 1.6.1 and 1.6.2](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.1...1.6.2)**
* Corregge alcune stringhe di traduzione in greco

**[opencontent/openpa-ls changes between d3e73a0 and 20665ce](https://github.com/OpencontentCoop/openpa/compare/d3e73a0...20665ce)**
* Coregge un problema nella creazione dell'oranigramma  2874


## [1.2.9](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.8...1.2.9) - 2020-02-12


#### Code dependencies
| Changes                            | From    | To       | Compare                                                                                |
|------------------------------------|---------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.133.7 | 3.133.13 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.7...3.133.13)                   |
| composer/installers                | 71a969f | 7d610d5  | [...](https://github.com/composer/installers/compare/71a969f...7d610d5)                |
| guzzlehttp/promises                | ac2529f | 6379353  | [...](https://github.com/guzzle/promises/compare/ac2529f...6379353)                    |
| opencontent/ocbootstrap-ls         | 1.9.2   | 1.9.3    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.2...1.9.3)            |
| opencontent/ocmultibinary-ls       | 2.2     | 2.2.1    | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2...2.2.1)            |
| opencontent/ocsensor-ls            | 4.3.5   | 4.3.7    | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.5...4.3.7)               |
| opencontent/ocsensorapi            | 4.3.1   | 4.3.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.1...4.3.2)            |
| opencontent/openpa-ls              | db7c958 | d3e73a0  | [...](https://github.com/OpencontentCoop/openpa/compare/db7c958...d3e73a0)             |
| symfony/deprecation-contracts      |         | f315c0a  |                                                                                        |
| symfony/event-dispatcher           | 6a29d7f | ec966bd  | [...](https://github.com/symfony/event-dispatcher/compare/6a29d7f...ec966bd)           |
| symfony/event-dispatcher-contracts | 454f462 | 5531ac7  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/454f462...5531ac7) |
| symfony/options-resolver           | 188abc9 | 1942f69  | [...](https://github.com/symfony/options-resolver/compare/188abc9...1942f69)           |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.2 and 1.9.3](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.2...1.9.3)**
* Corregge le traduzioni del modulo di cambio password

**[opencontent/ocmultibinary-ls changes between 2.2 and 2.2.1](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2...2.2.1)**
* Fix opendata converter

**[opencontent/ocsensor-ls changes between 4.3.5 and 4.3.7](https://github.com/OpencontentCoop/ocsensor/compare/4.3.5...4.3.7)**
* Fix wrong 204 response
* Corregge un problema nella trasformazione dell'url nel webhook

**[opencontent/ocsensorapi changes between 4.3.1 and 4.3.2](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.1...4.3.2)**
* Bugfix status api
* Fix private message api

**[opencontent/openpa-ls changes between db7c958 and d3e73a0](https://github.com/OpencontentCoop/openpa/compare/db7c958...d3e73a0)**
* Aggiunge WhatsApp e Telegram tra i valori possibili dei contatti
* Considera content_tag_menu nel calcolo del tree menu


## [1.2.8](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.7...1.2.8) - 2020-02-05


#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| friendsofsymfony/http-cache | 5abb500 | 1658d8a | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/5abb500...1658d8a) |
| opencontent/ocsensorapi     | 4.3.0   | 4.3.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.0...4.3.1)       |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 4.3.0 and 4.3.1](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.0...4.3.1)**
* Bugfix openapi definition


## [1.2.7](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.6...1.2.7) - 2020-02-05


#### Code dependencies
| Changes                  | From      | To      | Compare                                                                      |
|--------------------------|-----------|---------|------------------------------------------------------------------------------|
| aws/aws-sdk-php          | 3.133.6   | 3.133.7 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.6...3.133.7)          |
| erasys/openapi-php       | 307cce3   | 9885e8f | [...](https://github.com/erasys/openapi-php/compare/307cce3...9885e8f)       |
| illuminate/contracts     | 5.8.x-dev | 6.x-dev | [...](https://github.com/illuminate/contracts/compare/5.8.x-dev...6.x-dev)   |
| opencontent/ocsensor-ls  | 4.3.4     | 4.3.5   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.4...4.3.5)     |
| symfony/event-dispatcher | af3ef02   | 6a29d7f | [...](https://github.com/symfony/event-dispatcher/compare/af3ef02...6a29d7f) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.4 and 4.3.5](https://github.com/OpencontentCoop/ocsensor/compare/4.3.4...4.3.5)**
* Add spinner in geo search, fix duplicate suggestions, force readonly area select


## [1.2.6](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.5...1.2.6) - 2020-01-31


#### Code dependencies
| Changes                   | From    | To      | Compare                                                                    |
|---------------------------|---------|---------|----------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.133.5 | 3.133.6 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.5...3.133.6)        |
| opencontent/ocexportas-ls | 1.3.4   | 1.3.5   | [...](https://github.com/OpencontentCoop/ocexportas/compare/1.3.4...1.3.5) |
| opencontent/ocsensor-ls   | 4.3.3   | 4.3.4   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.3...4.3.4)   |
| opencontent/openpa-ls     | 0fffd64 | db7c958 | [...](https://github.com/OpencontentCoop/openpa/compare/0fffd64...db7c958) |


Relevant changes by repository:

**[opencontent/ocexportas-ls changes between 1.3.4 and 1.3.5](https://github.com/OpencontentCoop/ocexportas/compare/1.3.4...1.3.5)**
* Corregge l'esportazione ANAC dei raggruppamenti

**[opencontent/ocsensor-ls changes between 4.3.3 and 4.3.4](https://github.com/OpencontentCoop/ocsensor/compare/4.3.3...4.3.4)**
* Migliora l'esperienza utente in fase di selezione della geolocalizzazione
* Corregge i link da http a https

**[opencontent/openpa-ls changes between 0fffd64 and db7c958](https://github.com/OpencontentCoop/openpa/compare/0fffd64...db7c958)**
* Evoluzione di robots.txt per bloccare i bad crawler


## [1.2.5](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.4...1.2.5) - 2020-01-24


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 4.3.2 | 4.3.3 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.2...4.3.3) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.2 and 4.3.3](https://github.com/OpencontentCoop/ocsensor/compare/4.3.2...4.3.3)**
* Espone la textarea meta anche all'utente con permessi standard


## [1.2.4](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.3...1.2.4) - 2020-01-23


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 4.3.1 | 4.3.2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.1...4.3.2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.1 and 4.3.2](https://github.com/OpencontentCoop/ocsensor/compare/4.3.1...4.3.2)**
* hot bugfix


## [1.2.3](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.2...1.2.3) - 2020-01-23
- Fix sensor_post/privacy label in installer

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                  |
|-------------------------|---------|---------|--------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.133.4 | 3.133.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.4...3.133.5)      |
| opencontent/ocsensor-ls | 4.3.0   | 4.3.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.0...4.3.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.0 and 4.3.1](https://github.com/OpencontentCoop/ocsensor/compare/4.3.0...4.3.1)**
* Limita la ricerca di nominatim al boundbox del perimetro della prima area configurata


## [1.2.2](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.1...1.2.2) - 2020-01-23


#### Code dependencies
| Changes              | From    | To      | Compare                                                                   |
|----------------------|---------|---------|---------------------------------------------------------------------------|
| opencontent/ocmap-ls | 39ee78b | 93c8432 | [...](https://github.com/OpencontentCoop/ocmap/compare/39ee78b...93c8432) |


Relevant changes by repository:

**[opencontent/ocmap-ls changes between 39ee78b and 93c8432](https://github.com/OpencontentCoop/ocmap/compare/39ee78b...93c8432)**
* Introduce il connettore per ocopendata_form


## [1.2.1](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.0...1.2.1) - 2020-01-23
- Fix area class in installer



## [1.2.0](https://gitlab.com/opencontent/opensegnalazioni/compare/1.0.0...1.2.0) - 2020-01-22
- Implementazione dei perimetri di area e del servizio di ricerca del civico di prossimità
- Update deps
-  2020-01-13 lorello: required to allow authentication in Sensor Genova      using SIRAC variables comge_nome, comge_cognome, etc...     underscores_in_headers on;
- Update composer dependencies
- Update traefik Docker tag to v1.7.20
- Require user fiscal code as env var
- Assegnazione osservatori in base a Quartiere/Zona di una segnalazione, Predisposizione del modulo di login con SPID, Apertura segnalazioni da chiamate all'Helpdesk (URP)
- Assegnazione osservatori in base a Quartiere/Zona di una segnalazione Predisposizione del modulo di login con SPID Apertura segnalazioni da chiamate all'Helpdesk (URP)
- Add webhooks and fiscal_code validations
- Add zally linter inside CI
- Update sensor extensions, add userprofile in settings, fix installer
- Fix area and category relations classes and editor base role in installer Fix line endings
- Fix docker compose: removed volume on source image
- Changed installer startup: moved inside main entrypoint script to easily use the available variables EZ_ROOT, EZ_INSTANCE
- Fixed unsupported use of variables in Dockefile of ngix
- Fixed Build arg that is now lowercase
- Fixed error on image names
- Fixed variable names using protection parenthesis
- Fixed order of image building: start with php and then build nguinx
- Fixed multiple run when branch=master
- Added multiple image creation
- Dropped zap config
- Added CI
- Update composer
- fix installer sensor admin role
- Update composer
- Update compser, avoid reinstall on socker-compose up
- Upgrade to ocsensor  version 4

#### Code dependencies
| Changes                            | From      | To        | Compare                                                                                |
|------------------------------------|-----------|-----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.112.21  | 3.133.4   | [...](https://github.com/aws/aws-sdk-php/compare/3.112.21...3.133.4)                   |
| composer/installers                | 141b272   | 71a969f   | [...](https://github.com/composer/installers/compare/141b272...71a969f)                |
| erasys/openapi-php                 |           | 307cce3   |                                                                                        |
| ezsystems/ezmbpaex-ls              | 5.3.3.1   | 5.3.3.3   | [...](https://github.com/Opencontent/ezmbpaex/compare/5.3.3.1...5.3.3.3)               |
| friendsofsymfony/http-cache        | 01d87fa   | 5abb500   | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/01d87fa...5abb500)      |
| guzzlehttp/guzzle                  | bcbb52f   | 6.5.x-dev | [...](https://github.com/guzzle/guzzle/compare/bcbb52f...6.5.x-dev)                    |
| guzzlehttp/promises                | 17d36ed   | ac2529f   | [...](https://github.com/guzzle/promises/compare/17d36ed...ac2529f)                    |
| illuminate/contracts               |           | 5.8.x-dev |                                                                                        |
| justinrainbow/json-schema          |           | 5.2.9     |                                                                                        |
| league/event                       |           | 7823f10   |                                                                                        |
| mtdowling/jmespath.php             | 2.4.0     | 52168cb   | [...](https://github.com/jmespath/jmespath.php/compare/2.4.0...52168cb)                |
| opencontent/ocbootstrap-ls         | 1.7.4     | 1.9.2     | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.7.4...1.9.2)            |
| opencontent/occodicefiscale-ls     |           | 542aa99   |                                                                                        |
| opencontent/ocexportas-ls          | 1.3.3     | 1.3.4     | [...](https://github.com/OpencontentCoop/ocexportas/compare/1.3.3...1.3.4)             |
| opencontent/ocgdprtools-ls         | 1.3.0     | 1.4.5     | [...](https://github.com/OpencontentCoop/ocgdprtools/compare/1.3.0...1.4.5)            |
| opencontent/ocinstaller            | a0f23f7   | ce5b6e1   | [...](https://github.com/OpencontentCoop/ocinstaller/compare/a0f23f7...ce5b6e1)        |
| opencontent/ocmap-ls               |           | 39ee78b   |                                                                                        |
| opencontent/ocmultibinary-ls       | 2.1       | 2.2       | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.1...2.2)              |
| opencontent/ocopendata-ls          | 2.22.0    | 2.23.4    | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.22.0...2.23.4)           |
| opencontent/ocopendata_forms-ls    |           | 1.6.2     |                                                                                        |
| opencontent/ocsearchtools-ls       | 1.10.7    | 1.10.9    | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.7...1.10.9)        |
| opencontent/ocsensor-ls            | 3.2.0     | 4.3.0     | [...](https://github.com/OpencontentCoop/ocsensor/compare/3.2.0...4.3.0)               |
| opencontent/ocsensorapi            | 2.1.1     | 4.3.0     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/2.1.1...4.3.0)            |
| opencontent/ocsiracauth-ls         |           | 1.0       |                                                                                        |
| opencontent/ocsocialdesign-ls      | 1.5.1.0   | 1.6.1     | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.5.1.0...1.6.1)       |
| opencontent/ocsocialuser-ls        | 1.7.2.0   | 1.8.1     | [...](https://github.com/OpencontentCoop/ocsocialuser/compare/1.7.2.0...1.8.1)         |
| opencontent/ocwebhookserver-ls     |           | 38bc369   |                                                                                        |
| opencontent/openpa-ls              | 12d6d5f   | 0fffd64   | [...](https://github.com/OpencontentCoop/openpa/compare/12d6d5f...0fffd64)             |
| opencontent/openpa_sensor-ls       | 1.6       | 4.1.1     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/1.6...4.1.1)            |
| opencontent/openpa_theme_2014-ls   | 2.14.1    | 2.15.0    | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.14.1...2.15.0)    |
| opencontent/openpa_userprofile-ls  |           | 20b44fb   |                                                                                        |
| php-http/client-common             | 9a2a4c1   | 03086a2   | [...](https://github.com/php-http/client-common/compare/9a2a4c1...03086a2)             |
| php-http/discovery                 | 5a0da02   | 82dbef6   | [...](https://github.com/php-http/discovery/compare/5a0da02...82dbef6)                 |
| php-http/guzzle6-adapter           | 4b491b7   | bd7b405   | [...](https://github.com/php-http/guzzle6-adapter/compare/4b491b7...bd7b405)           |
| php-http/httplug                   | 7af4427   | c9b2424   | [...](https://github.com/php-http/httplug/compare/7af4427...c9b2424)                   |
| php-http/message                   | 144d13b   | fcf9b45   | [...](https://github.com/php-http/message/compare/144d13b...fcf9b45)                   |
| php-http/promise                   | 1cc44dc   | 02ee67f   | [...](https://github.com/php-http/promise/compare/1cc44dc...02ee67f)                   |
| psr/container                      |           | fc1bc36   |                                                                                        |
| psr/event-dispatcher               |           | cfea31e   |                                                                                        |
| psr/log                            | c4421fc   | 5628725   | [...](https://github.com/php-fig/log/compare/c4421fc...5628725)                        |
| psr/simple-cache                   |           | 408d5ea   |                                                                                        |
| symfony/event-dispatcher           | 4.4.x-dev | af3ef02   | [...](https://github.com/symfony/event-dispatcher/compare/4.4.x-dev...af3ef02)         |
| symfony/event-dispatcher-contracts | c43ab68   | 454f462   | [...](https://github.com/symfony/event-dispatcher-contracts/compare/c43ab68...454f462) |
| symfony/options-resolver           | 4.4.x-dev | 188abc9   | [...](https://github.com/symfony/options-resolver/compare/4.4.x-dev...188abc9)         |
| symfony/polyfill-ctype             | 550ebaa   | fbdeaec   | [...](https://github.com/symfony/polyfill-ctype/compare/550ebaa...fbdeaec)             |
| symfony/polyfill-mbstring          |           | 34094cf   |                                                                                        |
| symfony/polyfill-php73             | 2ceb49e   | 5e66a0f   | [...](https://github.com/symfony/polyfill-php73/compare/2ceb49e...5e66a0f)             |
| zetacomponents/mail                | 1.8.3     | 1.9.1     | [...](https://github.com/zetacomponents/Mail/compare/1.8.3...1.9.1)                    |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.7.4 and 1.9.2](https://github.com/OpencontentCoop/ocbootstrap/compare/1.7.4...1.9.2)**
* Specifica la lingua corrente in user/edit, corregge la visualizzazione del template di edit del datatype ocgdpr in bootstrap4
* Recepisce la funzionalità di ordinamento di file multipli in bootstrap4
* Aggiunge il file di traduzione delle stringhe in greco
* Corregge il file di traduzione delle stringhe in greco
* Introduce la funzionalità dei moduli di login per cui è possibile aggiungere un modulo alla pagina /user/login da un'estensione terza
* Permette l'inserimento di testo formattato nei moduli di login
* Recepisce gli aggiornamenti di ocevents

**[opencontent/ocexportas-ls changes between 1.3.3 and 1.3.4](https://github.com/OpencontentCoop/ocexportas/compare/1.3.3...1.3.4)**
* Aggiorna exporter ANAC sulla base di http://www.anticorruzione.it/portal/public/classic/Servizi/ServiziOnline/DichiarazioneAdempLegge190

**[opencontent/ocgdprtools-ls changes between 1.3.0 and 1.4.5](https://github.com/OpencontentCoop/ocgdprtools/compare/1.3.0...1.4.5)**
* Rende traducibile il testo e i link dell'attributo a livello di classe (con retrocompatibilità) Da la possibilità di forzare l'annullamento dell'accettazione e costringe l'utente ad accettare al successivo login Logga su ezaudit
* Bug fix on user acceptance
* Fix conflict with openpa_userprofile
* Corregge la visualizzazione del datatype in versione collectinfo
* Corregge il connettore opendata_form per il caso in cui l'editor non sia l'utente corrente
* Svuota la cache statica (varnish) al cambio di valore di accettazione (varnish mette in cache anche i 302)
* Ignora i privilegi di accesso al modulo gdpr/user_acceptance

**[opencontent/ocinstaller changes between a0f23f7 and ce5b6e1](https://github.com/OpencontentCoop/ocinstaller/compare/a0f23f7...ce5b6e1)**
* Ignore content update, Add custom sql, Typo fixes
* Add storing and checking installer version
* Install tag tree from local yaml
* recursive read tag tree
* Log class compare details
* Add condition parameter
* Get env vars from $_SERVER
* Add is_install_from_scratch global variable
* Bug fix  is_install_from_scratch variable
* workaround is_install_from_scratch and variable not found

**[opencontent/ocmultibinary-ls changes between 2.1 and 2.2](https://github.com/OpencontentCoop/ocmultibinary/compare/2.1...2.2)**
* Added sort feature ( 4)    Added sort feature   Fixed translations

**[opencontent/ocopendata-ls changes between 2.22.0 and 2.23.4](https://github.com/OpencontentCoop/ocopendata/compare/2.22.0...2.23.4)**
* Introduce la ricerca cursor tramite eZFindExtendedAttributeFilterInterface
* Considera più valori per il campo field nel filtro stato
* Permette l'aggiornamento di ezuser senza errore
* Previene fatal error in caso di cluster failure in section repository
* Fix calling static method Base::validate non statically
* Add tag children pagination query parameters
* Fix temp file creation in exportas paginated download

**[opencontent/ocsearchtools-ls changes between 1.10.7 and 1.10.9](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.7...1.10.9)**
* Corregge l'indicizzazione della prima lettera della stringa e introduce un nuovo subattribute normalized utile per l'ordinamento alfabetico
* Corregge le stringhe statiche e imposta le traduzioni

**[opencontent/ocsensor-ls changes between 3.2.0 and 4.3.0](https://github.com/OpencontentCoop/ocsensor/compare/3.2.0...4.3.0)**
* add admin edit_privacy template avoiding ez error
* edit response action and gui
* Introduce il codice per il google tag manager
* Corregge un bug nella visualizzazione della data di modifica
* Corregge un bug nel form di inserimento delle segnalazioni
* Refactor completo orientato a openapi
* Bugfix
* Bugfix in notifications settings
* Remove user/edit template override
* Bugfix in notifications settings
* Fix mail vars on pre-publish event
* Avoid ezcMvcResponseWriter status code bug
* Fix ApproverCanReopen or AuthorCanReopen
* Add child category in csv export
* Add export in sensor/stat
* Fix ezmpaex forgot password link
* Fix repository::isModerationEnabled()
* Fix category assign action
* Fix terms link ( 8)  Espone il link ai termini di servizio nell'edit di segnalazione https://opencontent.freshdesk.com/a/tickets/2295
* Add full dashboard participant filters
* Merge branch 'refactor-category-assign' into openapi   Conflicts:  	translations/ger-DE/translation.ts  	translations/untranslated/translation.ts
* Fix translations string in edit post
* Add webhooks
* Show edit attribute sensor_root/user_inactivity_days as select
* Introduces the interface for the creation of users when posting
* Introduce new FlashMessage listener Remove unused scenarios
* Recepisce la funzionalità login_modules di ocbootstrap
* Introduce la funzionalità dei perimetri di selezione nelle zone e il servizio opzionale per ricavare il civico di prossimità

**[opencontent/ocsensorapi changes between 2.1.1 and 4.3.0](https://github.com/OpencontentCoop/ocsensorapi/compare/2.1.1...4.3.0)**
* Refactor per esposizione delle api in formato openapi
* Bugfix
* Fix ApproverCanReopen or AuthorCanReopen
* Fix parent/child category in PerCategory stat
* Fix correct object version in PostInitializer
* Unique definition of global moderation in repository::isModerationEnabled
* Fix category assign action
* Consider the reporter and author as distinct users
* Add area observer on set area or on set category Remove unused shenarios Introduce MailValidator as eZMail::validate wrapper Fix some bugs
* Avoid fatal error in FirstAreaApproverScenario when install
* Avoid fatal error in FirstAreaApproverScenario when install
* Add meta attribute and area bounding box in area tree

**[opencontent/ocsocialdesign-ls changes between 1.5.1.0 and 1.6.1](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.5.1.0...1.6.1)**
* Aggiunge il file di traduzione delle stringhe in greco
* Eredita il template di login da ocbootstrap

**[opencontent/ocsocialuser-ls changes between 1.7.2.0 and 1.8.1](https://github.com/OpencontentCoop/ocsocialuser/compare/1.7.2.0...1.8.1)**
* Aggiunge il file di traduzione delle stringhe in greco
* Corregge un bug su stringa di traduzione

**[opencontent/openpa-ls changes between 12d6d5f and 0fffd64](https://github.com/OpencontentCoop/openpa/compare/12d6d5f...0fffd64)**
* Fix create_instance script
* add agenda in OpenPABase::getInstances

**[opencontent/openpa_sensor-ls changes between 1.6 and 4.1.1](https://github.com/OpencontentCoop/openpa_sensor/compare/1.6...4.1.1)**
* Refactor per allineamento alla nuova versione di ocsensor
* Bugfix
* Pass correct object version in PostInitializer
* Add default notification and dashboard filters in workflow
* Add webhook menu
* Hide sensor post in default template

**[opencontent/openpa_theme_2014-ls changes between 2.14.1 and 2.15.0](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.14.1...2.15.0)**
* Eredita il template di login da ocbootstrap e recepisce la funzionalità login_modules di ocbootstrap


## [1.0.0] - 2019-10-15

First public release


